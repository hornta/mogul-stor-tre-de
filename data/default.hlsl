// [header]
struct PixelInput 
{
	float4 world : POSITION;
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD0;
	float3 normal : NORMAL;
};

// [vertex]
struct VertexInput 
{
	float4 position : POSITION;
	float2 texcoord : TEXCOORD0;
	float3 normal : NORMAL;
};

cbuffer OncePerFrame 
{
	matrix projection;
	matrix view;
};

cbuffer OncePerObject
{
	matrix world;
};

PixelInput main(VertexInput input) 
{
	PixelInput output;
	output.world = mul(world, input.position);
	output.position = mul(view, output.world);
	output.position = mul(projection, output.position);
	output.texcoord = input.texcoord;
	output.normal = normalize(mul((float3x3)world, input.normal));
	return output;
}

// [pixel]

#define MAX_LIGHTS 8
#define DIRECTIONAL_LIGHT 0
#define POINT_LIGHT 1
#define SPOT_LIGHT 2

Texture2D texture0;
SamplerState sampler0;

struct Material
{
	float4 emissive;
	float4 ambient;
	float4 diffuse;
	float4 specular;
	float specularPower;
	bool useTexture;
	float2 padding;
};

cbuffer MaterialProperties
{
	Material material;
};

struct Light
{
	float3 position; float pad0;
	float3 direction; float pad1;
	float4 color;
	float spotAngle;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;
	int type;
	bool enabled;
	int2 padding;
};

cbuffer LightProperties
{
	float4 eye;
	float4 globalAmbient;
	Light lights[MAX_LIGHTS];
};

struct LightningResult
{
	float4 diffuse;
	float4 specular;
};

float4 performDiffuse(Light light, float3 L, float3 N)
{
	// Take the dot product between the light vector and the normal
	// Both vectors has to be normalized because the dotProduct must lie between [0..1]
	float dotProduct = max(0, dot(N, L));
	
	return light.color * dotProduct;
}

float4 performSpecular(Light light, float3 V, float3 L, float3 N)
{
	// Take the reflection of the light vector on the normal
	float3 R = normalize(reflect(-L, N));
	float dotProduct = max(0, dot(R, V));
	return light.color * pow(dotProduct, material.specularPower);
}

float performAttenuation(Light light, float d)
{
	return 1.f / (light.constantAttenuation + light.linearAttenuation * d + light.quadraticAttenuation * d * d);
}

float getSpotLightIntensity(Light light, float3 L)
{
	float minCos = cos(light.spotAngle);
	float maxCos = (minCos + 1.f) / 2.f;
	float cosAngle = dot(light.direction.xyz, -L);
	return smoothstep(minCos, maxCos, cosAngle);
}

LightningResult performPointLight(Light light, float3 V, float4 P, float3 N)
{
	LightningResult result;
	
	// Light vector
	float3 L = (light.position - P).xyz;
	
	// Distance from pixel to light source
	float distance = length(L);
	
	// Normalize light vector
	L /= distance;
	
	// Calculate the attenuation
	float attenuation = performAttenuation(light, distance);
	
	// Scale the diffuse and specular components by the attenuation
	result.diffuse = performDiffuse(light, L, N) * attenuation;
	result.specular = performSpecular(light, V, L, N) * attenuation;
	
	return result;
}

LightningResult performDirectionalLight(Light light, float3 V, float3 N)
{
	LightningResult result;
	float3 L = -light.direction.xyz;
	
	result.diffuse = performDiffuse(light, L, N);
	result.specular = performSpecular(light, V, L, N);
	return result;
}

LightningResult performSpotLight(Light light, float3 V, float4 P, float3 N)
{
	LightningResult result;
	
	float3 L = (light.position - P).xyz;
	float distance = length(L);
	L = L  / distance;
	
	float attenuation = performAttenuation(light, distance);
	float spotIntensity = getSpotLightIntensity(light, L);
	
	result.diffuse = performDiffuse(light, L, N) * attenuation * spotIntensity;
	result.specular = performSpecular(light, V, L, N) * attenuation * spotIntensity;
	
	return result;
}

LightningResult performLightning(float4 P, float3 N)
{
	float3 V = normalize(eye - P).xyz;
	
	LightningResult finalResult = {{0, 0, 0, 0}, {0, 0, 0, 0}};
	
	for(int i = 0; i < MAX_LIGHTS; ++i)
	{
		LightningResult result = {{0, 0, 0, 0}, {0, 0, 0, 0}};
		if(!lights[i].enabled) continue;
		
		switch(lights[i].type)
		{
		case DIRECTIONAL_LIGHT:
			result = performDirectionalLight(lights[i], V, N);
			break;
			
		case POINT_LIGHT:
			result = performPointLight(lights[i], V, P, N);
			break;
			
		case SPOT_LIGHT:
			result = performSpotLight(lights[i], V, P, N);
			break;
		}
		
		finalResult.diffuse += result.diffuse;
		finalResult.specular += result.specular;
	}
	
	finalResult.diffuse = saturate(finalResult.diffuse);
	finalResult.specular = saturate(finalResult.specular);
	return finalResult;
}

float4 main(PixelInput input) : SV_TARGET
{
	LightningResult light = performLightning(input.world, normalize(input.normal));
	
	float4 emissive = material.emissive;
	float4 ambient = material.ambient * globalAmbient;
	float4 diffuse = material.diffuse * light.diffuse;
	float4 specular = material.specular * light.specular;
	
	float4 textureColor = { 1, 1, 1, 1 };
	if(material.useTexture)
	{
		textureColor = texture0.Sample(sampler0, input.texcoord);
	}
	
	float4 finalColor = (emissive + ambient + diffuse + specular) * textureColor;
	return finalColor;
}
