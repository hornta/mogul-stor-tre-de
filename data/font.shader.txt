// [header]
struct PixelInput 
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
};

// [vertex]
struct VertexInput 
{
	float3 position : POSITION;
	float2 texcoord : TEXCOORD;
};

cbuffer OncePerFrame 
{
	matrix projection;
};

PixelInput main(VertexInput input) 
{
	PixelInput output;
	output.position = mul(projection, input.position);
	output.texcoord = input.texcoord;
	return output;
}

// [pixel]
Texture2D diffuseTexture;
SamplerState defaultSampler;

float4 main(PixelInput input) : SV_TARGET
{
	return diffuseTexture.Sample(defaultSampler, input.texcoord);
}
