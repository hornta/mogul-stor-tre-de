// gui.shader.txt

// [header]
struct PixelInput
{
	float4 position : SV_POSITION;
	float2 texcoord : TEXCOORD;
};

// [vertex]

struct VertexInput
{
	float3 position : POSITION;
	float2 texcoord : TEXCOORD;
};

cbuffer Camera
{
	matrix projection;
};

cbuffer Object
{
	matrix world;
};

PixelInput main(VertexInput input)
{
	PixelInput output;
	output.position = mul(world, float4(input.position, 1.0));
	output.position = mul(projection, output.position);
	output.texcoord = input.texcoord;
	return output;
}

// [pixel]

Texture2D diffuseMap;
SamplerState defaultSampler;

float4 main(PixelInput input) : SV_TARGET
{
	return diffuseMap.Sample(defaultSampler, input.texcoord);
}
