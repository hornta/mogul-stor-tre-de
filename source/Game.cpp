#include "stdafx.h"
#include "Game.h"

#include "loading_state.h"
#include "menu_state.h"
#include "ResourceLoader.h"

namespace helium
{
	gameplay::AbstractGame::Ptr Game::create()
	{
		return AbstractGame::Ptr(new Game);
	}

	Game::Game()
	{
	}

	Game::~Game()
	{

	}

	void Game::initialize()
	{
		gameplay::AbstractGame::initialize(); // initalize the main game class
		// create ids for states
		uint32_t string_hash[] =
		{
			String::hash32("loading_state", 14),
			String::hash32("menu_state", 10)
		};

		m_resourceLoader = new ResourceLoader();
		m_resourceLoader->init(m_render_system);
		m_resourceLoader->setShaderFolder("../data/");
		m_resourceLoader->setTextureFolder("../data/");
		m_resourceLoader->setSamplerFolder("../data/");
		m_resourceLoader->setVertexFormatFolder("../data/");
		m_resourceLoader->setFontFolder("../data/");

		ServiceLocator<ResourceLoader>().set_service(m_resourceLoader);
		// create state factory and attach states to it
		gameplay::StateFactory* factory = m_state_manager->get_factory();
		factory->attach<loading_state>(string_hash[0]);
		factory->attach<menu_state>(string_hash[1]);

		// and start a state
		//m_state_manager->set_state(string_hash[0]);
		m_state_manager->set_state(string_hash[1]);
	}

	void Game::shutdown()
	{
		delete m_resourceLoader;
		m_resourceLoader = nullptr;
		AbstractGame::shutdown();
	}
}