#include "stdafx.h"
#include "SoundSystem.h"
#include "fmod.hpp"
#include "fmod_errors.h"
#include <iostream>

Sound::Sound()
{

}

SoundSystem::SoundSystem(void)
{
	FMOD::System_Create(&m_system);
	m_system->init(128, FMOD_INIT_NORMAL, 0);
}

SoundSystem::~SoundSystem(void)
{
	auto it = m_sounds.begin();
	while (it != m_sounds.end())
	{
		it->second->m_sound->release();
		delete it->second;
		++it;
	}
	m_system->release();
}

void SoundSystem::Update()
{
	m_system->update();
}

void SoundSystem::loadSound(const std::string& filename, const std::string& key, FMOD_MODE mode)
{
	Sound* sound = new Sound();
	m_system->createSound(filename.c_str(), mode, 0, &sound->m_sound);
	
	auto it = m_sounds.find(key);
	if (it == m_sounds.end())
	{
		m_sounds.insert(std::make_pair(key, sound));
	}
}

//Play a song from a specific file at a specific volume
void SoundSystem::playSound(const std::string& key, float volume)
{
	auto it = m_sounds.find(key);
	if (it == m_sounds.end())
	{
		helium::Debug::write(helium::EDebugLevel::Info, "Sound does not exist");
		return;
	}

	Sound* sound = it->second;

	//Stops channel if channel is currently playing
	bool isPlaying = false;
	if (sound->m_channel->isPlaying(&isPlaying) == FMOD_OK)
	{
		sound->m_channel->stop();
	}

	//Start song with requested volume
	m_system->playSound(sound->m_sound, 0, true, &sound->m_channel);
	sound->m_channel->setVolume(volume);
	sound->m_channel->setPaused(false);
}

float SoundSystem::getVolume(const std::string& key)
{
	auto it = m_sounds.find(key);
	if (it == m_sounds.end())
	{
		helium::Debug::write(helium::EDebugLevel::Info, "Sound does not exist");
		return 0.f;
	}

	Sound* sound = it->second;
	bool isPlaying = false;
	float volume = 0.f;
	if (sound->m_channel->isPlaying(&isPlaying) == FMOD_OK)
	{
		sound->m_channel->getVolume(&volume);
	}
	return volume;
}

void SoundSystem::changeVolume(const std::string& key, float amount)
{
	auto it = m_sounds.find(key);
	if (it == m_sounds.end())
	{
		helium::Debug::write(helium::EDebugLevel::Info, "Sound does not exist");
		return;
	}

	Sound* sound = it->second;

	bool isPlaying = false;
	if (sound->m_channel->isPlaying(&isPlaying) == FMOD_OK)
	{
		sound->m_channel->setVolume(amount);
	}
}
