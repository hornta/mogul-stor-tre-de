// GameoObject.cpp

#include "stdafx.h"
#include "GameObject.h"
#include "Object3D.h"

void GameObject::SetPosition(Vector3 pos) {
	m_object3D->translate(pos - m_object3D->getDerivedPosition());
}

void GameObject::SetRotation(Vector3 rot) {
	rot;
	
	// ToDo: Fix if needed
}

void GameObject::Move(Vector3 pos) {
	m_object3D->translate(pos);
}

void GameObject::RotateX(float degrees) {
	m_object3D->pitch(degrees);
}

void GameObject::RotateY(float degrees) {
	m_object3D->yaw(degrees);
}

void GameObject::RotateZ(float degrees) {
	m_object3D->roll(degrees);
}

Vector3 GameObject::GetPosition() {
	return m_object3D->getDerivedPosition();
}

Vector3 GameObject::GetRotation() {
	Quaternion q = m_object3D->getOrientation();
	Euler e = q.toEuler();
	return Vector3(e.x, e.y, e.z);
}

Object3D* GameObject::GetObject3D() {
	return m_object3D;
}

ObjectType GameObject::GetType() {
	return m_type;
}
