// StaticObject.cpp

#include "stdafx.h"
#include "StaticObject.h"

#include "Geometry.hpp"
#include "Material.h"
#include "Mesh.h"

StaticObject::StaticObject(Object3D* obj) {
	m_object3D = obj;
	m_type = StaticO;
}

StaticObject::~StaticObject() {

}

bool StaticObject::Update(float dt) {
	dt;
	return true;
}

void StaticObject::CleanUp() {

}
