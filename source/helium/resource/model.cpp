// model.cpp

#include "stdafx.h"
#include <cassert>
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/resource/model.hpp>

namespace helium
{
	namespace resource
	{
		// static 
		VertexBuffer::Ptr VertexBuffer::create()
		{
			return VertexBuffer::Ptr(new VertexBuffer);
		}

		// private
		VertexBuffer::VertexBuffer()
		{
			m_mode = system::EBufferMode::Invalid;
			m_size = 0;
			m_count = 0;
			m_buffer = nullptr;
		}

		// public
		VertexBuffer::~VertexBuffer()
		{
			m_size = 0;
			m_count = 0;
			if (m_buffer)
			{
				delete[] m_buffer;
				m_buffer = nullptr;
			}
		}

		bool VertexBuffer::create(system::EBufferMode mode, uint16_t size, uint32_t count)
		{
			//ServiceLocator<system::RenderSystem>::get_service()->create_vertex_buffer()
			m_mode = mode;
			m_size = size;
			m_count = count;
			m_buffer = new uint8_t[m_size * m_count];
			return true;
		}

		uint16_t VertexBuffer::get_size() const
		{
			return m_size;
		}

		uint32_t VertexBuffer::get_count() const
		{
			return m_count;
		}

		const void* VertexBuffer::get_buffer() const
		{
			return m_buffer;
		}

		void VertexBuffer::dispose()
		{
			if (m_id == (uint32_t)-1)
				return;
			ServiceLocator<system::RenderSystem>::get_service()->destroy_vertex_buffer(m_id);
		}

		// static 
		IndexBuffer::Ptr IndexBuffer::create()
		{
			return IndexBuffer::Ptr(new IndexBuffer);
		}

		// private
		IndexBuffer::IndexBuffer()
		{
			m_size = 0;
			m_count = 0;
			m_buffer = nullptr;
		}

		// public
		IndexBuffer::~IndexBuffer()
		{
			m_size = 0;
			m_count = 0;
			if (m_buffer)
			{
				delete[] m_buffer;
				m_buffer = nullptr;
			}
		}

		uint16_t IndexBuffer::get_size() const
		{
			return m_size;
		}

		uint32_t IndexBuffer::get_count() const
		{
			return m_count;
		}

		const void* IndexBuffer::get_buffer() const
		{
			return m_buffer;
		}

		void IndexBuffer::dispose()
		{
			if (m_id == (uint32_t)-1)
				return;
			ServiceLocator<system::RenderSystem>::get_service()->destroy_index_buffer(m_id);
		}

		// static 
		Mesh::Ptr Mesh::create()
		{
			return Mesh::Ptr(new Mesh);
		}

		// private
		Mesh::Mesh()
		{
		}

		// public
		Mesh::~Mesh()
		{
		}

		const VertexBuffer* Mesh::get_vertex_buffer() const
		{
			return m_vertex_buffer.get();
		}

		bool Mesh::has_index_buffer() const
		{
			return m_index_buffer.get() != nullptr;
		}

		const IndexBuffer* Mesh::get_index_buffer() const
		{
			return m_index_buffer.get();
		}


#pragma pack(push, 1)
		struct ModelHeader
		{
			char		 magic[4];
			uint32_t version;
			uint32_t count;
		};

#pragma pack(pop)

		// static 
		Model::Ptr Model::create(const std::string& filename)
		{
			// todo(tommi)
			std::ifstream stream;
			stream.open(filename.c_str(), std::ios_base::binary);
			if (!stream.is_open())
			{
				Debug::write(EDebugLevel::Error, 
					String::format("model: could not open file %s", 
					filename.c_str()));
				return Model::Ptr(nullptr);
			}

			stream.close();

			Debug::write(EDebugLevel::Info, 
				String::format("model: %s", 
				filename.c_str()));

			return Model::Ptr(nullptr);
		}

		// private
		Model::Model()
		{

		}

		// public
		Model::~Model()
		{
			m_meshes.clear();
		}

		uint32_t Model::get_mesh_count() const
		{
			return (uint32_t)m_meshes.size();
		}

		const Mesh* Model::get_mesh(uint32_t index) const
		{
			assert(index < m_meshes.size());
			return m_meshes[index].get();
		}
	}
}
