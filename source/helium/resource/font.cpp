// font.cpp

#include "stdafx.h"
#include <helium/resource/font.hpp>

namespace helium
{
	namespace resource
	{
		Font::Font()
			: m_lineHeight(0),
			  m_base(0),
			  m_scaleW(0),
			  m_scaleH(0),
			  m_pages(0),
			  m_packed(0)
		{
			
		}

		Font::~Font()
		{
		}

		bool Font::loadFromFile(const std::string& filepath)
		{
			std::ifstream stream;
			stream.open(filepath);
			if (!stream.is_open())
			{
				Debug::write(EDebugLevel::Error, String::format("font: could not find file %s", filepath));
				return false;
			}

			std::string line;
			std::string read;
			std::string key;
			std::string val;
			std::stringstream converter;
			std::size_t i;

			while (!stream.eof())
			{
				std::stringstream lineStream;
				std::getline(stream, line);
				lineStream << line;

				// read the lines type
				lineStream >> read;
				if (read == "info")
				{

				}
				else if (read == "common")
				{
					while (!lineStream.eof())
					{
						converter.clear();
						lineStream >> read;

						i = read.find("=");
						key = read.substr(0, i);
						val = read.substr(i + 1);

						converter << val;
						if (key == "lineHeight")
							converter >> m_lineHeight;
						else if (key == "base")
							converter >> m_base;
						else if (key == "scaleW")
							converter >> m_scaleW;
						else if (key == "scaleH")
							converter >> m_scaleH;
						else if (key == "pages")
							converter >> m_numPages;
						else if (key == "packed")
							converter >> m_packed;
					}
				}
				else if (read == "page")
				{
					std::size_t pageIndex = 0;

					while (!lineStream.eof())
					{
						converter.clear();
						lineStream >> read;

						i = read.find("=");
						key = read.substr(0, i);
						val = read.substr(i + 1);

						converter << val;
						if (key == "id")
							converter >> pageIndex;
						else if (key == "file")
						{
							converter >> m_pages[(const unsigned int)pageIndex].filename;
							m_pages[(const unsigned int)pageIndex].filename = m_pages[(const unsigned int)pageIndex].filename.substr(1, m_pages[(const unsigned int)pageIndex].filename.length() - 2);
						}
					}
				}
				else if (read == "char")
				{
					std::size_t glyphIndex = 0;

					while (!lineStream.eof())
					{
						converter.clear();
						lineStream >> read;
						i = read.find("=");
						key = read.substr(0, i);
						val = read.substr(i + 1);

						converter << val;
						if (key == "id")
						{
							converter >> glyphIndex;
							m_glyphs[(const unsigned int)glyphIndex].id = (uint32_t)glyphIndex;
						}
						else if (key == "x")
							converter >> m_glyphs[(const unsigned int)glyphIndex].x;
						else if (key == "y")
							converter >> m_glyphs[(const unsigned int)glyphIndex].y;
						else if (key == "width")
							converter >> m_glyphs[(const unsigned int)glyphIndex].width;
						else if (key == "height")
							converter >> m_glyphs[(const unsigned int)glyphIndex].height;
						else if (key == "xoffset")
							converter >> m_glyphs[(const unsigned int)glyphIndex].xOffset;
						else if (key == "yoffset")
							converter >> m_glyphs[(const unsigned int)glyphIndex].yOffset;
						else if (key == "xadvance")
							converter >> m_glyphs[(const unsigned int)glyphIndex].xAdvance;
						else if (key == "page")
							converter >> m_glyphs[(const unsigned int)glyphIndex].page;
						else if (key == "chnl")
							converter >> m_glyphs[(const unsigned int)glyphIndex].chnl;
					}
				}
				else if (read == "kerning")
				{
					Kerning kerning;

					while (!lineStream.eof())
					{
						converter.clear();
						lineStream >> read;
						i = read.find("=");
						key = read.substr(0, i);
						val = read.substr(i + 1);

						converter << val;
						if (key == "first")
							converter >> kerning.first;
						else if (key == "second")
							converter >> kerning.second;
						else if (key == "amount")
							converter >> kerning.amount;
					}

					m_kernings.push_back(kerning);
				}
			}

			stream.close();
			return true;
		}

		bool Font::hasGlyph(uint8_t ascii) const
		{
			auto it = m_glyphs.begin();
			while (it != m_glyphs.end())
			{
				if (it->first == ascii)
					return true;
				++it;
			}
			return false;
		}

		Glyph* Font::getGlyph(uint8_t ascii)
		{
			auto it = m_glyphs.begin();
			while (it != m_glyphs.end())
			{
				if (it->first == ascii)
					return &it->second;
				++it;
			}
			return nullptr;
		}

		void Font::retrieveGlyphs(const std::string& string, std::vector<Glyph*>& glyphs)
		{
			for (std::size_t i = 0; i < string.length(); ++i)
			{
				Glyph* glyph = getGlyph(static_cast<uint8_t>(string[i]));
				if (glyph != nullptr)
				{
					glyphs.push_back(glyph);
				}
			}
		}

		float Font::getKerning(uint8_t first, uint8_t second) const
		{
			for (std::size_t i = 0; i < m_kernings.size(); ++i)
			{
				if (m_kernings[i].first == first || m_kernings[i].second == second)
				{
					return m_kernings[i].amount;
				}
			}
			return 0.f;
		}

		std::vector<Page*> Font::getPages()
		{
			std::vector<Page*> pages;
			for (unsigned i = 0; i < m_pages.size(); ++i)
			{
				pages.push_back(&m_pages[i]);
			}
			return pages;
		}
	}
}
