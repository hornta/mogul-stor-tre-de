// texture.cpp

#include "stdafx.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/resource/texture.hpp>

namespace helium
{
	namespace resource
	{
		static system::ETextureFormat get_format_from_components(int components)
		{
			switch (components)
			{
			case 0:
				Debug::fatal("texture: invalid texture format");
				break;
			case 1:
				return system::ETextureFormat::R8;
				break;
			case 2:
				return system::ETextureFormat::R8G8;
				break;
			case 3:
				return system::ETextureFormat::R8G8B8;
				break;
			case 4:
				return system::ETextureFormat::R8G8B8A8;
				break;
			}
			return system::ETextureFormat::Invalid;
		}

		Texture::Texture()
		{
			m_format = system::ETextureFormat::Invalid;
			m_type = system::ETextureType::Invalid;
			m_width = 0;
			m_height = 0;
			m_depth = 0;
			m_mipmaps = 0;
			m_pixels = nullptr;
			m_stbi = false;
		}

		// public
		Texture::~Texture()
		{
			m_format = system::ETextureFormat::Invalid;
			m_type = system::ETextureType::Invalid;
			m_width = 0;
			m_height = 0;
			m_depth = 0;
			if (m_pixels)
			{
				for (unsigned i = 0; i < m_mipmaps; i++)
				{
					if (m_stbi)
						stbi_image_free(m_pixels[i]);
					else
						delete[] m_pixels[i];
					m_pixels[i] = nullptr;
				}
				delete[] m_pixels;
				m_pixels = nullptr;
			}
			m_mipmaps = 0;
			m_stbi = false;
		}

		system::ETextureType Texture::get_type() const
		{
			return m_type;
		}

		bool Texture::loadTexture(const std::string& filename)
		{
			FILE* file = nullptr;
			file = fopen(filename.c_str(), "rb");
			if (!file)
			{
				Debug::write(EDebugLevel::Error, String::format("texture: could not find file %s", filename.c_str()));
				return false;
			}

			int width = 0, height = 0, components = 0;
			uint8_t* pixels = stbi_load_from_file(file, &width, &height, &components, 0);
			fclose(file);

			Debug::write(EDebugLevel::Info, String::format("texture: %s", filename.c_str()));

			m_format = get_format_from_components(components);
			m_type = ((width > 0 && height == 1) ? system::ETextureType::Texture1D : system::ETextureType::Texture2D);
			m_width = width;
			m_height = height;
			m_mipmaps = 1;
			m_pixels = new uint8_t*[1];
			m_pixels[0] = pixels;
			m_stbi = true;
			return true;
		}

		bool Texture::has_mipmaps() const
		{
			return m_mipmaps > 1;
		}

		system::ETextureFormat Texture::get_format() const
		{
			return m_format;
		}

		uint16_t Texture::get_width() const
		{
			return m_width;
		}

		uint16_t Texture::get_height() const
		{
			return m_height;
		}

		uint16_t Texture::get_depth() const
		{
			return m_depth;
		}

		const uint8_t* Texture::get_pixel_data(uint32_t index) const
		{
			return m_pixels[index];
		}

		void Texture::dispose() 
		{
			if (m_id == (uint32_t)-1)
				return;
			ServiceLocator<system::RenderSystem>::get_service()->destroy_texture(m_id);
		}
	}
}
