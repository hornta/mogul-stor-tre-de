// sampler.cpp

#include "stdafx.h"
#include <cassert>
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/resource/sampler.hpp>

namespace helium
{
	namespace resource
	{
		static system::ESamplerFilterMode get_filter(std::string& filter)
		{
			std::transform(filter.begin(), filter.end(), filter.begin(), ::tolower);
			if (filter.compare("nearest") == 0)
				return system::ESamplerFilterMode::Nearest;
			else if (filter.compare("linear") == 0)
				return system::ESamplerFilterMode::Linear;
			return system::ESamplerFilterMode::Invalid;
		}

		static system::ESamplerAddressMode get_addrmode(std::string& addr)
		{
			std::transform(addr.begin(), addr.end(), addr.begin(), ::tolower);
			if (addr.compare("clamp") == 0)
				return system::ESamplerAddressMode::Clamp;
			else if (addr.compare("wrap") == 0)
				return system::ESamplerAddressMode::Wrap;
			else if (addr.compare("mirror") == 0)
				return system::ESamplerAddressMode::Mirror;
			return system::ESamplerAddressMode::Invalid;
		}

		// static 
		Sampler::Ptr Sampler::create(const std::string& filename)
		{
			std::ifstream stream;
			stream.open(filename.c_str());
			if (!stream.is_open())
			{
				Debug::write(EDebugLevel::Error, String::format("sampler: could not find file %s", filename.c_str()));
				return Sampler::Ptr(nullptr);
			}

			std::string filter;
			std::string addru, addrv, addrw;
			stream >> filter;
			stream >> addru;
			if (!stream.eof())
				stream >> addrv;
			if (!stream.eof())
				stream >> addrw;
			stream.close();

			Debug::write(EDebugLevel::Info, String::format("sampler: %s", filename.c_str()));

			Sampler* sampler = new Sampler;
			sampler->m_filter_mode = get_filter(filter);
			sampler->m_address_mode[0] = get_addrmode(addru);
			if (addrv.length() > 0)
				sampler->m_address_mode[1] = get_addrmode(addrv);
			if (addrw.length() > 0)
				sampler->m_address_mode[2] = get_addrmode(addrw);
			return Sampler::Ptr(sampler);
		}

		// private
		Sampler::Sampler()
		{
			m_filter_mode = system::ESamplerFilterMode::Invalid;
			m_address_mode[0] = system::ESamplerAddressMode::Invalid;
			m_address_mode[1] = system::ESamplerAddressMode::Invalid;
			m_address_mode[2] = system::ESamplerAddressMode::Invalid;
		}

		// public
		Sampler::~Sampler()
		{
		}

		bool Sampler::has_addr_u() const
		{
			return m_address_mode[0] != system::ESamplerAddressMode::Invalid;
		}

		bool Sampler::has_addr_v() const
		{
			return m_address_mode[1] != system::ESamplerAddressMode::Invalid;
		}

		bool Sampler::has_addr_w() const
		{
			return m_address_mode[2] != system::ESamplerAddressMode::Invalid;
		}

		system::ESamplerFilterMode Sampler::get_filter_mode() const
		{
			return m_filter_mode;
		}

		system::ESamplerAddressMode Sampler::get_address_mode(uint32_t index) const
		{
			assert(index > 3);
			return m_address_mode[index];
		}

		system::ESamplerAddressMode Sampler::get_addr_u() const
		{
			return m_address_mode[0];
		}

		system::ESamplerAddressMode Sampler::get_addr_v() const
		{
			return m_address_mode[0];
		}

		system::ESamplerAddressMode Sampler::get_addr_w() const
		{
			return m_address_mode[0];
		}

		void Sampler::dispose()
		{
			if (m_id == (uint32_t)-1)
				return;
			ServiceLocator<system::RenderSystem>::get_service()->destroy_sampler_state(m_id);
		}
	}
}
