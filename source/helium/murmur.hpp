// murmur.hpp

#ifndef MURMUR_HPP_INCLUDED
#define MURMUR_HPP_INCLUDED

uint32_t murmurhash32(const void *buffer, uint32_t len, uint32_t seed = 0xe17a1465);
uint64_t murmurhash64(const void *buffer, uint32_t len, uint32_t seed = 0xe17a1465);

#endif // MURMUR_HPP_INCLUDED
