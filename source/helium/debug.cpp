// debug.cpp

#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <ctime>
#include <chrono>
#include <cassert>
#include <helium/debug.hpp>

namespace helium
{
	static std::string timestamp()
	{
		static char tsmp[32];
		auto now = std::chrono::system_clock::now();
		auto s = std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
		//auto frac = (now - s).time_since_epoch().count();
		auto tt = std::chrono::system_clock::to_time_t(now);
		std::strftime(tsmp, sizeof(tsmp), "%H:%M:%S", std::localtime(&tt));
		//sprintf(tsmp + 8, ".%03d", (uint32_t)frac);
		return std::string(tsmp);
	}

	// static
	EDebugLevel Debug::ms_level = EDebugLevel::Info;

	void Debug::set_debug_level(EDebugLevel level)
	{
		ms_level = level;
	}

	void Debug::write(const EDebugLevel level, const std::string& message)
	{
		if ((uint32_t)level < (uint32_t)ms_level)
			return;

		std::string tsmp = timestamp();
		std::cerr << tsmp << "  " <<  message << std::endl;
		std::cout << tsmp << "  " << message << std::endl;
	}

	void Debug::fatal(const std::string& message)
	{
		std::string tsmp = timestamp();
		std::cerr << tsmp << "  " << message << std::endl;
		std::string msg = tsmp + " " + message;
		if (MessageBoxA(NULL, msg.c_str(), "FatalError!", MB_ICONERROR | MB_OK) == IDOK)
		{
			assert(false);
		}
	}

	// public
	Debug::Debug()
	{
		m_stream.open("debug.txt");
		m_old = std::cerr.rdbuf();
		std::cerr.rdbuf(m_stream.rdbuf());

		AllocConsole();
		HANDLE cout = GetStdHandle(STD_OUTPUT_HANDLE);
		int sout = _open_osfhandle((intptr_t)cout, _O_TEXT);
		FILE* fout = _fdopen(sout, "w");
		*stdout = *fout;
		setvbuf(stdout, NULL, _IONBF, 0);
		std::ios::sync_with_stdio(true);
	}

	Debug::~Debug()
	{
		FreeConsole();

		m_stream.flush();
		m_stream.close();
		std::cerr.rdbuf(m_old);
	}
}
