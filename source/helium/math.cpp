// math_util.cpp

#include "stdafx.h"
#include <cmath>
#include <limits>
#include <helium/math.hpp>

namespace helium
{
	const float Math::PI = 3.14159265359f;
	const float Math::PI2 = 6.28318530718f;

	float Math::sqrt(float value)
	{
		return std::sqrtf(value);
	}

	float Math::sin(float radian)
	{
		return std::sinf(radian);
	}

	float Math::cos(float radian)
	{
		return std::cosf(radian);
	}

	float Math::tan(float radian)
	{
		return std::tanf(radian);
	}

	float Math::to_rad(float degrees)
	{
		return degrees * PI / 180.0f;
	}

	float Math::to_deg(float radians)
	{
		return radians * 180.0f / PI;
	}

	template <>
	bool Math::are_equal<float>(const float& a, const float& b)
	{
		return std::fabs(a - b) < std::numeric_limits<float>::epsilon();
	}

	template <>
	bool Math::are_equal<double>(const double& a, const double& b)
	{
		return std::abs(a - b) < std::numeric_limits<double>::epsilon();
	}


	Matrix4 Math::makeViewMatrix(const Vector3& position, const Quaternion& orientation)
	{
		Matrix4 rot = Matrix4::IDENTITY;
		rot.makeRotationFromQuaternion(orientation);

		Matrix4 rotT = Matrix4::transpose(rot);
		Vector3 trans = -rotT * position;

		// Make final matrix
		Matrix4 viewMatrix = Matrix4::IDENTITY;
		viewMatrix = rotT;
		viewMatrix.m._41 = trans.m_x;
		viewMatrix.m._42 = trans.m_y;
		viewMatrix.m._43 = trans.m_z;

		return viewMatrix;
	}

	const Vector2 Vector2::UP(0.f, 1.f);
	const Vector2 Vector2::ZERO(0.f, 0.f);

	Vector2::Vector2()
		: m_x(0.0f)
		, m_y(0.0f)
	{
	}

	Vector2::Vector2(float x, float y)
		: m_x(x)
		, m_y(y)
	{
	}

	Vector2::Vector2(const Vector2& rhs)
		: m_x(rhs.m_x)
		, m_y(rhs.m_y)
	{
	}

	Vector2& Vector2::operator=(const Vector2& rhs)
	{
		m_x = rhs.m_x;
		m_y = rhs.m_y;
		return *this;
	}

	Vector2& Vector2::operator+=(const Vector2& rhs)
	{
		m_x += rhs.m_x;
		m_y += rhs.m_y;
		return *this;
	}

	Vector2& Vector2::operator-=(const Vector2& rhs)
	{
		m_x -= rhs.m_x;
		m_y -= rhs.m_y;
		return *this;
	}

	Vector2& Vector2::operator*=(const float real)
	{
		m_x *= real;
		m_y *= real;
		return *this;
	}

	Vector2& Vector2::operator/=(const float real)
	{
		m_x /= real;
		m_y /= real;
		return *this;
	}

	Vector2 Vector2::operator+(const Vector2& rhs)
	{
		return Vector2(m_x + rhs.m_x, m_y + rhs.m_y);
	}

	Vector2 Vector2::operator-(const Vector2& rhs)
	{
		return Vector2(m_x - rhs.m_x, m_y - rhs.m_y);
	}

	Vector2 Vector2::operator*(const float real)
	{
		return Vector2(m_x * real, m_y * real);
	}

	Vector2 Vector2::operator/(const float real)
	{
		return Vector2(m_x / real, m_y / real);
	}

	float Vector2::length() const
	{
		return Math::sqrt(m_x * m_x + m_y * m_y);
	}

	float Vector2::length_squared() const
	{
		return m_x * m_x + m_y * m_y;
	}

	float Vector2::dot(const Vector2& rhs) const
	{
		return m_x * rhs.m_x + m_y * rhs.m_y;
	}

	void Vector2::normalize()
	{
		float len = length();
		if (len > 0.0f)
		{
			float inv = 1.0f / len;
			m_x *= inv;
			m_y *= inv;
		}
	}

	const Vector3 Vector3::Up(0, 1, 0);
	const Vector3 Vector3::UP(0, 1, 0);
	const Vector3 Vector3::RIGHT(1, 0, 0);
	const Vector3 Vector3::FORWARD(0, 0, 1);
	const Vector3 Vector3::ZERO(0, 0, 0);
	const Vector3 Vector3::UNIT_SCALE(1, 1, 1);

	Vector3::Vector3()
		: m_x(0.0f)
		, m_y(0.0f)
		, m_z(0.0f)
	{
	}

	Vector3::Vector3(float x, float y, float z)
		: m_x(x)
		, m_y(y)
		, m_z(z)
	{
	}

	Vector3::Vector3(const Vector3& rhs)
		: m_x(rhs.m_x)
		, m_y(rhs.m_y)
		, m_z(rhs.m_z)
	{
	}

	Vector3& Vector3::operator=(const Vector3& rhs)
	{
		m_x = rhs.m_x;
		m_y = rhs.m_y;
		m_z = rhs.m_z;
		return *this;
	}

	Vector3& Vector3::operator+=(const Vector3& rhs)
	{
		m_x += rhs.m_x;
		m_y += rhs.m_y;
		m_z += rhs.m_z;
		return *this;
	}

	Vector3& Vector3::operator-=(const Vector3& rhs)
	{
		m_x -= rhs.m_x;
		m_y -= rhs.m_y;
		m_z -= rhs.m_z;
		return *this;
	}

	Vector3& Vector3::operator*=(const float real)
	{
		m_x *= real;
		m_y *= real;
		m_z *= real;
		return *this;
	}

	Vector3& Vector3::operator*=(const Vector3& rhs)
	{
		m_x *= rhs.m_x;
		m_y *= rhs.m_y;
		m_z *= rhs.m_z;
		return *this;
	}

	Vector3& Vector3::operator/=(const float real)
	{
		m_x /= real;
		m_y /= real;
		m_z /= real;
		return *this;
	}

	Vector3& Vector3::operator/=(const Vector3& rhs)
	{
		m_x /= rhs.m_x;
		m_y /= rhs.m_y;
		m_z /= rhs.m_z;
		return *this;
	}

	const Vector3 Vector3::operator+(const Vector3& rhs) const
	{
		return Vector3(m_x + rhs.m_x, m_y + rhs.m_y, m_z + rhs.m_z);
	}

	Vector3 Vector3::operator+(const Vector3&rhs)
	{
		return Vector3(m_x + rhs.m_x, m_y + rhs.m_y, m_z + rhs.m_z);
	}

	Vector3 Vector3::operator-(const Vector3&rhs) const
	{
		return Vector3(m_x - rhs.m_x, m_y - rhs.m_y, m_z - rhs.m_z);
	}

	Vector3 Vector3::operator*(const float real)
	{
		return Vector3(m_x * real, m_y * real, m_z * real);
	}

	Vector3 Vector3::operator*(const Vector3& rhs) const
	{
		return Vector3(m_x * rhs.m_x, m_y * rhs.m_y, m_z * rhs.m_z);
	}

	Vector3 Vector3::operator/(const float real) const
	{
		return Vector3(m_x / real, m_y / real, m_z / real);
	}

	const Vector3 Vector3::operator*(const float real) const
	{
		return Vector3(m_x * real, m_y * real, m_z * real);
	}

	Vector3 Vector3::min(const Vector3& left, const Vector3& right)
	{
		float x = left.m_x < right.m_x ? left.m_x : right.m_x;
		float y = left.m_y < right.m_y ? left.m_y : right.m_y;
		float z = left.m_z < right.m_z ? left.m_z : right.m_z;
		return Vector3(x, y, z);
	}

	Vector3 Vector3::max(const Vector3& left, const Vector3& right)
	{
		float x = left.m_x > right.m_x ? left.m_x : right.m_x;
		float y = left.m_y > right.m_y ? left.m_y : right.m_y;
		float z = left.m_z > right.m_z ? left.m_z : right.m_z;
		return Vector3(x, y, z);
	}

	Vector3 Vector3::normalize(const Vector3& v)
	{
		Vector3 result = v;
		float len = v.length();
		if (len > 0.0f)
		{
			float inv = 1.0f / len;
			result.m_x *= inv;
			result.m_y *= inv;
			result.m_z *= inv;
		}
		return result;
	}

	Vector3 Vector3::cross(const Vector3& rhs)
	{
		return Vector3(
			m_y * rhs.m_z - m_z * rhs.m_y,
			m_z * rhs.m_x - m_x * rhs.m_z,
			m_x * rhs.m_y - m_y * rhs.m_x);
	}

	void Vector3::crossVectors(const Vector3& l, const Vector3& r)
	{
		float ax = l.m_x;
		float ay = l.m_y;
		float az = l.m_z;

		float bx = r.m_x;
		float by = r.m_y;
		float bz = r.m_z;

		m_x = ay * bz - az * by;
		m_y = az * bx - ax * bz;
		m_z = ax * by - ay * bx;
	}

	void Vector3::subVectors(const Vector3&l, const Vector3& r)
	{
		m_x = l.m_x - r.m_x;
		m_y = l.m_y - r.m_y;
		m_z = l.m_z - r.m_z;
	}

	void Vector3::divideScalar(float scalar)
	{
		float invScalar = 1.f / scalar;
		m_x *= invScalar;
		m_y *= invScalar;
		m_z *= invScalar;
	}

	void Vector3::normalize()
	{
		divideScalar(length());
	}

	float Vector3::length() const
	{
		return Math::sqrt(m_x * m_x + m_y * m_y + m_z * m_z);
	}

	float Vector3::length_squared() const
	{
		return m_x * m_x + m_y * m_y + m_z * m_z;
	}

	float Vector3::dot(const Vector3& rhs) const
	{
		return m_x * rhs.m_x + m_y * rhs.m_y + m_z * rhs.m_z;
	}


		Vector4::Vector4()
		: m_x(0.0f)
		, m_y(0.0f)
		, m_z(0.0f)
		, m_w(0.0f)
	{
	}

	Vector4::Vector4(float x, float y, float z, float w)
		: m_x(x)
		, m_y(y)
		, m_z(z)
		, m_w(w)
	{
	}

	Vector4::Vector4(const Vector4& rhs)
		: m_x(rhs.m_x)
		, m_y(rhs.m_y)
		, m_z(rhs.m_z)
		, m_w(rhs.m_w)
	{
	}


	Vector4::Vector4(const Vector3& rhs, float w)
		: m_x(rhs.m_x)
		, m_y(rhs.m_y)
		, m_z(rhs.m_z)
		, m_w(w)
	{
		
	}

	Vector4& Vector4::operator=(const Vector4& rhs)
	{
		m_x = rhs.m_x;
		m_y = rhs.m_y;
		m_z = rhs.m_z;
		m_w = rhs.m_w;
		return *this;
	}

	Matrix4::Matrix4()
	{
		m._11 = m._12 = m._13 = m._14 = 0.0f;
		m._21 = m._22 = m._23 = m._24 = 0.0f;
		m._31 = m._32 = m._33 = m._34 = 0.0f;
		m._41 = m._42 = m._43 = m._44 = 0.0f;
	}

	Matrix4::Matrix4(float _11, float _12, float _13, float _14,
		float _21, float _22, float _23, float _24,
		float _31, float _32, float _33, float _34,
		float _41, float _42, float _43, float _44)
	{
		m._11 = _11; m._12 = _12; m._13 = _13; m._14 = _14;
		m._21 = _21; m._22 = _22; m._23 = _23; m._24 = _24;
		m._31 = _31; m._32 = _32; m._33 = _33; m._34 = _34;
		m._41 = _41; m._42 = _42; m._43 = _43; m._44 = _44;
	}

	Matrix4::Matrix4(const Matrix4& rhs)
	{
		for (unsigned i = 0; i < 16; i++)
			_m[i] = rhs._m[i];
	}

	Matrix4& Matrix4::operator=(const Matrix4& rhs)
	{
		for (unsigned i = 0; i < 16; i++)
			_m[i] = rhs._m[i];
		return *this;
	}

	Matrix4 Matrix4::operator*(const Matrix4 &rhs)
	{
		Matrix4 r;
		r.m._11 = m._11*rhs.m._11 + m._12*rhs.m._21 + m._13*rhs.m._31 + m._14*rhs.m._41;
		r.m._12 = m._11*rhs.m._12 + m._12*rhs.m._22 + m._13*rhs.m._32 + m._14*rhs.m._42;
		r.m._13 = m._11*rhs.m._13 + m._12*rhs.m._23 + m._13*rhs.m._33 + m._14*rhs.m._43;
		r.m._14 = m._11*rhs.m._14 + m._12*rhs.m._24 + m._13*rhs.m._34 + m._14*rhs.m._44;

		r.m._21 = m._21*rhs.m._11 + m._22*rhs.m._21 + m._23*rhs.m._31 + m._24*rhs.m._41;
		r.m._22 = m._21*rhs.m._12 + m._22*rhs.m._22 + m._23*rhs.m._32 + m._24*rhs.m._42;
		r.m._23 = m._21*rhs.m._13 + m._22*rhs.m._23 + m._23*rhs.m._33 + m._24*rhs.m._43;
		r.m._24 = m._21*rhs.m._14 + m._22*rhs.m._24 + m._23*rhs.m._34 + m._24*rhs.m._44;

		r.m._31 = m._31*rhs.m._11 + m._32*rhs.m._21 + m._33*rhs.m._31 + m._34*rhs.m._41;
		r.m._32 = m._31*rhs.m._12 + m._32*rhs.m._22 + m._33*rhs.m._32 + m._34*rhs.m._42;
		r.m._33 = m._31*rhs.m._13 + m._32*rhs.m._23 + m._33*rhs.m._33 + m._34*rhs.m._43;
		r.m._34 = m._31*rhs.m._14 + m._32*rhs.m._24 + m._33*rhs.m._34 + m._34*rhs.m._44;

		r.m._41 = m._41*rhs.m._11 + m._42*rhs.m._21 + m._43*rhs.m._31 + m._44*rhs.m._41;
		r.m._42 = m._41*rhs.m._12 + m._42*rhs.m._22 + m._43*rhs.m._32 + m._44*rhs.m._42;
		r.m._43 = m._41*rhs.m._13 + m._42*rhs.m._23 + m._43*rhs.m._33 + m._44*rhs.m._43;
		r.m._44 = m._41*rhs.m._14 + m._42*rhs.m._24 + m._43*rhs.m._34 + m._44*rhs.m._44;
		return r;
	}

	const Matrix4 Matrix4::operator*(const Matrix4 &rhs) const
	{
		Matrix4 r;
		r.m._11 = m._11*rhs.m._11 + m._12*rhs.m._21 + m._13*rhs.m._31 + m._14*rhs.m._41;
		r.m._12 = m._11*rhs.m._12 + m._12*rhs.m._22 + m._13*rhs.m._32 + m._14*rhs.m._42;
		r.m._13 = m._11*rhs.m._13 + m._12*rhs.m._23 + m._13*rhs.m._33 + m._14*rhs.m._43;
		r.m._14 = m._11*rhs.m._14 + m._12*rhs.m._24 + m._13*rhs.m._34 + m._14*rhs.m._44;

		r.m._21 = m._21*rhs.m._11 + m._22*rhs.m._21 + m._23*rhs.m._31 + m._24*rhs.m._41;
		r.m._22 = m._21*rhs.m._12 + m._22*rhs.m._22 + m._23*rhs.m._32 + m._24*rhs.m._42;
		r.m._23 = m._21*rhs.m._13 + m._22*rhs.m._23 + m._23*rhs.m._33 + m._24*rhs.m._43;
		r.m._24 = m._21*rhs.m._14 + m._22*rhs.m._24 + m._23*rhs.m._34 + m._24*rhs.m._44;

		r.m._31 = m._31*rhs.m._11 + m._32*rhs.m._21 + m._33*rhs.m._31 + m._34*rhs.m._41;
		r.m._32 = m._31*rhs.m._12 + m._32*rhs.m._22 + m._33*rhs.m._32 + m._34*rhs.m._42;
		r.m._33 = m._31*rhs.m._13 + m._32*rhs.m._23 + m._33*rhs.m._33 + m._34*rhs.m._43;
		r.m._34 = m._31*rhs.m._14 + m._32*rhs.m._24 + m._33*rhs.m._34 + m._34*rhs.m._44;

		r.m._41 = m._41*rhs.m._11 + m._42*rhs.m._21 + m._43*rhs.m._31 + m._44*rhs.m._41;
		r.m._42 = m._41*rhs.m._12 + m._42*rhs.m._22 + m._43*rhs.m._32 + m._44*rhs.m._42;
		r.m._43 = m._41*rhs.m._13 + m._42*rhs.m._23 + m._43*rhs.m._33 + m._44*rhs.m._43;
		r.m._44 = m._41*rhs.m._14 + m._42*rhs.m._24 + m._43*rhs.m._34 + m._44*rhs.m._44;
		return r;
	}

	Matrix4 Matrix4::operator-() const
	{
		Matrix4 m;
		for (uint32_t i = 0; i < 16; i++)
		{
			m._m[i] = -1 * _m[i];
		}
		return m;
	}

	Vector3 Matrix4::operator*(const Vector3&rhs)
	{
		return Vector3(rhs.m_x * m._11 + rhs.m_y * m._12 + rhs.m_z * m._13,
			rhs.m_x * m._21 + rhs.m_y * m._22 + rhs.m_z * m._23,
			rhs.m_x * m._31 + rhs.m_y * m._32 + rhs.m_z * m._33);
	}

	void Matrix4::identity()
	{
		m._12 = m._13 = m._14 = 0.0f;
		m._21 = m._23 = m._24 = 0.0f;
		m._31 = m._32 = m._34 = 0.0f;
		m._41 = m._42 = m._43 = 0.0f;
		m._11 = m._22 = m._33 = m._44 = 1.0f;
	}

	Matrix4 Matrix4::inverse()
	{
		float a0 = _m[0] * _m[5] - _m[1] * _m[4];
		float a1 = _m[0] * _m[6] - _m[2] * _m[4];
		float a2 = _m[0] * _m[7] - _m[3] * _m[4];
		float a3 = _m[1] * _m[6] - _m[2] * _m[5];
		float a4 = _m[1] * _m[7] - _m[3] * _m[5];
		float a5 = _m[2] * _m[7] - _m[3] * _m[6];
		float b0 = _m[8] * _m[13] - _m[9] * _m[12];
		float b1 = _m[8] * _m[14] - _m[10] * _m[12];
		float b2 = _m[8] * _m[15] - _m[11] * _m[12];
		float b3 = _m[9] * _m[14] - _m[10] * _m[13];
		float b4 = _m[9] * _m[15] - _m[11] * _m[13];
		float b5 = _m[10] * _m[15] - _m[11] * _m[14];
		float det = a0*b5 - a1*b4 + a2*b3 + a3*b2 - a4*b1 + a5*b0;
		if (Math::abs(det) > 0.0000001f)
		{
			Matrix4 inv;
			inv._m[0] = +_m[5] * b5 - _m[6] * b4 + _m[7] * b3;
			inv._m[4] = -_m[4] * b5 + _m[6] * b2 - _m[7] * b1;
			inv._m[8] = +_m[4] * b4 - _m[5] * b2 + _m[7] * b0;
			inv._m[12] = -_m[4] * b3 + _m[5] * b1 - _m[6] * b0;
			inv._m[1] = -_m[1] * b5 + _m[2] * b4 - _m[3] * b3;
			inv._m[5] = +_m[0] * b5 - _m[2] * b2 + _m[3] * b1;
			inv._m[9] = -_m[0] * b4 + _m[1] * b2 - _m[3] * b0;
			inv._m[13] = +_m[0] * b3 - _m[1] * b1 + _m[2] * b0;
			inv._m[2] = +_m[13] * a5 - _m[14] * a4 + _m[15] * a3;
			inv._m[6] = -_m[12] * a5 + _m[14] * a2 - _m[15] * a1;
			inv._m[10] = +_m[12] * a4 - _m[13] * a2 + _m[15] * a0;
			inv._m[14] = -_m[12] * a3 + _m[13] * a1 - _m[14] * a0;
			inv._m[3] = -_m[9] * a5 + _m[10] * a4 - _m[11] * a3;
			inv._m[7] = +_m[8] * a5 - _m[10] * a2 + _m[11] * a1;
			inv._m[11] = -_m[8] * a4 + _m[9] * a2 - _m[11] * a0;
			inv._m[15] = +_m[8] * a3 - _m[9] * a1 + _m[10] * a0;

			float inv_det = 1.0f / det;
			inv._m[0] *= inv_det;
			inv._m[1] *= inv_det;
			inv._m[2] *= inv_det;
			inv._m[3] *= inv_det;
			inv._m[4] *= inv_det;
			inv._m[5] *= inv_det;
			inv._m[6] *= inv_det;
			inv._m[7] *= inv_det;
			inv._m[8] *= inv_det;
			inv._m[9] *= inv_det;
			inv._m[10] *= inv_det;
			inv._m[11] *= inv_det;
			inv._m[12] *= inv_det;
			inv._m[13] *= inv_det;
			inv._m[14] *= inv_det;
			inv._m[15] *= inv_det;

			return inv;
		};
		return Matrix4(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	}

	void Matrix4::compose(const Vector3& position, const Quaternion& quaternion, const Vector3& scale = Vector3(1, 1, 1))
	{
		makeRotationFromQuaternion(quaternion);
		setScale(scale);
		setPosition(position);
	}

	void Matrix4::makeRotationFromQuaternion(const Quaternion& q)
	{
		float x = q.x;
		float y = q.y;
		float z = q.z;
		float w = q.w;

		float x2 = x + x;
		float y2 = y + y;
		float z2 = z + z;

		float xx = x * x2;
		float xy = x * y2;
		float xz = x * z2;

		float yy = y * y2;
		float yz = y * z2;
		float zz = z * z2;

		float wx = w * x2;
		float wy = w * y2;
		float wz = w * z2;

		m._11 = 1 - (zz + yy);
		m._21 = xy - wz;
		m._31 = xz + wy;

		m._12 = xy + wz;
		m._22 = 1 - (zz + xx);
		m._32 = yz - wx;

		m._13 = xz - wy;
		m._23 = yz + wx;
		m._33 = 1 - (xx + yy);
	}

	void Matrix4::setPosition(const Vector3& p)
	{
		m._41 = p.m_x;
		m._42 = p.m_y;
		m._43 = p.m_z;
	}

	void Matrix4::setScale(const Vector3& s)
	{
		float x = s.m_x;
		float y = s.m_y;
		float z = s.m_z;

		m._11 *= x;
		m._12 *= x;
		m._13 *= x;

		m._21 *= y;
		m._22 *= y;
		m._23 *= y;

		m._31 *= z;
		m._32 *= z;
		m._33 *= z;
	}


	void Matrix4::lookAt(const Vector3& position, const Vector3& target, const Vector3& up)
	{
		Vector3 x;
		Vector3 y;
		Vector3 z;

		z.subVectors(position, target);
		z.normalize();

		x.crossVectors(up, z);
		x.normalize();

		y.crossVectors(z, x);

		m._11 = x.m_x;
		m._12 = x.m_y;
		m._13 = x.m_z;

		m._21 = y.m_x;
		m._22 = y.m_y;
		m._23 = y.m_z;

		m._31 = z.m_x;
		m._32 = z.m_y;
		m._33 = z.m_z;
	}

	const Matrix4 Matrix4::IDENTITY = Matrix4(1.f, 0.f, 0.f, 0.f,
		                                      0.f, 1.f, 0.f, 0.f,
											  0.f, 0.f, 1.f, 0.f,
											  0.f, 0.f, 0.f, 1.f);

	Matrix4 Matrix4::scale(float x, float y, float z)
	{
		return Matrix4(
			x, 0.0f, 0.0f, 0.0f,
			0.0f, y, 0.0f, 0.0f,
			0.0f, 0.0f, z, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
	}

	Matrix4 Matrix4::scale(const Vector3 &v)
	{
		return Matrix4(
			v.m_x, 0.0f, 0.0f, 0.0f,
			0.0f, v.m_y, 0.0f, 0.0f,
			0.0f, 0.0f, v.m_z, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f);
	}

	Matrix4 Matrix4::rotation(const Quaternion& quat)
	{
		Matrix4 mat;
		mat.makeRotationFromQuaternion(quat);
		return mat;
	}

	Matrix4 Matrix4::rotation(const Vector3 &axis, float radians)
	{
		float c = Math::cos(radians);
		float s = Math::sin(radians);
		float xx = axis.m_x*axis.m_x;
		float xy = axis.m_x*axis.m_y;
		float xz = axis.m_x*axis.m_z;
		float yy = axis.m_y*axis.m_y;
		float yz = axis.m_y*axis.m_z;
		float zz = axis.m_z*axis.m_z;

		Matrix4 r;
		r.identity();
		r.m._11 = xx*(1.0f - c) + c;
		r.m._12 = xy*(1.0f - c) - axis.m_z*s;
		r.m._13 = xz*(1.0f - c) + axis.m_y*s;
		r.m._21 = xy*(1.0f - c) + axis.m_z*s;
		r.m._22 = yy*(1.0f - c) + c;
		r.m._23 = yz*(1.0f - c) - axis.m_x*s;
		r.m._31 = xz*(1.0f - c) - axis.m_y*s;
		r.m._32 = yz*(1.0f - c) + axis.m_x*s;
		r.m._33 = zz*(1.0f - c) + c;
		return r;
	}

	Matrix4 Matrix4::rotation(float x, float y, float z, float radians)
	{
		float c = Math::cos(radians);
		float s = Math::sin(radians);
		float xx = x * x;
		float xy = x * y;
		float xz = x * z;
		float yy = y * y;
		float yz = y * z;
		float zz = z * z;

		Matrix4 r;
		r.identity();
		r.m._11 = xx * (1.0f - c) + c;
		r.m._12 = xy * (1.0f - c) - z * s;
		r.m._13 = xz * (1.0f - c) + y * s;
		r.m._21 = xy * (1.0f - c) + z * s;
		r.m._22 = yy * (1.0f - c) + c;
		r.m._23 = yz * (1.0f - c) - x * s;
		r.m._31 = xz * (1.0f - c) - y * s;
		r.m._32 = yz * (1.0f - c) + x * s;
		r.m._33 = zz * (1.0f - c) + c;
		return r;
	}

	Matrix4 Matrix4::translation(float x, float y, float z)
	{
		return Matrix4(
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			x, y, z, 1.0f);
	}

	Matrix4 Matrix4::translation(const Vector3 &v)
	{
		return Matrix4(
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			v.m_x, v.m_y, v.m_z, 1.0f);
	}

	Matrix4 Matrix4::perspective(float fov, float aspect, float znear, float zfar)
	{
		Matrix4 r;
		r.identity();
		float h = 1.0f / Math::tan(fov * 0.5f);
		float w = h / aspect;
		r.m._11 = w;
		r.m._22 = h;
		r.m._33 = zfar / (zfar - znear);
		r.m._34 = 1.0f;
		r.m._43 = (-znear * zfar) / (zfar - znear);
		r.m._44 = 0.0f;
		return r;
	}

	Matrix4 Matrix4::orthogonal(float width, float height, float znear, float zfar)
	{
		Matrix4 r;
		r.identity();
		r.m._11 = 2.0f / (width);
		r.m._22 = 2.0f / (-height);
		r.m._33 = 1.0f / (zfar - znear);
		r.m._41 = -1.0f; // (width) / (-width);
		r.m._42 = 1.0f; // height / height;
		r.m._43 = znear / (znear - zfar);
		return r;
	}

	Matrix4 Matrix4::transpose(const Matrix4& m)
	{
		Matrix4 mat = Matrix4::IDENTITY;
		for (uint32_t i = 0; i < 16; ++i)
		{
			int k = i / 4;
			int j = fmod(i, 4);

			mat._m[i] = m._m[4 * j + k];
		}
		return mat;
	}

	Ray::Ray()
	{
	}

	Ray::Ray(const Vector3& origin, const Vector3& direction)
		: m_origin(origin)
		, m_direction(direction)
	{
	}

	Ray::Ray(const Ray& rhs)
		: m_origin(rhs.m_origin)
		, m_direction(rhs.m_direction)
	{
	}

	Ray& Ray::operator=(const Ray& rhs)
	{
		m_origin = rhs.m_origin;
		m_direction = rhs.m_direction;
		return *this;
	}

	Plane::Plane()
		: m_D(0.0f)
	{
	}

	Plane::Plane(const Plane& rhs)
		: m_normal(rhs.m_normal)
		, m_D(rhs.m_D)
	{
	}

	Plane::Plane(const Vector3& normal, const float D)
		: m_normal(normal)
		, m_D(D)
	{
	}

	Plane& Plane::operator=(const Plane& rhs)
	{
		m_normal = rhs.m_normal;
		m_D = rhs.m_D;
		return *this;
	}

	BoundingSphere::BoundingSphere()
	{
	}

	BoundingSphere::BoundingSphere(const Vector3& center, float radius)
	{
		m_center = center;
		m_radius = radius;
	}

	BoundingSphere::BoundingSphere(const BoundingSphere& rhs)
	{
		m_center = rhs.m_center;
		m_radius = rhs.m_radius;
	}

	BoundingSphere& BoundingSphere::operator=(const BoundingSphere& rhs)
	{
		m_center = rhs.m_center;
		m_radius = rhs.m_radius;
		return *this;
	}

	AxisAlignedBoundingBox::AxisAlignedBoundingBox()
	{
	}

	AxisAlignedBoundingBox::AxisAlignedBoundingBox(const AxisAlignedBoundingBox& rhs)
	{
		m_min = rhs.m_min;
		m_max = rhs.m_max;
	}

	AxisAlignedBoundingBox& AxisAlignedBoundingBox::operator=(const AxisAlignedBoundingBox& rhs)
	{
		m_min = rhs.m_min;
		m_max = rhs.m_max;
		return *this;
	}

	void AxisAlignedBoundingBox::setFromPoints(const Vertex* p, int numPoints)
	{
		for (int i = 0; i < numPoints; ++i)
		{
			m_min = Vector3::min(m_min, p[i].position);
			m_max = Vector3::max(m_max, p[i].position);
		}

		if (fabs(m_min.m_x - m_max.m_x) < FLT_EPSILON)
			m_max.m_x += 0.01f;

		if (fabs(m_min.m_y - m_max.m_y) < FLT_EPSILON)
			m_max.m_y += 0.01f;

		if (fabs(m_min.m_z - m_max.m_z) < FLT_EPSILON)
			m_max.m_z += 0.01f;
	}

	Frustum::Frustum()
	{
	}
	
	Frustum::Frustum(const Frustum& rhs)
	{
		for(uint32_t i = 0; i < 6; i++)
			m_planes[i] = rhs.m_planes[i];
	}

	Frustum& Frustum::operator=(const Frustum& rhs)
	{
		for(uint32_t i = 0; i < 6; i++)
			m_planes[i] = rhs.m_planes[i];
		return *this;
	}

	// www.cs.otago.ac.nz/postgrads/alexis/planeExtraction.pdf
	void Frustum::construct(Matrix4& viewproj)
	{
		// left
		m_planes[0].m_normal.m_x = viewproj.m._14 + viewproj.m._11;
		m_planes[0].m_normal.m_y = viewproj.m._24 + viewproj.m._21;
		m_planes[0].m_normal.m_z = viewproj.m._34 + viewproj.m._31;
		m_planes[0].m_D = viewproj.m._44 + viewproj.m._41;

		// right
		m_planes[1].m_normal.m_x = viewproj.m._14 - viewproj.m._11;
		m_planes[1].m_normal.m_y = viewproj.m._24 - viewproj.m._21;
		m_planes[1].m_normal.m_z = viewproj.m._34 - viewproj.m._31;
		m_planes[1].m_D = viewproj.m._44 - viewproj.m._41;

		// top
		m_planes[2].m_normal.m_x = viewproj.m._14 - viewproj.m._12;
		m_planes[2].m_normal.m_y = viewproj.m._24 - viewproj.m._22;
		m_planes[2].m_normal.m_z = viewproj.m._34 - viewproj.m._32;
		m_planes[2].m_D = viewproj.m._44 - viewproj.m._42;

		// bottom
		m_planes[3].m_normal.m_x = viewproj.m._14 + viewproj.m._12;
		m_planes[3].m_normal.m_y = viewproj.m._24 + viewproj.m._22;
		m_planes[3].m_normal.m_z = viewproj.m._34 + viewproj.m._32;
		m_planes[3].m_D = viewproj.m._44 + viewproj.m._42;

		// near
		m_planes[4].m_normal.m_x = viewproj.m._13;
		m_planes[4].m_normal.m_y = viewproj.m._23;
		m_planes[4].m_normal.m_z = viewproj.m._33;
		m_planes[4].m_D = viewproj.m._43;

		// far
		m_planes[5].m_normal.m_x = viewproj.m._14 - viewproj.m._13;
		m_planes[5].m_normal.m_y = viewproj.m._24 - viewproj.m._23;
		m_planes[5].m_normal.m_z = viewproj.m._34 - viewproj.m._33;
		m_planes[5].m_D = viewproj.m._44 - viewproj.m._43;

		for (uint32_t i = 0; i < 6; i++)
		{
			float len = m_planes[i].m_normal.length();
			m_planes[i].m_normal.normalize();
			m_planes[i].m_D /= len;
		}
	}

	const bool Frustum::is_inside(const Vector3& point) const
	{
		for (uint32_t i = 0; i < 6; i++)
		{
			if (m_planes[i].m_normal.dot(point) + m_planes[i].m_D < 0.0f)
				return false;
		}
		return true;
	}

	const bool Frustum::is_inside(const BoundingSphere& sphere) const
	{
		for (uint32_t i = 0; i < 6; i++)
		{
			if (m_planes[i].m_normal.dot(sphere.m_center) + m_planes[i].m_D < -sphere.m_radius)
				return false;
		}
		return true;
	}

	static float ffabs(float a)
	{
		union
		{
			float f;
			int32_t i;
		} u;
		u.f = a;
		u.i &= 0x7fffffff;
		return u.f;
	}

	const bool Frustum::is_inside(const AxisAlignedBoundingBox& aabb) const
	{
		const Vector3 ac = aabb.m_min + (aabb.m_max / 2.f);
		const Vector3& ae = ac - aabb.m_max;
		for (uint32_t i = 0; i < 6; i++)
		{
			const Vector3& pn = m_planes[i].m_normal;
			float d = ac.dot(pn);
			float r = ae.m_x * ffabs(pn.m_x) + ae.m_y * ffabs(pn.m_y) + ae.m_z * ffabs(pn.m_z);
			float dpr = d + r;
			float dmr = d - r;
			if (dpr < -m_planes[i].m_D)
				return false;
			else if (dmr < -m_planes[i].m_D)
				continue;
		}
		return true;
	}

	bool Frustum::intersectsObject(Object3D* object) const
	{
		object;
		return true;
	}


	Transform::Transform()
	{
	}

	Transform::Transform(const Matrix4& matrix)
	{
		m_matrix = matrix;
	}

	const Matrix4& Transform::getMatrix() const
	{
		return m_matrix;
	}

	Transform Transform::getInverse() const
	{
		return Transform(m_matrix);
	}

	void Transform::translate(float x, float y, float z)
	{
		m_matrix.translation(x, y, z);
	}

	void Transform::translate(const Vector3& offset)
	{
		m_matrix.translation(offset);
	}

	void Transform::rotateX(float angle)
	{
		m_matrix.rotation(Vector3(1, 0, 0), Math::to_rad(angle));
	}

	void Transform::rotateY(float angle)
	{
		m_matrix.rotation(Vector3(0, 1, 0), Math::to_rad(angle));
	}

	void Transform::rotateZ(float angle)
	{
		m_matrix.rotation(Vector3(0, 0, 1), Math::to_rad(angle));
	}

	void Transform::scale(float x, float y, float z)
	{
		m_matrix.scale(x, y, z);
	}

	void Transform::scale(const Vector3& scale)
	{
		m_matrix.scale(scale);
	}

	Transform Transform::operator *(const Transform& right)
	{
		return m_matrix * right.getMatrix();
	}

	Transform Transform::operator *= (const Transform& right)
	{
		return m_matrix * right.getMatrix();
	}


	const Quaternion Quaternion::IDENTITY(0, 0, 0, 1);

	Quaternion::Quaternion()
		: x(0),
		  y(0),
		  z(0),
		  w(1)
	{

	}

	Quaternion::Quaternion(float x, float y, float z, float w):
		x(x),
		y(y),
		z(z),
		w(w)
	{

	}

	Quaternion::~Quaternion()
	{

	}

	void Quaternion::setFromAxisAngle(Vector3 axis, float angle)
	{
		float halfAngle = angle / 2.f;
		float s = sin(halfAngle);

		x = axis.m_x * s;
		y = axis.m_y * s;
		z = axis.m_z * s;
		w = cos(halfAngle);
	}

	void Quaternion::setFromEuler(const Euler& euler)
	{
		float ex = Math::to_rad(euler.x);
		float ey = Math::to_rad(euler.y);
		float ez = Math::to_rad(euler.z);

		float c1 = cos(ex / 2);
		float c2 = cos(ey / 2);
		float c3 = cos(ez / 2);
		float s1 = sin(ex / 2);
		float s2 = sin(ey / 2);
		float s3 = sin(ez / 2);

		x = s1 * c2 * c3 + c1 * s2 * s3;
		y = c1 * s2 * c3 - s1 * c2 * s3;
		z = c1 * c2 * s3 + s1 * s2 * c3;
		w = c1 * c2 * c3 - s1 * s2 * s3;
	}

	void Quaternion::setFromRotationMatrix(const Matrix4& mat)
	{
		float m11 = mat._m[0];
		float m12 = mat._m[4];
		float m13 = mat._m[8];

		float m21 = mat._m[1];
		float m22 = mat._m[5];
		float m23 = mat._m[9];

		float m31 = mat._m[2];
		float m32 = mat._m[6];
		float m33 = mat._m[10];

		float trace = m11 + m22 + m33;
		float s;

		if (trace > 0)
		{
			s = 0.5f / Math::sqrt(trace + 1.0f);

			w = 0.25f / s;
			x = (m32 - m23) * s;
			y = (m13 - m31) * s;
			z = (m21 - m12) * s;
		}
		else if (m11 > m22 && m11 > m33)
		{
			s = 2.f * Math::sqrt(1.f + m11 - m22 - m33);

			w = (m32 - m23) / s;
			x = 0.25f * s;
			y = (m12 + m21) / s;
			z = (m13 + m31) / s;
		}
		else if (m22 > m33)
		{
			s = 2.f * Math::sqrt(1.f + m22 - m11 - m33);

			w = (m13 - m31) / s;
			x = (m12 + m21) / s;
			y = 0.25 * s;
			z = (m23 + m32) / s;
		}
		else
		{
			s = 2.f * Math::sqrt(1.f + m33 - m11 - m22);

			w = (m21 + m12) / s;
			x = (m13 + m31) / s;
			y = (m23 + m32) / s;
			z = 0.25f * s;
		}
	}

	Euler Quaternion::toEuler() const
	{
		Euler euler;

		float sqx = x * x;
		float sqy = y * y;
		float sqz = z * z;
		float sqw = w * w;

		euler.x = atan2f(2 * (x * w - y * z), (sqw - sqx - sqy + sqz));
		euler.y = asinf(Math::clamp(2 * (x * z + y * w), -1.f, 1.f));
		euler.z = atan2f(2 * (z * w - x * y), (sqw + sqx - sqy - sqz));
		return euler;
	}

	void Quaternion::normalize()
	{
		float length = (w * w) + (x * x) + (y * y) + (z * z);
		float f = 1.f / length;
		*this = *this * f;
	}

	Vector3 Quaternion::operator*(const Vector3& rhs) const
	{
		Vector3 uv;
		Vector3 uuv;
		Vector3 qvec(z, y, z);
		uv = qvec.cross(rhs);
		uuv = qvec.cross(uv);
		uv *= 2.f * w;
		uuv *= 2.f;
		return rhs + uv + uuv;
	}

	Quaternion Quaternion::operator*(const Quaternion& rhs) const
	{
		float ax = x;
		float ay = y;
		float az = z;
		float aw = w;

		float bx = rhs.x;
		float by = rhs.y;
		float bz = rhs.z;
		float bw = rhs.w;

		return Quaternion
			(
			ax * bw + aw * bx + ay * bz - az * by,
			ay * bw + aw * by + az * bx - ax * bz,
			az * bw + aw * bz + ax * by - ay * bx,
			aw * bw - ax * bx - ay * by - az * bz
		);
	}

	Quaternion Quaternion::operator*(float rhs) const
	{
		return Quaternion(x * rhs, y * rhs, z * rhs, w * rhs);
	}

	Euler::Euler()
	{
		x = 0.f;
		y = 0.f;
		z = 0.f;
	}

	bool Euler::operator !=(const Euler& right)
	{
		return (x != right.x || y != right.y || z != right.z);
	}

	bool Euler::operator ==(const Euler& right)
	{
		return (x == right.x && y == right.y && z == right.z);
	}

	float RandomRange(float min, float max) {
		float range = max - min;

		int randomizedInteger = rand() % 10000;
		float randomizedNormalized = static_cast<float>(randomizedInteger) / 10000.0f;
		return randomizedNormalized * range + min;
	}
}
