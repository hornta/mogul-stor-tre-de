// example_game.cpp

#include "stdafx.h"
#include <helium/gameplay/state_factory.hpp>
#include <helium/example/example_loading_state.hpp>
#include <helium/example/example_scene_state.hpp>
#include <helium/example/example_game.hpp>

namespace helium
{
	using namespace gameplay;

	namespace example
	{
		//static 
		AbstractGame::Ptr Game::create()
		{
			return AbstractGame::Ptr(new Game);
		}

		// private
		Game::Game()
		{
		}

		// public
		Game::~Game()
		{
		}

		void Game::initialize()
		{
			AbstractGame::initialize();
			
			uint32_t string_hash[] =
			{
				String::hash32("exampleloading", 14),
				String::hash32("examplescene", 12),
			};

			gameplay::StateFactory* factory = m_state_manager->get_factory();
			factory->attach<ExampleLoadingState>(string_hash[0]);
			factory->attach<ExampleSceneState>(string_hash[1]);
			m_state_manager->set_state(string_hash[0]);
		}

		void Game::shutdown()
		{
			AbstractGame::shutdown();
		}
	}
}
