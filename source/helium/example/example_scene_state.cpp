// example_scene_state.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/resource/resource_cache.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/scene/camera.hpp>
#include <helium/scene/transform.hpp>
#include <helium/scene/lighting_property.hpp>
#include <helium/scene/shader_unit.hpp>
#include <helium/scene/shader_parameter.hpp>
#include <helium/scene/texture_unit.hpp>
#include <helium/scene/technique.hpp>
#include <helium/scene/technique_factory.hpp>
#include <helium/example/example_scene_state.hpp>

namespace helium
{
	namespace example
	{
		ExampleSceneState::ExampleSceneState()
		{
		}

		ExampleSceneState::~ExampleSceneState()
		{
		}

		void ExampleSceneState::initialize()
		{
			// create scene
			m_scene = scene::Scene::create();
			scene::SceneNode* node = m_scene->create_scene_node();
			(void)node;
		}

		void ExampleSceneState::shutdown()
		{
		}

		bool ExampleSceneState::update()
		{
			return true;
		}

		void ExampleSceneState::draw()
		{
		}

		uint32_t ExampleSceneState::next()
		{
			return 0;
		}
	}
}
