// camera.cpp

#include "stdafx.h"
#include <helium/scene/camera.hpp>

namespace helium
{
	namespace scene
	{
		Camera::Camera()
			: m_right(Vector3::RIGHT),
			  m_up(Vector3::UP),
			  m_forward(Vector3::FORWARD)
		{
			m_dirty = true;
			m_pitch = 0.f;
			m_yaw = 0.f;
			m_roll = 0.f;
			m_view.identity();
			m_projection.identity();
		}

		Camera::~Camera()
		{
		}

		void Camera::update()
		{
			if (!m_dirty)
				return;
			Vector3 right   = Vector3::RIGHT;
			Vector3 up      = Vector3::UP;
			Vector3 forward = Vector3::FORWARD;

			Matrix4 rotateZ = Matrix4::rotation(forward, Math::to_rad(m_roll));
			up = rotateZ * up;
			right = rotateZ * right;

			Matrix4 rotateY = Matrix4::rotation(up, Math::to_rad(m_yaw));
			right = rotateY * right;
			forward = rotateY * forward;

			Matrix4 rotateX = Matrix4::rotation(right, Math::to_rad(m_pitch));
			up = rotateX * up;
			forward = rotateX * forward;

			right.normalize();
			up.normalize();
			forward.normalize();

			m_right = right;
			m_up = up;
			m_forward = forward;

			m_view.m._11 = m_right.m_x;
			m_view.m._21 = m_right.m_y;
			m_view.m._31 = m_right.m_z;

			m_view.m._12 = m_up.m_x;
			m_view.m._22 = m_up.m_y;
			m_view.m._32 = m_up.m_z;

			m_view.m._13 = m_forward.m_x; 
			m_view.m._23 = m_forward.m_y;
			m_view.m._33 = m_forward.m_z;

			m_view.m._41 = -m_position.dot(m_right);
			m_view.m._42 = -m_position.dot(m_up);
			m_view.m._43 = -m_position.dot(m_forward);

			Matrix4 viewproj = m_view * m_projection;
			m_frustum.construct(viewproj);
		}

		void Camera::move_x(float amount) {
			m_dirty = true;
			m_position += Vector3(1.0f, 0.0f, 0.0f) * amount;
		}

		void Camera::move_y(float amount) {
			m_dirty = true;
			m_position += Vector3(0.0f, 1.0f, 0.0f) * amount;
		}

		void Camera::move_z(float amount) {
			m_dirty = true;
			m_position += Vector3(0.0f, 0.0f, 1.0f) * amount;
		}

		void Camera::move_forward(float amount)
		{
			m_dirty = true;
			m_position += m_forward * amount;
		}

		void Camera::move_sidestep(float amount)
		{
			m_dirty = true;
			m_position += m_right * amount;
		}

		void Camera::move_elevate(float amount)
		{
			m_dirty = true;
			m_position += m_up * amount;
		}

		void Camera::rotatex(float amount)
		{
			m_dirty = true;
			m_pitch += amount;
		}

		void Camera::rotatey(float amount)
		{
			m_dirty = true;
			m_yaw += amount;
		}

		void Camera::rotatez(float amount)
		{
			m_dirty = true;
			m_roll += amount;
		}

		void Camera::lookAt(const Vector3& v)
		{
			Vector3 dir = v - m_position;
			float x = Math::to_deg(atan2(-dir.m_y, dir.m_z));
			
			float y = Math::to_deg(atan2(dir.m_x, dir.m_z));

			m_yaw = y;
			m_pitch = x;
			m_dirty = true;
		}

		void Camera::updatePosition(const Vector3& playerPos) {
			m_position = playerPos;
			move_forward(-15);
		}

		void Camera::set_perspective(float fov, float aspect, float znear, float zfar)
		{
			m_dirty = true;
			m_projection = Matrix4::perspective(fov, aspect, znear, zfar);
		}

		void Camera::set_orthogonal(float width, float height, float znear, float zfar)
		{
			m_dirty = true;
			m_projection = Matrix4::orthogonal(width, height, znear, zfar);
		}

		const Matrix4& Camera::get_view() const
		{
			return m_view;
		}

		const Matrix4& Camera::get_projection() const
		{
			return m_projection;
		}

		Vector3 Camera::get_position() const
		{
			return m_position;
		}

		const Frustum& Camera::get_frustum() const
		{
			return m_frustum;
		}
	}
}
