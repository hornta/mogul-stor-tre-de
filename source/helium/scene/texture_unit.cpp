// texture_unit.cpp

#include "stdafx.h"
#include <cassert>
#include <helium/scene/texture_unit.hpp>

namespace helium
{
	namespace scene
	{
		// static 
		TextureUnit::Ptr TextureUnit::create(uint32_t texture, uint32_t sampler, uint32_t blend)
		{
			return TextureUnit::Ptr(new TextureUnit(texture, sampler, blend));
		}

		// private
		TextureUnit::TextureUnit(uint32_t texture, uint32_t sampler, uint32_t blend)
		{
			m_texture = texture; 
			m_sampler = sampler;
			m_blend = blend;
		}

		// public
		TextureUnit::~TextureUnit()
		{
		}

		uint32_t TextureUnit::get_texture() const
		{
			return m_texture;
		}

		uint32_t TextureUnit::get_sampler() const
		{
			return m_sampler;
		}

		uint32_t TextureUnit::get_blend_state() const
		{
			return m_blend;
		}
	}
}
