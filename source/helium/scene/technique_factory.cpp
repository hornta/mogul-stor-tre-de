// technique_factory.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/scene/technique.hpp>
#include <helium/scene/technique_factory.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>
#include <helium/resource/material.hpp>

namespace helium
{
	namespace scene
	{
		// static
		Technique* TechniqueFactory::create_technique(const resource::Material* material)
		{
			(void)material;
			return nullptr;
		}
	}
}
