// scene.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/scene/scene.hpp>
#include "Object3d.h"
#include "Mesh.h"
#include "Light.h"

namespace helium
{
	namespace scene
	{
		Scene::Scene(helium::system::RenderSystem* renderSystem, SoundSystem* soundSystem)
			: m_renderSystem(renderSystem),
			m_soundSystem(soundSystem)
		{
			//m_resourceLoader.init(m_renderSystem);
			m_lightIndex = 0;
			m_camera = nullptr;
			m_render_dispatcher = renderer::RenderDispatcher::create(ServiceLocator<system::RenderSystem>::get_service());
			m_globalAmbient = Color::White;
			m_eye = Vector4(1.f, 1.f, 1.f, 1.f);

			for (int i = 0; i < MAX_LIGHTS; ++i)
			{
				m_lights[i].position = Vector3(0.f, 0.f, 0.f);
				m_lights[i].direction = Vector3(0.f, 0.f, 0.f);
				m_lights[i].color = Color::Black;
				m_lights[i].spotAngle = 0.f;
				m_lights[i].constantAttenuation = 0.f;
				m_lights[i].linearAttenuation = 0.f;
				m_lights[i].quadraticAttenuation = 0.f;
				m_lights[i].type = ELightType::UNDEFINED;
				m_lights[i].enabled = false;
			}
		}

		Scene::~Scene()
		{
			Object3D::object_count = 0;
			auto it = m_nodes.begin();
			while (it != m_nodes.end())
			{
				delete (*it);
				(*it) = nullptr;
				++it;
			}

			{
				auto it = m_lightNodes.begin();
				while (it != m_lightNodes.end())
				{
					delete (*it);
					(*it) = nullptr;
					++it;
				}
			}
			m_nodes.clear();
			m_visibles.clear();
			m_lightNodes.clear();

			if (m_camera != nullptr)
			{
				delete m_camera;
				m_camera = nullptr;
			}
		}

		void Scene::pre_render()
		{
			m_visibles.clear();

			const Frustum& frustum = m_camera->get_frustum();
			for (auto& node : m_nodes)
			{
				Mesh* mesh = static_cast<Mesh*>(node);
				mesh->updateAABB();
				if (!frustum.is_inside(mesh->getAABB()))
					continue;
					
				m_visibles.push_back(node);
			}
			// sort nodes by shader and texture

		}

		void Scene::post_render()
		{
			m_render_dispatcher->process();
		}

		void Scene::render()
		{
			renderer::RenderCommand cmd;

			// Generate shadow map
			for (auto& light : m_lights)
			{
				light;
				cmd.m_type = renderer::ERenderCommandType::ClearShadowMap;
				m_render_queue.push(cmd);

				cmd.m_type = renderer::ERenderCommandType::TargetShadowMap;
				m_render_queue.push(cmd);
			}

			cmd.m_type = renderer::ERenderCommandType::TargetRenderView;
			m_render_queue.push(cmd);

			// Render recursive
			for (auto &node : m_nodes)
			{
				renderRecursive(node);
			}
			
			m_render_dispatcher->add_queue(&m_render_queue);
		}

		void Scene::renderMesh(Mesh* mesh)
		{
			renderer::RenderCommand cmd;

			// shader
			cmd.m_type = renderer::ERenderCommandType::Shader;
			cmd.m_cmd.m_id = mesh->shader;
			m_render_queue.push(cmd);

			// texture
			cmd.m_type = renderer::ERenderCommandType::Texture;
			cmd.m_cmd.m_texture.m_id = mesh->m_material.getTexture();
			cmd.m_cmd.m_texture.m_slot = 0;
			m_render_queue.push(cmd);

			// sampler
			cmd.m_type = renderer::ERenderCommandType::Sampler;
			cmd.m_cmd.m_sampler.m_id = mesh->sampler;
			m_render_queue.push(cmd);

			// vertex format
			cmd.m_type = renderer::ERenderCommandType::VertexFormat;
			cmd.m_cmd.m_id = mesh->vertex_format;
			m_render_queue.push(cmd);

			// vertex buffer
			cmd.m_type = renderer::ERenderCommandType::VertexBuffer;
			cmd.m_cmd.m_id = mesh->vertex_buffer;
			m_render_queue.push(cmd);

			// index buffer
			cmd.m_type = renderer::ERenderCommandType::IndexBuffer;
			cmd.m_cmd.m_id = mesh->index_buffer;
			m_render_queue.push(cmd);

			// raster
			cmd.m_type = renderer::ERenderCommandType::Rasterizer;
			cmd.m_cmd.m_id = mesh->raster;
			m_render_queue.push(cmd);

			// depth state
			cmd.m_type = renderer::ERenderCommandType::Depth;
			cmd.m_cmd.m_id = mesh->depth;
			m_render_queue.push(cmd);

			/* LIGHTNING */
			cmd.m_type = renderer::ERenderCommandType::ShaderConstant;

			// - Eye position
			cmd.m_cmd.m_constant.m_size = sizeof(Vector4);
			cmd.m_cmd.m_constant.m_data = static_cast<void*>(&m_eye);
			cmd.m_cmd.m_constant.m_hash = String::hash32("eye", 3);
			m_render_queue.push(cmd);

			// - Global ambient
			cmd.m_cmd.m_constant.m_size = sizeof(Color);
			cmd.m_cmd.m_constant.m_data = static_cast<void*>(&m_globalAmbient);
			cmd.m_cmd.m_constant.m_hash = String::hash32("globalAmbient", 13);
			m_render_queue.push(cmd);

			// - Lights
			cmd.m_cmd.m_constant.m_size = sizeof(LightConstant) * MAX_LIGHTS;
			cmd.m_cmd.m_constant.m_data = &m_lights;
			cmd.m_cmd.m_constant.m_hash = String::hash32("lights", 6);
			m_render_queue.push(cmd);

			// - Material
			cmd.m_cmd.m_constant.m_size = sizeof(Material::Constant);
			cmd.m_cmd.m_constant.m_data = &mesh->m_material.getConstant();
			cmd.m_cmd.m_constant.m_hash = String::hash32("material", 8);
			m_render_queue.push(cmd);

			// projection
			cmd.m_cmd.m_constant.m_size = sizeof(Matrix4);
			cmd.m_cmd.m_constant.m_data = static_cast<void*>(const_cast<Matrix4*>(&m_camera->get_projection()));
			cmd.m_cmd.m_constant.m_hash = String::hash32("projection", 10);
			m_render_queue.push(cmd);

			// view
			cmd.m_cmd.m_constant.m_data = static_cast<void*>(const_cast<Matrix4*>(&m_camera->get_view()));
			cmd.m_cmd.m_constant.m_hash = String::hash32("view", 4);
			m_render_queue.push(cmd);

			// world
			cmd.m_cmd.m_constant.m_data = static_cast<void*>(const_cast<Matrix4*>(&mesh->getMatrix()));
			cmd.m_cmd.m_constant.m_hash = String::hash32("world", 5);
			m_render_queue.push(cmd);

			if (mesh->m_geometry->m_numIndicies == 0)
			{
				cmd.m_type = renderer::ERenderCommandType::Draw;
				cmd.m_cmd.m_draw.m_mode = static_cast<uint32_t>(mesh->draw_mode);
				cmd.m_cmd.m_draw.m_count = mesh->draw_count;
				cmd.m_cmd.m_draw.m_start = 0;
				m_render_queue.push(cmd);
			}
			else
			{
				cmd.m_type = renderer::ERenderCommandType::DrawIndexed;
				cmd.m_cmd.m_draw.m_mode = static_cast<uint32_t>(mesh->draw_mode);
				cmd.m_cmd.m_draw.m_count = mesh->draw_count;
				cmd.m_cmd.m_draw.m_start = 0;
				m_render_queue.push(cmd);
			}
		}

		void Scene::renderRecursive(Object3D* node)
		{
			if (node->hasChildren())
			{
				std::vector<Object3D*>& children = node->getChildren();
				for (auto &n : children)
				{
					renderRecursive(n);
				}
			}
			renderMesh(static_cast<Mesh*>(node));
		}

		void Scene::update(float dt)
		{
			dt;
			for (auto &node : m_nodes)
			{
				node->update();
			}
			m_camera->update();
			m_eye = Vector4(m_camera->get_position(), 1.f);
		}

		void Scene::add(Object3D* object3d)
		{
			if (object3d->m_type == EObjectType::LIGHT)
			{
				m_lightNodes.push_back(object3d);
				Light* light = static_cast<Light*>(object3d);

				if (light->m_light_type == ELightType::DIRECTIONAL_LIGHT)
				{
					m_lights[m_lightIndex].direction = light->m_direction;
					m_lights[m_lightIndex].color = light->m_color;
					m_lights[m_lightIndex].type = light->m_light_type;
					m_lights[m_lightIndex].enabled = light->m_enabled;
				}
				else if (light->m_light_type == ELightType::POINT_LIGHT)
				{
					m_lights[m_lightIndex].position = light->m_localPosition;
					m_lights[m_lightIndex].color = light->m_color;
					m_lights[m_lightIndex].constantAttenuation = light->m_attenuation.m_x;
					m_lights[m_lightIndex].linearAttenuation = light->m_attenuation.m_y;
					m_lights[m_lightIndex].quadraticAttenuation = light->m_attenuation.m_z;
					m_lights[m_lightIndex].type = light->m_light_type;
					m_lights[m_lightIndex].enabled = light->m_enabled;
				}
				++m_lightIndex;
				return;
			}
			m_nodes.push_back(object3d);
		}

		void Scene::set_camera(Camera* camera)
		{
			m_camera = camera;
		}

		void Scene::setResourceLoader(ResourceLoader* resourceLoader)
		{
			m_resourceLoader = resourceLoader;
		}

		Camera* Scene::get_camera()
		{
			return m_camera;
		}

		Object3D* Scene::getObjectById(uint32_t id)
		{
			Object3D* n = nullptr;
			for (auto& node : m_nodes)
			{
				if (node->m_id == id)
					n = node;
				else
					n = findObjectByIdRecursive(node, id);

				if (n != nullptr)
					break;
			}
			return n;
		}

		Object3D* Scene::findObjectByIdRecursive(Object3D* parent, uint32_t id)
		{
			auto children = parent->getChildren();
			for (Object3D* child : children)
			{
				if (child->m_id == id)
				{
					return child;
				}
				else if (child->hasChildren())
				{
					findObjectByIdRecursive(child, id);
				}
			}
			return nullptr;
		}

		Object3D* Scene::getLightById(uint32_t id)
		{
			for (Object3D* node : m_lightNodes)
			{
				if (node->m_id == id)
					return node;
			}
			return nullptr;
		}

		void Scene::removeObjectById(uint32_t id)
		{
			for (std::size_t i = 0; i < m_nodes.size(); ++i)
			{
				if (m_nodes[i]->m_id == id)
				{
					removeObject(i);
					return;
				}
			}
		}

		void Scene::removeObject(Object3D* object)
		{
			for (std::size_t i = 0; i < m_nodes.size(); ++i)
			{
				if (m_nodes[i] == object)
				{
					removeObject(i);
					return;
				}
			}
		}

		void Scene::removeObject(std::size_t index)
		{
			Object3D* object = m_nodes[index];
			if (object->hasChildren()) {
				std::vector<Object3D*> children = object->getChildren();
				for (unsigned i = 0; i < children.size(); i++) {
					for (std::size_t j = 0; j < m_nodes.size(); ++j)
					{
						if (m_nodes[j] == children.at(i))
						{
							removeObject(j);
						}
					}
				}
			}

			delete object;
			object = nullptr;

			m_nodes.erase(m_nodes.begin() + index);
		}

		ResourceLoader* Scene::getResourceLoader()
		{
			return m_resourceLoader;
		}

		SoundSystem* Scene::getSoundSystem()
		{
			return m_soundSystem;
		}

		RenderSystem* Scene::getRenderSystem() {
			return m_renderSystem;
		}
	}
}
