// state_manager.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/gameplay/state_manager.hpp>

namespace helium
{
	namespace gameplay
	{
		// static
		StateManager::Ptr StateManager::create()
		{
			return StateManager::Ptr(new StateManager);
		}

		// private
		StateManager::StateManager()
		{
			m_active_state = nullptr;
		}

		// public
		StateManager::~StateManager()
		{
			if (m_active_state != nullptr)
			{
				m_active_state->shutdown();
				delete m_active_state;
				m_active_state = nullptr;
			}
		}

		bool StateManager::update(float dt)
		{
			if (!m_active_state)
				return false;

			if (!m_active_state->update(dt))
				set_state(m_active_state->get_next());
			return true;
		}

		void StateManager::draw()
		{
			if (!m_active_state)
				return;
			m_active_state->draw();
		}

		void StateManager::set_state(uint32_t id_hash)
		{
			if (m_active_state)
			{
				m_active_state->shutdown();
				delete m_active_state;
				m_active_state = nullptr;
			}
			if (id_hash == 0)
				return;

			m_active_state = m_factory->construct(id_hash);
			m_active_state->setGame(m_game);
			m_active_state->initialize();
		}

		void StateManager::set_factory(StateFactory::Ptr factory)
		{
			m_factory = std::move(factory);
		}

		StateFactory* StateManager::get_factory()
		{
			return m_factory.get();
		}
	}
}
