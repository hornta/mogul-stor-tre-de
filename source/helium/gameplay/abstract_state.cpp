// abstract_state.cpp

#include "stdafx.h"
#include <helium/gameplay/abstract_state.hpp>

namespace helium
{
	namespace gameplay
	{
		AbstractState::~AbstractState()
		{
			m_start_time = Time::now();
			m_id_hash = 0;
		}

		void AbstractState::setGame(AbstractGame* game)
		{
			m_game = game;
		}

		uint32_t AbstractState::get_next()
		{
			return m_id_hash_next;
		}

		void AbstractState::setNext(uint32_t hash)
		{
			m_id_hash_next = hash;
		}
	}
}
