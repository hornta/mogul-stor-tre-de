// abstract_game.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/audio_system.hpp>
#include <helium/system/collision_system.hpp>
#include <helium/system/render_system.hpp>
#include <helium/gameplay/abstract_game.hpp>
#include "SoundSystem.h"

namespace helium
{
	namespace gameplay
	{
		using namespace helium::system;

		//static 
		AbstractGame::Ptr AbstractGame::create()
		{
			return AbstractGame::Ptr(new AbstractGame);
		}

		// public
		AbstractGame::~AbstractGame()
		{
		}

		void AbstractGame::initialize()
		{
			Debug::write(EDebugLevel::Info, "abstract game init");
			m_state_manager = StateManager::create();
			m_state_manager->set_factory(StateFactory::create());
			m_state_manager->m_game = this;
			m_collision_system = ServiceLocator<system::CollisionSystem>::get_service();
			m_render_system = ServiceLocator<system::RenderSystem>::get_service();
			m_audio_system = ServiceLocator<system::AudioSystem>::get_service();
			m_soundSystem = new SoundSystem();
			m_soundSystem->loadSound("../data/music/soundtrack2.wav", "song1", FMOD_LOOP_NORMAL);
			m_soundSystem->loadSound("../data/music/pistolshot.ogg", "WEAPON_FIRE", FMOD_LOOP_OFF);
			m_soundSystem->loadSound("../data/music/weaponcock.ogg", "WEAPON_COCK", FMOD_LOOP_OFF);

		}

		void AbstractGame::shutdown()
		{
			delete m_soundSystem;
			m_soundSystem = nullptr;
			m_state_manager->set_state(0);
		}

		bool AbstractGame::process()
		{
			Time now = Time::now();
			Time delta = now - m_time;
			m_time = now;
			float dt = delta.as_seconds();

			m_audio_system->process();
			if (!m_state_manager->update(dt))
				return false;
			m_collision_system->process();
			m_render_system->clear();
			m_state_manager->draw();
			m_render_system->present();
			m_soundSystem->Update();
			return true;
		}

		system::RenderSystem* AbstractGame::renderer()
		{
			return m_render_system;
		}

		system::CollisionSystem* AbstractGame::collision()
		{
			return m_collision_system;
		}

		system::AudioSystem* AbstractGame::audio()
		{
			return m_audio_system;
		}

		SoundSystem* AbstractGame::getSoundSystem()
		{
			return m_soundSystem;
		}

		ResourceLoader* AbstractGame::getResourceLoader()
		{
			return m_resourceLoader;
		}
	}
}