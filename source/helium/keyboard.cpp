// keyboard.cpp

#include "stdafx.h"
#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium/service_locator.hpp>
#include <helium/keyboard.hpp>

namespace helium
{
	//static 
	Keyboard::Ptr Keyboard::create()
	{
		return Keyboard::Ptr(new Keyboard);
	}

	// private
	Keyboard::Keyboard()
	{
		memset(m_curr, 0, sizeof(m_curr));
		memset(m_prev, 0, sizeof(m_prev));
		ServiceLocator<OSEventDispatcher>::get_service()->attach(this);
	}

	// public
	Keyboard::~Keyboard()
	{
		ServiceLocator<OSEventDispatcher>::get_service()->detach(this);
	}

	bool Keyboard::is_key_down(EKeyCode key) const
	{
		return m_curr[(uint32_t)key];
	}

	bool Keyboard::is_key_down_once(EKeyCode key) const
	{
		return m_curr[(uint32_t)key] && !m_prev[(uint32_t)key];
	}

	void Keyboard::post_frame()
	{
		memcpy(m_prev, m_curr, sizeof(m_curr));
	}

	void Keyboard::notify(OSEvent* event)
	{
		if (event->m_type == EOSEventType::Keyboard)
			m_curr[event->m_data.keyboard.keycode] = event->m_data.keyboard.state;
	}
}
