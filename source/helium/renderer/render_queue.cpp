// render_queue.cpp

#include "stdafx.h"
#include <helium/renderer/render_queue.hpp>

namespace helium
{
	namespace renderer
	{
		RenderQueue::RenderQueue()
		{
			reset();
		}

		void RenderQueue::reset()
		{
		}

		void RenderQueue::push(RenderCommand& cmd)
		{
			m_queue.push(cmd);
		}

		bool RenderQueue::pop(RenderCommand& cmd)
		{
			if (m_queue.size() == 0)
				return false;
			cmd = m_queue.front();
			m_queue.pop();
			return true;
		}
	}
}
