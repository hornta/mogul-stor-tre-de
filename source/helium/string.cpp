// string.cpp

#include "stdafx.h"
#include <cstdarg>
#include <cstring>
#include "murmur.hpp"
#include <helium/string.hpp>

namespace helium
{
	uint32_t String::hash32(const void* array, uint32_t length)
	{
		return murmurhash32(array, length);
	}

	uint64_t String::hash64(const void* array, uint32_t length)
	{
		return murmurhash64(array, length);
	}

	std::string String::format(const char* fmt, ...)
	{
		static char message[16384];
		va_list vl;
		va_start(vl, fmt);
		vsprintf(message, fmt, vl);
		va_end(vl);
		return std::string(message);
	}
}
