#include "stdafx.h"
#include "Color.h"


Color::Color()
	:m_r(0),
	m_g(0),
	m_b(0),
	m_a(1.f)
{

}

Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	m_r = static_cast<float>(r) / 256.f;
	m_g = static_cast<float>(g) / 256.f;
	m_b = static_cast<float>(b) / 256.f;
	m_a = static_cast<float>(a) / 256.f;
}

const Color Color::Black(0, 0, 0);
const Color Color::Silver(192, 192, 192);
const Color Color::Gray(128, 128, 128);
const Color Color::White(255, 255, 255);
const Color Color::Maroon(128, 0, 0);
const Color Color::Red(255, 0, 0);
const Color Color::Purple(128, 0, 128);
const Color Color::Fuchsia(255, 0, 255);
const Color Color::Green(0, 128, 0);
const Color Color::Lime(0, 255, 0);
const Color Color::Olive(128, 128, 0);
const Color Color::Yellow(255, 255, 0);
const Color Color::Navy(0, 0, 128);
const Color Color::Blue(0, 0, 255);
const Color Color::Teal(0, 128, 128);
const Color Color::Aqua(0, 255, 255);
const Color Color::Transparent(0, 0, 0, 0);