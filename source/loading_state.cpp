#include "stdafx.h"

#include <helium/system/render_system.hpp>
#include <helium/gameplay/abstract_game.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>
#include <helium/scene/scene.hpp>
#include "loading_state.h"

#include "Geometry.hpp"
#include "Material.h"
#include "Mesh.h"
#include "Light.h"
#include "Color.h"
#include "HudManager.h"
#include "GameStats.h"
#include "SoundSystem.h"
#include "PlayerObject.h"
#include "EnemyObject.h"
#include "PickupObject.h"
#include "GameObjectManager.h"
#include "Weapon.h"
#include <iostream>

loading_state::loading_state()
	: m_mouseDeltaX(0.f),
	m_mouseDeltaY(0.f)
{
	
}

loading_state::~loading_state()
{
}

void loading_state::initialize()
{
	m_pickupTimer = 0.0f;
	m_enemyTimer = 0.0f;
	m_gameOverTimer = 0.0f;
	m_game->getSoundSystem()->playSound("song1", 0.2f);
	m_running = true;
	m_scene = new scene::Scene(m_game->renderer(), m_game->getSoundSystem());
	m_scene->setResourceLoader(m_game->getResourceLoader());
	m_keyboard = Keyboard::create();
	m_mouse = Mouse::create();
	
	helium::scene::Camera* camera = new helium::scene::Camera();
	float aspect = (float)m_game->renderer()->get_width() / (float)m_game->renderer()->get_height();
	camera->set_perspective(Math::to_rad(45.f), aspect, 0.5f, 10000.0f);
	camera->rotatex(70.f);
	camera->translateZ(-16.0f);
	camera->translateY(8.0f);
	m_scene->set_camera(camera);

	m_gs = new GameStats();

	m_gom = new GameObjectManager(m_scene, -20.0f, 20.0f, -20.0f, 20.0f);
	
	m_gom->SpawnPlayer();
	m_gom->SpawnMap();
	m_gom->SpawnRandomPickup();
	m_gom->SpawnRandomPickup();
	m_gom->SpawnRandomPickup();
	m_gom->SpawnRandomPickup();
	
	m_hm = new HudManager();
	m_hm->Init(m_scene, m_gs, m_gom->GetPlayer());
	
	DirectionalLight* d1 = new DirectionalLight(Vector3(0, -1, 0), Color::White);
	d1->m_castShadow = true;
	d1->m_projection = Matrix4::orthogonal(m_game->renderer()->get_width(),
		m_game->renderer()->get_height(), -10000.f, 10000.f);
	m_scene->add(d1);

	setNext(String::hash32("menu_state", 10));
}

void loading_state::shutdown()
{
	if (m_gs != nullptr) {
		delete m_gs;
		m_gs = nullptr;
	}

	if (m_hm != nullptr) {
		m_hm->CleanUp();
		delete m_hm;
		m_hm = nullptr;
	}

	if (m_gom != nullptr) {
		m_gom->CleanUp();
		delete m_gom;
		m_gom = nullptr;
	}

	delete m_scene;
	m_scene = nullptr;
}

bool loading_state::update(float dt)
{
	/* If dt is to large */
	if (dt > 0.1f)
		dt = 0.1f;

	/* Update timers */
	if (m_gom->GetPlayer()->GetHealth() <= 0) {
		m_gameOverTimer += dt;
	}
	m_pickupTimer += dt;
	m_enemyTimer += dt;
	m_gs->UpdateTime(dt);

	/* Spawn enemy wave */
	if (m_gom->GetNumberOfEnemies() == 0 || m_enemyTimer > 15.0f) {
		for (int i = 0; i < m_gs->GetActiveWaveNumber() / 3 + 1; i++) {
			m_gom->SpawnWave(5);
		}
		m_gs->IncrementWaveNumber();
		m_enemyTimer = 0.0f;
	}

	/* Spawn pickup */
	if (m_pickupTimer > 10.0f) {
		m_gom->SpawnRandomPickup();
		m_pickupTimer = 0.0f;
	}

	/* GameObjects Update */
	m_gom->UpdateObjects(dt);
	m_scene->get_camera()->updatePosition(m_gom->GetObjectByID(0)->GetPosition());

	/* Update GameStats */
	m_gs->IncreaseScore(m_gom->GetScoreThisTurn());

	/* Update HUD */
	m_hm->Update();
	
	/* Scene Update */
	m_scene->update(dt);

	/* GameOver */
	if (m_keyboard->is_key_down_once(EKeyCode::Escape) || m_gameOverTimer > 5.0f)
		m_running = false;

	/* Update mouse and keyboard */
	m_mouse->post_frame();
	m_keyboard->post_frame();

	return m_running;
}

void loading_state::draw()
{
	m_scene->pre_render();
	m_scene->render();
	m_scene->post_render();
	m_hm->Draw();
}