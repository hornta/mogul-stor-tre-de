#include "stdafx.h"
#include "ShadowMap.h"
#include <helium/system/render_system.hpp>

using namespace helium::system;

ShadowMap::ShadowMap()
{
	m_shadowMap = 0;
	m_depthStencilView = 0;
	m_shaderResourceView = 0;
}

ShadowMap::~ShadowMap()
{
	if (m_shaderResourceView)
	{
		m_shaderResourceView->Release();
		m_shaderResourceView = 0;
	}

	if (m_depthStencilView)
	{
		m_depthStencilView->Release();
		m_depthStencilView = 0;
	}

	if (m_shadowMap)
	{
		m_shadowMap->Release();
		m_shadowMap = 0;
	}
}

bool ShadowMap::initalize(ID3D11Device* device, uint32_t width, uint32_t height)
{
	// Setup texture description
	D3D11_TEXTURE2D_DESC textureDesc;
	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	textureDesc.SampleDesc.Count = 0;
	textureDesc.SampleDesc.Quality = 1;

	// Setup render target view description
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	depthStencilViewDesc.Format = textureDesc.Format;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	// Setup shader resource view
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	// Create texture and depth/resource views
	if (FAILED(device->CreateTexture2D(&textureDesc, NULL, &m_shadowMap))) return false;
	if (FAILED(device->CreateDepthStencilView(m_shadowMap, &depthStencilViewDesc, &m_depthStencilView))) return false;
	if (FAILED(device->CreateShaderResourceView(m_shadowMap, &shaderResourceViewDesc, &m_shaderResourceView))) return false;

	return true;
}