// Gamestats.cpp

#include "stdafx.h"
#include "GameStats.h"

GameStats::GameStats() {
	m_score = 0;
	m_waveNumber = 0;
	m_time = 0.0f;
}

GameStats::~GameStats() {

}

int GameStats::GetScore() const {
	return m_score;
}

int GameStats::GetActiveWaveNumber() const {
	return m_waveNumber;
}

float GameStats::GetTime() const {
	return m_time;
}

void GameStats::IncrementWaveNumber() {
	m_waveNumber++;
}

void GameStats::IncreaseScore(int amount) {
	m_score += amount;
}

void GameStats::UpdateTime(float dt) {
	m_time += dt;
}

void GameStats::ResetWaveNumber() {
	m_waveNumber = 0;
}

void GameStats::ResetScore() {
	m_score = 0;
}

void GameStats::ResetTime() {
	m_time = 0.0f;
}