// PlayerObject.cpp

#include "stdafx.h"
#include "PlayerObject.h"
#include <iostream>

#include "Geometry.hpp"
#include "Material.h"
#include "Mesh.h"
#include "Weapon.h"
#include <helium\scene\scene.hpp>
#include "PickupObject.h"
#include "GameObjectManager.h"
#include <cmath>

PlayerObject::PlayerObject(Object3D* obj_3D, Weapon* pistol) {
	m_type = ObjectType::PlayerO;

	m_move_dir = Vector3(0.0f, 0.0f, 0.0f);
	m_move_speed = 5.5f;
	m_weapon = nullptr;
	m_pistol = pistol;
	m_currentHealth = 5;
	m_maxHealth = 20;

	m_keyboard = Keyboard::create();
	m_mouse = Mouse::create();

	m_object3D = obj_3D;
}

PlayerObject::~PlayerObject() {

}

float PlayerObject::GetYRotation() {
	float y;
	Euler e = m_object3D->getDerivedRotation().toEuler();

	if (e.x == 0.0f && e.z == 0.0f) {
		if (e.y <= 0.0f)
			y = Math::to_deg(-e.y);
		else
			y = 360.0f + Math::to_deg(-e.y);
	}
	else {
		y = 180.0f + Math::to_deg(e.y);
	}

	return y;
}

bool PlayerObject::Update(float dt) {
	// Update things
	// Return false if it is to be removed

	if (m_currentHealth <= 0.0f) {
		return true;
	}

	Movement(dt);
	Rotate();

	UpdateWeapon(dt);

	m_keyboard->post_frame();
	m_mouse->post_frame();

	return true;
}

void PlayerObject::UpdateWeapon(float dt) {
	if (m_pistol != nullptr) {
		m_pistol->Update(dt);
	}
	if (m_weapon != nullptr) {
		m_weapon->Update(dt);
	}
}

void PlayerObject::CleanUp() {
	if (m_weapon != nullptr) {
		delete m_weapon;
		m_weapon = nullptr;
	}
	if (m_pistol != nullptr) {
		delete m_pistol;
		m_pistol = nullptr;
	}
}

void PlayerObject::Movement(float dt) {
	if (m_keyboard->is_key_down(EKeyCode::_W))
	{
		m_move_dir.m_z = 1.0f;
	}
	else if (m_keyboard->is_key_down(EKeyCode::_S))
	{
		m_move_dir.m_z = -1.0f;
	}
	else
	{
		m_move_dir.m_z = 0.0f;
	}

	if (m_keyboard->is_key_down(EKeyCode::_D))
	{
		m_move_dir.m_x = 1.0f;
	}
	else if (m_keyboard->is_key_down(EKeyCode::_A))
	{
		m_move_dir.m_x = -1.0f;
	}
	else
	{
		m_move_dir.m_x = 0.0f;
	}

	if (m_move_dir.m_x != 0.0f || m_move_dir.m_z != 0.0f) {
		m_move_dir.normalize();
		m_object3D->translate(m_move_dir * m_move_speed * dt);

	}
}

void PlayerObject::Rotate() {
	/*float mX = 2.0f * m_mouse->get_x() / 1280.0 - 1.0f;
	float mY = 1.0f - 2.0f * m_mouse->get_y() / 720.0;*/

	Vector3 dir;
	dir.m_x = m_mouse->get_x() - 1280 / 2;
	dir.m_z = m_mouse->get_y() - 720 / 2;
	dir.normalize();

	float rotationY = atan2f(dir.m_z, dir.m_x);
	rotationY = Math::to_deg(rotationY);
	Quaternion q;
	m_object3D->setRotation(q);
	RotateY(rotationY);
}

bool PlayerObject::Fire() {
	if (m_weapon != nullptr || m_pistol != nullptr) {
		if (m_mouse->is_button_down(EMouseButton::Right)) {
			if (m_weapon != nullptr)
				if (m_weapon->GetCurrentAmmunition() > 0) {
					return m_weapon->Fire();
				}
			return m_pistol->Fire();
		}
	}
	return false;
}

void PlayerObject::Pickup(PickupObject* object) {
	switch (object->GetPickupType()) {
	case PickupType::WeaponPickup:
		if (m_weapon != nullptr) {
			if (m_weapon->GetWeaponType() == object->GetWeapon()->GetWeaponType())
			{
				m_weapon->NewAmmo(object->GetAmount());
				m_gameObjectManager->getScene()->getSoundSystem()->playSound("WEAPON_COCK", 0.5f);
			}

			else
			{
				delete m_weapon;
				m_gameObjectManager->getScene()->getSoundSystem()->playSound("WEAPON_COCK", 0.5f);
				m_weapon = object->GetWeapon();
			}
		}
		else {
			m_weapon = object->GetWeapon();
			m_gameObjectManager->getScene()->getSoundSystem()->playSound("WEAPON_COCK", 0.5f);
		}
		object->WeaponTaken();
		break;

	case PickupType::HealthPickup:
		m_currentHealth += object->GetAmount();
		m_gameObjectManager->getScene()->getSoundSystem()->playSound("WEAPON_COCK", 0.5f);
		if (m_currentHealth > m_maxHealth)
			m_currentHealth = m_maxHealth;
		break;

	case PickupType::AmmunitionPickup:
		if (m_weapon != nullptr) {
			m_weapon->NewAmmo(object->GetAmount());
		}
		m_gameObjectManager->getScene()->getSoundSystem()->playSound("WEAPON_COCK", 0.5f);
		break;
	}

	object->Pickup();
}

void PlayerObject::GiveWeapon(Weapon* weapon) {
	if (m_weapon != nullptr) {
		delete m_weapon;
	}
	m_weapon = weapon;
}

const Weapon* PlayerObject::LookAtWeapon() const {
	if (m_weapon != nullptr) {
		return m_weapon;
	}
	return m_pistol;
}

void PlayerObject::RemoveHealth(int damage) {
	m_currentHealth -= damage;
	if (m_currentHealth <= 0) {
		m_currentHealth = 0;
	}
}

int PlayerObject::GetHealth() const {
	return m_currentHealth;
}
