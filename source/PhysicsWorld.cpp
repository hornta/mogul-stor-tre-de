#include "stdafx.h"
#include "PhysicsWorld.h"

using namespace helium;
PhysicsShape::~PhysicsShape()
{
	delete m_shape;
	m_shape = nullptr;
}

PhysicsWorld::PhysicsWorld(helium::Vector3 gravity)
{
	m_broadphase = new btDbvtBroadphase();
	m_collisionConfiguration = new btDefaultCollisionConfiguration();
	m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);
	m_solver = new btSequentialImpulseConstraintSolver();
	m_world = new btDiscreteDynamicsWorld(m_dispatcher, m_broadphase, m_solver, m_collisionConfiguration);
	m_world->setGravity(btVector3(gravity.m_x, gravity.m_y, gravity.m_z));
}

PhysicsWorld::~PhysicsWorld()
{
	delete m_world;
	delete m_solver;
	delete m_dispatcher;
	delete m_collisionConfiguration;
	delete m_broadphase;

	m_world = nullptr;
	m_solver = nullptr;
	m_dispatcher = nullptr;
	m_collisionConfiguration = nullptr;
	m_broadphase = nullptr;
}

void PhysicsWorld::update(float dt)
{
	m_world->stepSimulation(dt, 10.f);
}

void PhysicsWorld::setGravity(const helium::Vector3& gravity)
{
	m_world->setGravity(btVector3(gravity.m_x, gravity.m_y, gravity.m_z));
}

PhysicsBody* PhysicsWorld::addRigidBody(PhysicsBodyDef* def)
{
	PhysicsBody* pb = new PhysicsBody();
	btDefaultMotionState* motionState = new btDefaultMotionState(btTransform(btQuaternion(def->m_rotation.y, def->m_rotation.x, def->m_rotation.z), btVector3(def->m_position.m_x, def->m_position.m_y, def->m_position.m_z)));
	btRigidBody::btRigidBodyConstructionInfo bodyCI(def->m_mass, motionState, def->m_shape->m_shape);
	btRigidBody* body = new btRigidBody(bodyCI);
	m_world->addRigidBody(body);
	body->setFlags(def->collisionFlags);
	pb->m_body = body;
	return pb;
}

void PhysicsWorld::removeRigidBody(PhysicsBody* body)
{
	m_world->removeRigidBody(body->m_body);
	delete body->m_body->getCollisionShape();
	delete body->m_body->getMotionState();
	delete body;
}

PhysicsShape* PhysicsWorld::getShape(PhysicsShapeDef* def)
{
	PhysicsShape* shape = new PhysicsShape;
	switch (def->m_type)
	{
	case SHAPE_BOX:
		shape->m_shape = new btBoxShape(btVector3(def->m_def.m_box.m_extentX, def->m_def.m_box.m_extentY, def->m_def.m_box.m_extentZ));
		break;
	case SHAPE_PLANE:
		shape->m_shape = new btStaticPlaneShape(btVector3(def->m_def.m_plane.m_normX, def->m_def.m_plane.m_normY, def->m_def.m_plane.m_normZ), def->m_def.m_plane.m_constant);
		break;
	}

	return shape;
}