#include "stdafx.h"
#include "EnemyObject.h"

#include "Geometry.hpp"
#include "Material.h"
#include "Mesh.h"

#include <iostream>

EnemyObject::EnemyObject(Object3D* obj, const Object3D* pp) {
	m_type = ObjectType::EnemyO;
	m_currentState = State::Chasing;

	m_health = 4.0f * RandomRange(0.5f, 2.0f);
	m_damage = 1.0f;
	m_currentDamage = m_damage;
	m_moveSpeed = 3.f * RandomRange(0.8f, 2.0f);
	m_dead = false;
	m_deathTimer = 0.0f;
	m_frozenTimer = 0.0f;
	m_worth = 4;

	m_object3D = obj;
	m_playerPosition = pp;
}

EnemyObject::~EnemyObject() {

}

bool EnemyObject::Update(float dt) {
	// Update things
	// Return false if it is to be removed
	m_currentDamage = m_damage;

	switch (m_currentState) {
	case State::Dead:
		m_currentDamage = 0.0f;
		m_deathTimer += dt;
		RotateY(150 * dt);
		m_object3D->scale(0.99f);
		if (m_deathTimer > 1.0f)
			return false;
		break;

	case State::Idle:
		m_move_dir = Vector3::ZERO;
		break;

	case State::Chasing:
		ChaseMovement(dt);
		break;

	case State::Frozen:
		m_frozenTimer += dt;
		m_move_dir = Vector3::ZERO;
		m_currentDamage = 0.0f;
		if (m_frozenTimer > 1.0f) {
			m_currentState = State::Chasing;
		}
	}

	return true;
}

float EnemyObject::GetHealth() const {
	return m_health;
}

void EnemyObject::RemoveHealth(float damage) {
	m_health -= damage;
}

void EnemyObject::SetDead(bool d) {
	m_dead = d;
	m_currentState = State::Dead;
}

bool EnemyObject::IsItDead() const {
	return m_dead;
}

int EnemyObject::GetWorth() const {
	return m_worth;
}

void EnemyObject::ChaseMovement(const float &dt) {
	Vector3 pp = m_playerPosition->getDerivedPosition();
	m_move_dir = pp - GetPosition();
	if (m_move_dir.length() > 0.5f) {
		m_move_dir.normalize();
		Move(m_move_dir * m_moveSpeed * dt);
		Rotation();
	}
}

float EnemyObject::GetDamage() const {
	return m_currentDamage;
}

void EnemyObject::Freeze() {
	m_currentState = State::Frozen;
	m_frozenTimer = 0.0f;
}

void EnemyObject::Rotation() {
	float rotationY = atan2f(m_move_dir.m_x, m_move_dir.m_z);
	rotationY = Math::to_deg(rotationY);
	Quaternion q;
	m_object3D->setRotation(q);
	RotateY(rotationY);
}

void EnemyObject::CleanUp() {

}
