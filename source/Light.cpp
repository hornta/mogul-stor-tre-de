#include "stdafx.h"
#include "Light.h"
#include "Color.h"

Light::Light()
	: m_castShadow(false),
	m_enabled(true),
	m_spotAngle(0.f)

{
	m_type = EObjectType::LIGHT;
	m_light_type = ELightType::UNDEFINED;

	m_direction = helium::Vector3(0.f, 0.f, 0.f);
	m_color = Color::Black;
	m_spotAngle = 0.f;
	m_attenuation = helium::Vector3(0.f, 0.f, 0.f);
	m_enabled = true;
}

DirectionalLight::DirectionalLight(helium::Vector3 direction, Color color)
{
	m_light_type = ELightType::DIRECTIONAL_LIGHT;
	m_direction = direction;
	m_direction.normalize();
	m_color = color;
}

PointLight::PointLight(helium::Vector3 attenuation, Color color)
{
	m_light_type = ELightType::POINT_LIGHT;
	m_attenuation = attenuation;
	m_color = color;
}