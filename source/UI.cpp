#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include <helium\resource\font.hpp>
#include <helium\resource/texture.hpp>
#include <helium\scene\camera.hpp>

#include "UI.h"
#include "ResourceLoader.h"

using namespace helium;


UIElement::UIElement()
{
	m_shader = -1;
	m_texture = -1;
	m_sampler = -1;
	m_vertexformat = -1;
	m_vertexbuffer = -1;
	m_indexbuffer = -1;
	m_raster = -1;
	m_depth = -1;
	m_blend = -1;
	m_drawCount = 0;
	m_size = Vector2::ZERO;
	m_type = UIElementType::UIELEMENT_UNDEFINED;
	m_vb = nullptr;
}

UIElement::~UIElement()
{
	if (m_vb != nullptr)
	{
		delete[] m_vb;
		m_vb = nullptr;
	}
}

int UIElement::getId()
{
	return m_id;
}

void UIElement::setId(int id)
{
	m_id = id;
}

void UIElement::setPosition(float x, float y)
{
	m_position.m_x = x;
	m_position.m_y = y;
	m_world = m_world.translation(x, y, 0);
	m_dirty = true;
}

void UIElement::setOffset(float x, float y)
{
	m_offset.m_x = x;
	m_offset.m_y = y;
	m_dirty = true;
}

void UIElement::setSize(float x, float y)
{
	m_size.m_x = x;
	m_size.m_y = y;
	m_dirty = true;
}

void UIElement::setTextureRect(float top, float left, float right, float bottom)
{
	m_textureRect.m_x = top;
	m_textureRect.m_y = left;
	m_textureRect.m_z = right;
	m_textureRect.m_w = bottom;
	m_dirty = true;
}

void UIElement::setTexture(const std::string& filename)
{
	m_texture = m_ui->getResourceLoader()->getTexture(m_ui->getPostfixFolder() + filename);
}

void UIElement::setShader(const std::string& filename)
{
	m_shader = m_ui->getResourceLoader()->getShader(m_ui->getPostfixFolder() + filename);
}

void UIElement::setSampler(const std::string& filename)
{
	m_sampler = m_ui->getResourceLoader()->getSampler(m_ui->getPostfixFolder() + filename);
}

void UIElement::setVertexFormat(const std::string& filename, const std::string& filenameShader)
{
	m_vertexformat = m_ui->getResourceLoader()->getVertexFormat(m_ui->getPostfixFolder() + filename, m_ui->getPostfixFolder() + filenameShader);
}

void UIElement::setBlendState(int blendstate)
{
	m_blend = blendstate;
}

void UIElement::setRasterState(int raster)
{
	m_raster = raster;
}

void UIElement::setDepthState(int depth)
{
	m_depth = depth;
}

Vector2& UIElement::getOffset()
{
	return m_offset;
}

Vector2& UIElement::getSize()
{
	return m_size;
}

void UIElement::update()
{

}

void UIElement::receiveEvent(OSEvent* event)
{
	event;
}


UIButton::UIButton()
{
	m_drawCount = 6;
	m_vb = new UIVertex[m_drawCount];
	m_isDown = false;
	m_hasChanged = true;
	m_currentState = nullptr;
	m_bIdleState = false;
	m_bDownState = false;
}

void UIButton::update()
{
	if (m_hasChanged)
	{
		if (m_isDown)
		{
			m_currentState = &m_downState;
			m_vertexbuffer = m_currentState->vertexBuffer;
		}
		else
		{
			m_currentState = &m_idleState;
			m_vertexbuffer = m_currentState->vertexBuffer;
		}
	}
}

void UIButton::construct()
{
	float startX = m_size.m_x * -m_offset.m_x;
	float startY = m_size.m_y * -m_offset.m_y;

	float endX = startX + m_size.m_x;
	float endY = startY + m_size.m_y;


	m_vb[0].position = Vector3(startX, startY, .5f);
	m_vb[1].position = Vector3(endX, startY, .5f);
	m_vb[2].position = Vector3(endX, endY, .5f);
	m_vb[3].position = Vector3(endX, endY, .5f);
	m_vb[4].position = Vector3(startX, endY, .5f);
	m_vb[5].position = Vector3(startX, startY, .5f);

	system::RenderSystem* renderSystem = ServiceLocator<system::RenderSystem>::get_service();

	if (m_bIdleState)
	{
		float left = m_idleState.textureRect.m_x;
		float right = m_idleState.textureRect.m_z;
		float top = m_idleState.textureRect.m_y;
		float bottom = m_idleState.textureRect.m_w;
		m_vb[0].texcoord = Vector2(left, top);
		m_vb[1].texcoord = Vector2(right, top);
		m_vb[2].texcoord = Vector2(right, bottom);
		m_vb[3].texcoord = Vector2(right, bottom);
		m_vb[4].texcoord = Vector2(left, bottom);
		m_vb[5].texcoord = Vector2(left, top);
		m_idleState.vertexBuffer = renderSystem->create_vertex_buffer(EBufferMode::Static, m_vb, sizeof(UIVertex), m_drawCount);
	}


	if (m_bDownState)
	{
		float left = m_downState.textureRect.m_x;
		float right = m_downState.textureRect.m_z;
		float top = m_downState.textureRect.m_y;
		float bottom = m_downState.textureRect.m_w;
		m_vb[0].texcoord = Vector2(left, top);
		m_vb[1].texcoord = Vector2(right, top);
		m_vb[2].texcoord = Vector2(right, bottom);
		m_vb[3].texcoord = Vector2(right, bottom);
		m_vb[4].texcoord = Vector2(left, bottom);
		m_vb[5].texcoord = Vector2(left, top);
		m_downState.vertexBuffer = renderSystem->create_vertex_buffer(EBufferMode::Static, m_vb, sizeof(UIVertex), m_drawCount);
	}
}

void UIButton::receiveEvent(OSEvent* event)
{
	if (event->m_type == EOSEventType::MouseButton)
	{
		if (event->m_data.mouse.button.button == EMouseButton::Left)
		{
			if (event->m_data.mouse.button.state == true)
			{
				Vector2 mousePosition = m_ui->getMousePosition();

				float startX = m_position.m_x + m_size.m_x * -m_offset.m_x;
				float startY = m_position.m_y + m_size.m_y * -m_offset.m_y;

				float endX = startX + m_size.m_x;
				float endY = startY + m_size.m_y;

				if (mousePosition.m_x >= startX && mousePosition.m_x <= endX && mousePosition.m_y >= startY && mousePosition.m_y <= endY)
				{
					m_isDown = true;

					UIEvent e;
					e.type = UIEvent::ButtonPressed;
					e.button.button_id = m_id;
					m_ui->pushEvent(e);
				}
			}
			else
			{
				if (m_isDown)
				{
					m_isDown = false;

					UIEvent e;
					e.type = UIEvent::ButtonReleased;
					e.button.button_id = m_id;
					m_ui->pushEvent(e);
				}
			}
			m_hasChanged = true;
		}
	}
}

void UIButton::setIdleState(const ButtonState& btnState)
{
	m_idleState = btnState;
	m_bIdleState = true;
}

void UIButton::setDownState(const ButtonState& btnState)
{
	m_downState = btnState;
	m_bDownState = true;
}


UIText::UIText()
{
	m_font = nullptr;
}

void UIText::update()
{
	if (m_dirty)
	{
		construct();
		m_dirty = false;
	}
}

void UIText::construct()
{
	if (m_font == nullptr || m_string.empty())
		return;

	system::RenderSystem* renderSystem = ServiceLocator<system::RenderSystem>::get_service();

	std::vector<Glyph*> glyphs;
	m_font->retrieveGlyphs(m_string, glyphs);

	m_drawCount = (int)glyphs.size() * 6;

	if (m_vb != nullptr)
	{
		delete[] m_vb;
		m_vb = nullptr;
	}
	m_vb = new UIVertex[m_drawCount];

	m_size.m_x = 0;
	// get size and width of whole text
	for (std::size_t i = 0; i < glyphs.size(); ++i)
	{
		float kerning = 0.f;
		if (i != 0)
			kerning = m_font->getKerning(glyphs[i - 1]->id, glyphs[i]->id);

		m_size.m_x += static_cast<float>(glyphs[i]->width) + kerning;
		m_size.m_y = Math::max(static_cast<float>(glyphs[i]->height), m_size.m_y);
	}

	// start position for the first glyph
	float x = m_size.m_x * -m_offset.m_x;
	float y = m_size.m_y * -m_offset.m_y;

	// end position for the last glyph
	float endX = x + glyphs[0]->width;
	float endY = y + glyphs[0]->height;

	// get width and height of the font image
	std::string fontImage = m_font->getPages()[0]->filename;
	std::string fontPath = m_ui->getPostfixFolder() + fontImage;
	resource::Texture* texture = m_ui->getResourceLoader()->getImage(fontPath);
	float width = static_cast<float>(texture->get_width());
	float height = static_cast<float>(texture->get_height());

	// Calculate vertices and uvs for each glyph
	for (std::size_t i = 0; i < glyphs.size(); ++i)
	{
		// Get the kerning between this glyph and the previous, if there are any
		float kerning = 0.f;
		if (i != 0)
			kerning = m_font->getKerning(glyphs[i - 1]->id, glyphs[i]->id);

		endY = y + glyphs[i]->height;

		m_vb[i * 6 + 0].position = Vector3(x + glyphs[i]->xOffset, y + glyphs[i]->yOffset, .5f);
		m_vb[i * 6 + 1].position = Vector3(endX + glyphs[i]->xOffset, y + glyphs[i]->yOffset, .5f);
		m_vb[i * 6 + 2].position = Vector3(endX + glyphs[i]->xOffset, endY + glyphs[i]->yOffset, .5f);
		m_vb[i * 6 + 3].position = Vector3(endX + glyphs[i]->xOffset, endY + glyphs[i]->yOffset, .5f);
		m_vb[i * 6 + 4].position = Vector3(x + glyphs[i]->xOffset, endY + glyphs[i]->yOffset, .5f);
		m_vb[i * 6 + 5].position = Vector3(x + glyphs[i]->xOffset, y + glyphs[i]->yOffset, .5f);

		float left = static_cast<float>(glyphs[i]->x) / width;
		float right = static_cast<float>(glyphs[i]->width) / width + left;
		float top = static_cast<float>(glyphs[i]->y) / height;
		float bottom = static_cast<float>(glyphs[i]->height) / height + top;

		m_vb[i * 6 + 0].texcoord = Vector2(left, top);
		m_vb[i * 6 + 1].texcoord = Vector2(right, top);
		m_vb[i * 6 + 2].texcoord = Vector2(right, bottom);
		m_vb[i * 6 + 3].texcoord = Vector2(right, bottom);
		m_vb[i * 6 + 4].texcoord = Vector2(left, bottom);
		m_vb[i * 6 + 5].texcoord = Vector2(left, top);

		x += glyphs[i]->xAdvance;
		if (i != glyphs.size() - 1)
			endX = x + glyphs[i + 1]->width;
	}

	if (m_vertexbuffer == -1)
		m_vertexbuffer = renderSystem->create_vertex_buffer(EBufferMode::Dynamic, m_vb, sizeof(UIVertex), m_drawCount);
	else
		renderSystem->update_vertex_buffer(m_vertexbuffer, m_vb, sizeof(UIVertex) * m_drawCount);

	m_texture = m_ui->getResourceLoader()->getTexture(fontPath);
	std::vector<Page*> pages = m_font->getPages();
}

void UIText::receiveEvent(OSEvent* event)
{
	event;
}

void UIText::setString(const std::string& string)
{
	if (string != m_string)
	{
		m_dirty = true;
	}
	m_string = string;
}

void UIText::setFont(const std::string& filename)
{
	m_font = m_ui->getResourceLoader()->getFont(m_ui->getPostfixFolder() + filename);
}

UIImage::UIImage()
{
	m_drawCount = 6;
	m_vb = new UIVertex[m_drawCount];
}

void UIImage::update()
{

}

void UIImage::construct()
{
	float startX = m_size.m_x * -m_offset.m_x;
	float startY = m_size.m_y * -m_offset.m_y;

	float endX = startX + m_size.m_x;
	float endY = startY + m_size.m_y;

	m_vb[0].position = Vector3(startX, startY, .5f);
	m_vb[1].position = Vector3(endX, startY, .5f);
	m_vb[2].position = Vector3(endX, endY, .5f);
	m_vb[3].position = Vector3(endX, endY, .5f);
	m_vb[4].position = Vector3(startX, endY, .5f);
	m_vb[5].position = Vector3(startX, startY, .5f);

	float left = m_textureRect.m_x;
	float right = m_textureRect.m_z;
	float top = m_textureRect.m_y;
	float bottom = m_textureRect.m_w;

	m_vb[0].texcoord = Vector2(left, top);
	m_vb[1].texcoord = Vector2(right, top);
	m_vb[2].texcoord = Vector2(right, bottom);
	m_vb[3].texcoord = Vector2(right, bottom);
	m_vb[4].texcoord = Vector2(left, bottom);
	m_vb[5].texcoord = Vector2(left, top);

	system::RenderSystem* renderSystem = ServiceLocator<system::RenderSystem>::get_service();
	m_vertexbuffer = renderSystem->create_vertex_buffer(EBufferMode::Static, m_vb, sizeof(UIVertex), m_drawCount);
}

void UIImage::receiveEvent(OSEvent* event)
{
	event;
}

UI::UI()
	: m_postfixFolder("")
{
	ServiceLocator<OSEventDispatcher>::get_service()->attach(this);
}

UI::~UI()
{
	ServiceLocator<OSEventDispatcher>::get_service()->detach(this);

	auto it = m_elements.begin();
	while (it != m_elements.end())
	{
		delete (*it);
		(*it) = nullptr;
		++it;
	}
}

void UI::init(ResourceLoader* resourceLoader)
{
	m_render_dispatcher = renderer::RenderDispatcher::create(ServiceLocator<system::RenderSystem>::get_service());
	m_renderSystem = ServiceLocator<system::RenderSystem>::get_service();
	m_camera = Matrix4::orthogonal(m_renderSystem->get_width(), m_renderSystem->get_height(), 0.f, 1.f);
	m_resourceLoader = resourceLoader;
}

void UI::postfixFolder(const std::string& folder)
{
	m_postfixFolder = folder;
}

void UI::draw()
{
	renderer::RenderCommand cmd;
	for (UIElement* e : m_elements)
	{
		// shader
		cmd.m_type = renderer::ERenderCommandType::Shader;
		cmd.m_cmd.m_id = e->m_shader;
		m_render_queue.push(cmd);

		// texture
		cmd.m_type = renderer::ERenderCommandType::Texture;
		cmd.m_cmd.m_texture.m_id = e->m_texture;
		cmd.m_cmd.m_texture.m_slot = 0;
		m_render_queue.push(cmd);

		// sampler
		cmd.m_type = renderer::ERenderCommandType::Sampler;
		cmd.m_cmd.m_sampler.m_id = e->m_sampler;
		m_render_queue.push(cmd);

		// vertex format
		cmd.m_type = renderer::ERenderCommandType::VertexFormat;
		cmd.m_cmd.m_id = e->m_vertexformat;
		m_render_queue.push(cmd);

		// vertex buffer
		cmd.m_type = renderer::ERenderCommandType::VertexBuffer;
		cmd.m_cmd.m_id = e->m_vertexbuffer;
		m_render_queue.push(cmd);

		// index buffer
		cmd.m_type = renderer::ERenderCommandType::IndexBuffer;
		cmd.m_cmd.m_id = e->m_indexbuffer;
		m_render_queue.push(cmd);

		// raster
		cmd.m_type = renderer::ERenderCommandType::Rasterizer;
		cmd.m_cmd.m_id = e->m_raster;
		m_render_queue.push(cmd);

		// depth
		cmd.m_type = renderer::ERenderCommandType::Depth;
		cmd.m_cmd.m_id = e->m_depth;
		m_render_queue.push(cmd);

		// blend
		cmd.m_type = renderer::ERenderCommandType::Blend;
		cmd.m_cmd.m_id = e->m_blend;
		m_render_queue.push(cmd);

		cmd.m_type = renderer::ERenderCommandType::ShaderConstant;
		cmd.m_cmd.m_constant.m_size = sizeof(Matrix4);
		cmd.m_cmd.m_constant.m_data = static_cast<void*>(&e->m_world);
		cmd.m_cmd.m_constant.m_hash = String::hash32("world", 5);
		m_render_queue.push(cmd);

		cmd.m_type = renderer::ERenderCommandType::ShaderConstant;
		cmd.m_cmd.m_constant.m_size = sizeof(Matrix4);
		cmd.m_cmd.m_constant.m_data = static_cast<void*>(const_cast<Matrix4*>(&m_camera));
		cmd.m_cmd.m_constant.m_hash = String::hash32("projection", 10);
		m_render_queue.push(cmd);


		// draw
		cmd.m_type = renderer::ERenderCommandType::Draw;
		cmd.m_cmd.m_draw.m_mode = static_cast<uint32_t>(system::EDrawMode::TriangleList);
		cmd.m_cmd.m_draw.m_count = e->m_drawCount;
		cmd.m_cmd.m_draw.m_start = 0;
		m_render_queue.push(cmd);
	}

	m_render_dispatcher->add_queue(&m_render_queue);
	m_render_dispatcher->process();
}

void UI::update()
{
	for (auto& e : m_elements)
	{
		e->update();
	}
}

void UI::addElement(UIElement* element)
{
	element->m_ui = this;
	m_elements.push_back(element);
}
UIElement* UI::getObjectByID(int ID)
{
	for (std::size_t i = 0; i < m_elements.size(); ++i)
	{
		if (m_elements[i]->getId() == ID)
		{
			return m_elements[i];
		}
	}
	return nullptr;
}

Vector2& UI::getMousePosition()
{
	return m_mousePosition;
}

bool UI::pollEvent(UIEvent& event)
{
	if (m_events.size() == 0)
		return false;

	event = m_events.front();
	m_events.pop();

	return true;
}

void UI::pushEvent(const UIEvent& event)
{
	m_events.push(event);
}

Matrix4& UI::getCamera()
{
	return m_camera;
}

ResourceLoader* UI::getResourceLoader()
{
	return m_resourceLoader;
}

const std::string& UI::getPostfixFolder() const
{
	return m_postfixFolder;
}

void UI::notify(OSEvent* event)
{
	if (event->m_type == EOSEventType::MouseMove)
	{
		m_mousePosition.m_x = event->m_data.mouse.move.x;
		m_mousePosition.m_y = event->m_data.mouse.move.y;
	}

	for (auto& e : m_elements)
	{
		e->receiveEvent(event);
	}
}