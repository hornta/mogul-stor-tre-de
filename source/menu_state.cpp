#include "stdafx.h"

#include <helium/system/render_system.hpp>
#include <helium/gameplay/abstract_game.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/sampler.hpp>
#include <helium/resource/vertex_format.hpp>
#include <helium/scene/scene.hpp>

#include "menu_state.h"
#include "UI.h"

enum ButtonID
{
	BUTTON_START,
	BUTTON_EXIT
};

menu_state::menu_state()
{
}

menu_state::~menu_state()
{
	
}

void menu_state::initialize()
{
	m_leaveState = false;
	m_keyboard = Keyboard::create();
	m_mouse = Mouse::create();

	m_ui = new UI();
	m_ui->init(m_game->getResourceLoader());
	m_ui->postfixFolder("gui/");

	
	float screenCenterX = static_cast<float>(m_game->renderer()->get_width()) / 2.f;
	float screenCenterY = static_cast<float>(m_game->renderer()->get_height()) / 2.f;
	{
		UIText* text = new UIText();
		m_ui->addElement(text);
		text->setPosition(screenCenterX, screenCenterY - 200);
		text->setOffset(.5f, .5f);
		text->setString("Stormogulen 3D");

		text->setFont("font.txt");
		text->setShader("gui.shader");
		text->setSampler("gui.sampler");
		text->setVertexFormat("gui.vertexformat", "gui.shader");

		text->setBlendState(m_game->renderer()->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));
		text->setRasterState(m_game->renderer()->create_rasterizer_state(EFillMode::Solid, ECullMode::Back));
		text->setDepthState(m_game->renderer()->create_depth_state(false, false, EDepthMode::LessEqual));
		text->construct();
	}

	// Start button
	{
		UIButton* button = new UIButton();
		m_ui->addElement(button);
		button->setId(BUTTON_START);
		button->setOffset(0.5f, 0.5f);
		button->setPosition(screenCenterX, screenCenterY);
		button->setSize(256.f, 150.f);

		button->setTexture("start_menu.png");
		button->setShader("gui.shader");
		button->setSampler("gui.sampler");
		button->setVertexFormat("gui.vertexformat", "gui.shader");
		button->setBlendState(m_game->renderer()->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));

		ButtonState defaultState;
		defaultState.textureRect = Vector4(0.f, 0.f, 1.f, 0.25f);

		ButtonState downState;
		downState.textureRect = Vector4(0.f, 0.25f, 1.f, 0.5f);

		button->setIdleState(defaultState);
		button->setDownState(downState);

		button->construct();
	}
	{
		// Exit button
		UIButton* button = new UIButton();
		m_ui->addElement(button);
		button->setId(BUTTON_EXIT);
		button->setOffset(0.5f, 0.5f);
		button->setPosition(screenCenterX, screenCenterY + 170.f);
		button->setSize(256.f, 150.f);

		button->setTexture("start_menu.png");
		button->setShader("gui.shader");
		button->setSampler("gui.sampler");
		button->setVertexFormat("gui.vertexformat", "gui.shader");
		button->setBlendState(m_game->renderer()->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));

		ButtonState defaultState;
		defaultState.textureRect = Vector4(0.f, 0.5f, 1.f, 0.75f);

		ButtonState downState;
		downState.textureRect = Vector4(0.f, 0.75f, 1.f, 1.f);

		button->setIdleState(defaultState);
		button->setDownState(downState);

		button->construct();
	}

	{
		UIImage* image = new UIImage();
		m_ui->addElement(image);
		image->setPosition(0, 0);
		image->setTextureRect(0.f, 0.f, 1.f, 1.f);
		image->setSize(64.f, 64.f);
		
		image->setTexture("../textures/crate.png");
		image->setShader("gui.shader");
		image->setSampler("gui.sampler");
		image->setVertexFormat("gui.vertexformat", "gui.shader");

		image->construct();
	}
}

void menu_state::shutdown()
{
	delete m_ui;
	m_ui = nullptr;
}

bool menu_state::update(float dt)
{
	dt;
	UIEvent event;
	while (m_ui->pollEvent(event))
	{
		if (event.type == UIEvent::ButtonReleased)
		{
			if (event.button.button_id == BUTTON_START)
			{
				setNext(String::hash32("loading_state", 14));
			}
			else if (event.button.button_id == BUTTON_EXIT)
			{
				setNext(0);
			}
			m_leaveState = true;
		}
	}

	m_ui->update();
	m_mouse->post_frame();
	m_keyboard->post_frame();
	return !m_leaveState;
}

void menu_state::draw()
{
	m_ui->draw();
}