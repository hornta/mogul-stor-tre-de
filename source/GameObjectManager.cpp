// GameObjectManager.cpp

#include "stdafx.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "EnemyObject.h"
#include "PlayerObject.h"
#include "ProjectileObject.h"
#include "StaticObject.h"
#include "PickupObject.h"
#include "Geometry.hpp"
#include "Material.h"
#include "Mesh.h"
#include "Weapon.h"
#include <helium\scene\scene.hpp>
#include <vector>

#include <iostream>

GameObjectManager::GameObjectManager(scene::Scene* scene, float spawnSizeXMin, float spawnSizeXMax, float spawnSizeZMin, float spawnSizeZMax) {
	m_scene = scene;
	m_mapSizeXMin = spawnSizeXMin;
	m_mapSizeXMax = spawnSizeXMax;
	m_mapSizeZMin = spawnSizeZMin;
	m_mapSizeZMax = spawnSizeZMax;
	m_playerPos = nullptr;
	m_playerIndex = -1;
}

GameObjectManager::~GameObjectManager() {

}

void GameObjectManager::AddObject(GameObject* obj) {
	m_gameObjects.push_back(obj);
}

GameObject* GameObjectManager::GetObjectByID(int id) {
	return m_gameObjects.at(id);
}

void GameObjectManager::RemoveObjectByID(int id) {
	auto it = m_gameObjects.begin();
	for (int i = 0; i < id; i++) {
		it++;
		if (it == m_gameObjects.end())
			return;
	}

	m_gameObjects.at(id)->CleanUp();
	m_scene->removeObject(m_gameObjects.at(id)->GetObject3D());
	delete m_gameObjects.at(id);
	m_gameObjects.erase(it);
}

size_t GameObjectManager::GetSize() {
	return m_gameObjects.size();
}

void GameObjectManager::RemoveAllObjects() {
	for (unsigned int i = 0; i < m_gameObjects.size(); i++) {
		m_gameObjects.at(i)->CleanUp();
		m_scene->removeObject(m_gameObjects.at(i)->GetObject3D());
		delete m_gameObjects.at(i);
	}
	m_gameObjects.clear();
}

void GameObjectManager::UpdateObjects(float dt) {
	m_scoreThisTurn = 0.0f;
	for (unsigned int i = 0; i < m_gameObjects.size(); i++) {
		if (!m_gameObjects.at(i)->Update(dt)) {
			RemoveObjectByID(i);
			i--;
			continue;
		}

		if (m_gameObjects.at(i)->GetType() == ObjectType::EnemyO) {
			EnemyObject* enemy = static_cast<EnemyObject*>(m_gameObjects.at(i));
			if (!enemy->IsItDead()) {
				if (enemy->GetHealth() <= 0.0f) {
					enemy->SetDead(true);
					m_scoreThisTurn += enemy->GetWorth();
				}
			}
		}
		else if (m_gameObjects.at(i)->GetType() == ObjectType::PlayerO) {
			PlayerObject* player = static_cast<PlayerObject*>(m_gameObjects.at(i));
			if (player->Fire()) {
				const Weapon* weapon = player->LookAtWeapon();
				Vector3 dir;
				dir.m_z = Math::sin(Math::to_rad(player->GetYRotation()));
				dir.m_x = Math::cos(Math::to_rad(player->GetYRotation()));

				if (weapon->GetWeaponType() == WeaponType::Shotgun && weapon->GetCurrentAmmunition() > 0) {


					Vector3 dir2 = dir;
					dir2.m_x += RandomRange(-0.2f, 0.2f);
					dir2.m_z += RandomRange(-0.2f, 0.2f);
					dir2.normalize();

					Vector3 dir3 = dir;
					dir3.m_x += RandomRange(-0.2f, 0.2f);
					dir3.m_z += RandomRange(-0.2f, 0.2f);
					dir3.normalize();

					Vector3 dir4 = dir;
					dir4.m_x += RandomRange(-0.2f, 0.2f);
					dir4.m_z += RandomRange(-0.2f, 0.2f);
					dir4.normalize();

					Vector3 dir5 = dir;
					dir5.m_x += RandomRange(-0.2f, 0.2f);
					dir5.m_z += RandomRange(-0.2f, 0.2f);
					dir5.normalize();

					AddBullet(player->GetPosition(), dir, weapon->GetDamage(), weapon->GetRange());
					AddBullet(player->GetPosition(), dir2, weapon->GetDamage(), weapon->GetRange());
					AddBullet(player->GetPosition(), dir3, weapon->GetDamage(), weapon->GetRange());
					AddBullet(player->GetPosition(), dir4, weapon->GetDamage(), weapon->GetRange());
					AddBullet(player->GetPosition(), dir5, weapon->GetDamage(), weapon->GetRange());

				}
				AddBullet(player->GetPosition(), dir, weapon->GetDamage(), weapon->GetRange());
			}
		}
	}

	CollisionManagement();
}

helium::scene::Scene* GameObjectManager::GetScene()
{
	return m_scene;
}

int GameObjectManager::GetScoreThisTurn() {
	return m_scoreThisTurn;
}

void GameObjectManager::CleanUp() {
	RemoveAllObjects();
}

void GameObjectManager::SpawnWave(int amount) {
	for (int i = 0; i < amount; i++) {
		AddRandomEnemy();
	}
}

void GameObjectManager::AddRandomEnemy() {

	BoxGeometry* body = new BoxGeometry();
	Mesh* bodymesh = new Mesh(body, Material::Grey);
	bodymesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/LegoRed.png"));
	bodymesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
	bodymesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
	bodymesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
	bodymesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");
	bodymesh->scale(0.75f);

	BoxGeometry* head = new BoxGeometry();
	Mesh* headmesh = new Mesh(head, Material::YellowPlastic);
	headmesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/Crate.png"));
	headmesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
	headmesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
	headmesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
	headmesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");
	headmesh->translate(Vector3(0.0f, 0.5f, 0.0f));
	headmesh->scale(0.25f);

	bodymesh->add(headmesh);

	EnemyObject* eo = new EnemyObject(bodymesh, m_playerPos);

	int spawn1 = rand() % 10;	// if 0 spawn up/down, if 1 spawn right/left
	int spawn2 = rand() % 10;	// if 0 spawn up or right, if 1 spawn down or left
	Vector3 pos;
	if (spawn1 < 4.5f) {
		// randomize x-value
		pos.m_x = RandomRange(m_mapSizeXMin, m_mapSizeXMax);
		if (spawn2 < 4.5f) {
			// up
			pos.m_z = m_mapSizeZMax - 2.0f;
		}
		else {
			// down
			pos.m_z = m_mapSizeZMin + 2.0f;
		}
	}
	else {
		// randomize z-value
		pos.m_z = RandomRange(m_mapSizeZMin, m_mapSizeZMax);
		if (spawn2 < 4.5f) {
			// right
			pos.m_x = m_mapSizeXMax - 2.0f;
		}
		else {
			// left
			pos.m_x = m_mapSizeXMin + 2.0f;
		}
	}

	eo->SetPosition(pos);

	m_gameObjects.push_back(eo);
	m_scene->add(bodymesh);
	m_scene->add(headmesh);
}

void GameObjectManager::SpawnPlayer() {
	BoxGeometry* body = new BoxGeometry();
	Mesh* bodymesh = new Mesh(body, Material::Grey);
	bodymesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/LegoRed.png"));
	bodymesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
	bodymesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
	bodymesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
	bodymesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");

	BoxGeometry* head = new BoxGeometry();
	Mesh* headmesh = new Mesh(head, Material::YellowPlastic);
	headmesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/Crate.png"));
	headmesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
	headmesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
	headmesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
	headmesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");

	headmesh->translate(Vector3(0.0f, 0.8f, 0.0f));
	headmesh->scale(0.5f);
	bodymesh->add(headmesh);

	PlayerObject* po = new PlayerObject(bodymesh, new Weapon(WeaponType::Pistol));
	m_gameObjects.push_back(po);
	m_scene->add(bodymesh);
	m_scene->add(headmesh);
	m_playerPos = po->m_object3D;
	m_playerIndex = (int)m_gameObjects.size() - 1;

	po->m_gameObjectManager = this;
}

void GameObjectManager::AddBullet(Vector3 pos, Vector3 dir, float damage, float range) {
	BoxGeometry* cube = new BoxGeometry();
	Mesh* mesh = new Mesh(cube, Material::GreenPlastic);
	mesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
	mesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
	mesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
	mesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");
	mesh->scale(0.2f);

	ProjectileObject* po = new ProjectileObject(mesh, dir, damage, range);

	po->SetPosition(pos);
	m_scene->getSoundSystem()->playSound("WEAPON_FIRE", 1.f);
	m_gameObjects.push_back(po);
	m_scene->add(mesh);
}

void GameObjectManager::AddPickup(Vector3 pos, PickupType pt, Weapon* weapon) {
	BoxGeometry* cube;
	Mesh* mesh = nullptr;

	switch (pt) 
	{
	case PickupType::WeaponPickup:
		switch (weapon->GetWeaponType()) {
		case WeaponType::Auto:
			cube = new BoxGeometry();
			mesh = new Mesh(cube, Material::Grey);
			mesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/MetalCrate.png"));
			mesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
			mesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
			mesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
			mesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");
			mesh->setScale(1.f, 0.7f, 0.7f);
			break;
		case WeaponType::Shotgun:
			cube = new BoxGeometry();
			mesh = new Mesh(cube, Material::Grey);
			mesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/WoodenCrateGreen.png"));
			mesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
			mesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
			mesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
			mesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");
			mesh->setScale(1.f, 0.7f, 0.7f);
			break;
		default:
			break;
		}
		break;

	case PickupType::AmmunitionPickup:
		cube = new BoxGeometry();
		mesh = new Mesh(cube, Material::Grey);
		mesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/AmmoCrate.png"));
		mesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
		mesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
		mesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
		mesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");
		mesh->scale(0.6f);
		break; //case PickupType::AmmunitionPickup:
	case PickupType::HealthPickup:
		cube = new BoxGeometry();
		mesh = new Mesh(cube, Material::Grey);
		mesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/WoodenCrateCross.png"));
		mesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
		mesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
		mesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
		mesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");
		mesh->scale(0.6f);
		break; //case PickupType::HealthPickup:
	default:
		break; //default:
	}

	if (mesh != nullptr)
	{
		PickupObject* po = nullptr;

		if (pt == PickupType::WeaponPickup) {
			if (weapon != nullptr)
				po = new PickupObject(mesh, m_playerPos, pt, weapon);
			else {
				printf("Weapon Nullptr in GameObjectManager::AddPickup()");
				return;
			}
		}
		else
			po = new PickupObject(mesh, m_playerPos, pt);

		po->SetPosition(pos);
		m_gameObjects.push_back(po);
		m_scene->add(mesh);
	}
}

void GameObjectManager::CollisionManagement() {
	for (unsigned i = 0; i < m_gameObjects.size(); i++) {
		AxisAlignedBoundingBox AABB1 = static_cast<Mesh*>(m_gameObjects.at(i)->m_object3D)->getAABB();
		Vector3 size1 = AABB1.m_max - AABB1.m_min;
		float radius1;
		if (size1.m_x >= size1.m_z)
			radius1 = size1.m_x / 2.0f;
		else
			radius1 = size1.m_z / 2.0f;


		for (unsigned j = 0; j < m_gameObjects.size(); j++) {
			if (i == j)	// Continue if both are the same object
				continue;

			AxisAlignedBoundingBox AABB2 = static_cast<Mesh*>(m_gameObjects.at(j)->m_object3D)->getAABB();
			Vector3 size2 = AABB2.m_max - AABB2.m_min;
			float radius2;
			if (size2.m_x >= size2.m_z)
				radius2 = size2.m_x / 2.0f;
			else
				radius2 = size2.m_z / 2.0f;

			Vector3 distanceV = m_gameObjects.at(j)->GetPosition() - m_gameObjects.at(i)->GetPosition();
			float distance = distanceV.length();
			if (distance < radius1 + radius2) {
				switch (m_gameObjects.at(i)->GetType()) {
				case ObjectType::PlayerO:
				{
					PlayerObject* player = static_cast<PlayerObject*>(m_gameObjects.at(i));

					switch (m_gameObjects.at(j)->GetType()) {
					case ObjectType::EnemyO:
					{
						EnemyObject* enemy = static_cast<EnemyObject*>(m_gameObjects.at(j));
						if (enemy->GetDamage() > 0) {
							player->RemoveHealth(enemy->GetDamage());
							enemy->Freeze();
						}
						distanceV.normalize();
						player->Move(distanceV * (radius1 + radius2 - distance) * -1);
						break; //object2 Enemy
					}

					case ObjectType::StaticO:
						distanceV.normalize();
						player->Move(distanceV * (radius1 + radius2 - distance) * -1);
						break; //object2 Static

					case ObjectType::PickupO:
					{
						PickupObject* po = static_cast<PickupObject*>(m_gameObjects.at(j));
						player->Pickup(po);
						break; //object2 Pickup
					}

					case ObjectType::PlayerProjectileO:
						// Nothing should happen
						break;
					} // End of Player switch-case
					break; //object1 Player
				}



				case ObjectType::EnemyO:
				{
					EnemyObject* enemy = static_cast<EnemyObject*>(m_gameObjects.at(i));

					switch (m_gameObjects.at(j)->GetType()) {
					case ObjectType::PlayerO:
						// Nothing happens to the enemy
						break; //object2 Player

					case ObjectType::EnemyO:
						distanceV.normalize();
						enemy->Move(distanceV * (radius1 + radius2 - distance) * -1);
						break; //object2 Enemy

					case ObjectType::StaticO:
						distanceV.normalize();
						enemy->Move(distanceV * (radius1 + radius2 - distance) * -1);
						break; //object2 Static

					case ObjectType::PickupO:
						//Nothing happens
						break; //object2 Pickup

					case ObjectType::PlayerProjectileO:
					{
						ProjectileObject* proj = static_cast<ProjectileObject*>(m_gameObjects.at(j));
						enemy->RemoveHealth(proj->GetDamage());
						proj->SetHit(true);
						break;
					}
					} // End of Enemy switch-case
					break; //object1 Enemy
				}



				case ObjectType::PlayerProjectileO:
				{
					ProjectileObject* proj = static_cast<ProjectileObject*>(m_gameObjects.at(i));

					switch (m_gameObjects.at(j)->GetType()) {
					case ObjectType::StaticO:
						proj->SetHit(true);
						break; //object2 Static
					}
					break; //object1 Projectile
				}



				case ObjectType::StaticO:
					// Nothing here (check the other cases to see what happens to them)
					break; //object1 StaticO

				case ObjectType::PickupO:
					// Nothing here (check case Player)
					break; //object1 Pickup
				}


			}


		}
	}


}

bool GameObjectManager::PlayerIsAlive() const {
	PlayerObject* player = nullptr;

	for (unsigned i = 0; i < m_gameObjects.size(); i++) {
		if (m_gameObjects.at(i)->GetType()) {
			player = static_cast<PlayerObject*>(m_gameObjects.at(i));
			break;
		}
	}
	if (player != nullptr)
		return (player->GetHealth() > 0);
	return false;
}

void GameObjectManager::SpawnMap() {
	// Top wall
	SpawnWall(Vector3(m_mapSizeXMax + m_mapSizeXMin, 0.f, m_mapSizeZMax), m_mapSizeXMax - m_mapSizeXMin, false);
	// Bot wall
	SpawnWall(Vector3(m_mapSizeXMax + m_mapSizeXMin, 0.f, m_mapSizeZMin), m_mapSizeXMax - m_mapSizeXMin, false);
	// Right wall
	SpawnWall(Vector3(m_mapSizeXMax, 0.f, m_mapSizeZMax + m_mapSizeZMin), m_mapSizeZMax - m_mapSizeZMin, true);
	// Left wall
	SpawnWall(Vector3(m_mapSizeXMin, 0.f, m_mapSizeZMax + m_mapSizeZMin), m_mapSizeZMax - m_mapSizeZMin, true);
	// Floor
	SpawnFloor(Vector3(m_mapSizeXMax + m_mapSizeXMin, -1.f, m_mapSizeZMax + m_mapSizeZMin), m_mapSizeXMax - m_mapSizeXMin, m_mapSizeZMax - m_mapSizeZMin);

}

void GameObjectManager::SpawnWall(const Vector3& pos, int length, bool vertical) {
	if (vertical) {
		Vector3 position = pos;
		position.m_z -= length / 2.0f;
		for (int l = 0; l < length + 1; l++) {
			Mesh* mesh = new Mesh(new BoxGeometry, Material::Grey);
			mesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
			mesh->constructVertexIndexBuffer(m_scene->getRenderSystem());
			mesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
			mesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/Bricks.png"));
			mesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
			
			if (l % 2 == 1) {
				mesh->scale(2.005f, 1.005f, 2.005f);
			}
			else
			{
				mesh->scale(1.995f, 0.995f, 1.995f);
			}

			StaticObject* wall = new StaticObject(mesh);

			wall->RotateY(90.f);

			wall->SetPosition(position);
			m_gameObjects.push_back(wall);
			m_scene->add(mesh);

			position.m_z += 1.0f;
		}
	}
	else {
		Vector3 position = pos;
		position.m_x -= length / 2.0f;
		for (int l = 0; l < length; l++) {
			Mesh* mesh = new Mesh(new BoxGeometry, Material::Grey);
			mesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
			mesh->constructVertexIndexBuffer(m_scene->getRenderSystem());
			mesh->shader = m_scene->getResourceLoader()->getShader("default.hlsl");
			mesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/Bricks.png"));
			mesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");

			if (l % 2 == 1) {
				mesh->scale(2.0f, 1.0f, 2.0f);
			}
			else
			{
				mesh->scale(1.99f, 0.99f, 1.99f);
			}

			StaticObject* wall = new StaticObject(mesh);

			wall->SetPosition(position);
			m_gameObjects.push_back(wall);
			m_scene->add(mesh);

			position.m_x += 1.0f;
		}

	}
}

void GameObjectManager::SpawnFloor(const Vector3& pos, int width, int length) {
	Vector3 position = pos;

	position.m_x -= width / 2.0f - 0.5f;

	for (int i = 0; i < width; i += 3) {
		position.m_z = pos.m_z - length / 2.0f + 1.0f;
		for (int j = 0; j < length; j += 3) {
			BoxGeometry* floor = new BoxGeometry();
			Mesh* mesh = new Mesh(floor, Material::GreenPlastic);
			mesh->m_material.setTexture(m_scene->getResourceLoader()->getTexture("textures/floor.png"));
			mesh->shader = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
			mesh->vertex_format = m_scene->getResourceLoader()->getVertexFormat("default.vertexformat", "default.hlsl");
			mesh->sampler = m_scene->getResourceLoader()->getSampler("default.sampler");
			mesh->vertex_buffer = m_scene->getResourceLoader()->getVertexBuffer("cube");
			mesh->scale(3.0f, 0.2f, 3.0f);

			mesh->translate(position);

			m_scene->add(mesh);

			position.m_z += 3.0f;
		}
		position.m_x += 3.0f;
	}
}

const PlayerObject* GameObjectManager::GetPlayer() const {
	for (unsigned i = 0; i < m_gameObjects.size(); i++) {
		if (m_gameObjects.at(i)->GetType() == ObjectType::PlayerO)
			return static_cast<PlayerObject*>(m_gameObjects.at(i));
	}
	return nullptr;
}

void GameObjectManager::SpawnRandomPickup() {
	PickupObject po;
	Vector3 position;
	position.m_x = RandomRange(m_mapSizeXMin - 1.0f, m_mapSizeXMax - 1.0f);
	position.m_z = RandomRange(m_mapSizeZMin - 1.0f, m_mapSizeZMax - 1.0f);

	int r = rand() % 10;
	switch (r) {
	case 0:
		AddPickup(position, PickupType::HealthPickup);
		break;
	case 1:
	case 2:
		AddPickup(position, PickupType::WeaponPickup, new Weapon(WeaponType::Auto));
		break;
	case 3:
	case 4:
		AddPickup(position, PickupType::WeaponPickup, new Weapon(WeaponType::Shotgun));
		break;
	case 5:
	case 6:
	case 7:
	case 8:
	case 9:
		AddPickup(position, PickupType::AmmunitionPickup);
		break;
	}

	
}

int GameObjectManager::GetNumberOfEnemies() {
	int enemies = 0;

	auto it = m_gameObjects.begin();
	while (it != m_gameObjects.end()) {
		if ((*it)->GetType() == ObjectType::EnemyO) {
			enemies++;
		}
		it++;
	}

	return enemies;
}