#include "stdafx.h"
#include "Material.h"

using namespace helium;
const Material Material::RedPlastic(Vector4(0, 0, 0, 1.f), Vector4(0.5f, 0, 0, 1), Vector4(0.7f, 0.6f, 0.6f, 1), 32);
const Material Material::GreenPlastic(Vector4(0, 0, 0, 1.f), Vector4(0.1f, 0.35f, 0.1f, 1), Vector4(0.45f, 0.55f, 0.45f, 1), 32);
const Material Material::YellowPlastic(Vector4(0, 0, 0, 1.f), Vector4(0.5f, 0.5f, 0, 1), Vector4(0.6f, 0.6f, 0.5f, 1), 32);
const Material Material::Grey(Vector4(0.2f, 0.2f, 0.2f, 1.f), Vector4(0.5f, 0.5f, 0.5f, 0.f), Vector4(0.0f, 0.0f, 0.0f, 1.f), 32.f);

Material::Material()
{
	initValues();
}

Material::Material(helium::Vector4 emissive, helium::Vector4 ambient, helium::Vector4 diffuse, helium::Vector4 specular, float specularPower)
{
	initValues();
	m_constant.m_emissive = emissive;
	m_constant.m_ambient = ambient;
	m_constant.m_diffuse = diffuse;
	m_constant.m_specular = specular;
	m_constant.m_specularPower = specularPower;
}

Material::Material(helium::Vector4 ambient, helium::Vector4 diffuse, helium::Vector4 specular, float specularPower)
{
	initValues();
	m_constant.m_ambient = ambient;
	m_constant.m_diffuse = diffuse;
	m_constant.m_specular = specular;
	m_constant.m_specularPower = specularPower;
}

void Material::initValues()
{
	m_textureIndex = -1;
	m_constant.m_ambient = helium::Vector4(0.1f, 0.1f, 0.1f, 1.f);
	m_constant.m_diffuse = helium::Vector4(1.f, 1.f, 1.f, 1.f);
	m_constant.m_emissive = helium::Vector4(0.f, 0.f, 0.f, 1.f);
	m_constant.m_specular = helium::Vector4(1.f, 1.f, 1.f, 1.f);
	m_constant.m_specularPower = 32;
	m_constant.m_useTexture = false;
}

void Material::setTexture(int textureIndex)
{
	m_textureIndex = textureIndex;
	if (m_textureIndex >= 0)
	{
		m_constant.m_useTexture = true;
	}
}

int Material::getTexture()
{
	return m_textureIndex;
}

Material::Constant& Material::getConstant()
{
	return m_constant;
}