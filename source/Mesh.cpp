#include "stdafx.h"
#include "Mesh.h"
#include "Material.h"
#include "Geometry.hpp"
#include <helium/system/render_system.hpp>

Mesh::Mesh(Geometry* geometry, Material material)
	: m_geometry(geometry),
		m_material(material)
{
	m_type = EObjectType::MESH;
	if (geometry->m_numIndicies > 0)
		draw_count = geometry->m_numIndicies;
	else
		draw_count = geometry->m_numVertices;
	draw_mode = EDrawMode::TriangleList;
}

Mesh::~Mesh()
{
	delete m_geometry;
	m_geometry = nullptr;
}

const AxisAlignedBoundingBox& Mesh::getAABB() const
{
	return m_aabb;
}

void Mesh::constructVertexIndexBuffer(RenderSystem* renderSystem)
{
	renderSystem;
	vertex_buffer = renderSystem->create_vertex_buffer(EBufferMode::Static, m_geometry->m_vertices, sizeof(Vertex), m_geometry->m_numVertices);
	if (m_geometry->m_numIndicies > 0)
	{
		index_buffer = renderSystem->create_index_buffer(EIndexBufferType::Int16, m_geometry->m_numIndicies, m_geometry->m_indicies);
	}
}

void Mesh::updateBound()
{

}

void Mesh::updateAABB()
{
	m_aabb.setFromPoints(m_geometry->m_vertices, m_geometry->m_numVertices);
}


PhysicsMesh::PhysicsMesh(Geometry* geometry, Material material)
	: Mesh(geometry, material),
	m_body(nullptr)
{
	m_hasPhysicsBody = true;
}

void PhysicsMesh::setBody(PhysicsBody* body)
{
	m_body = body;
}

PhysicsBody* PhysicsMesh::getBody()
{
	return m_body;
}