// ProjectileObject.cpp

#include "stdafx.h"
#include "ProjectileObject.h"

#include "Geometry.hpp"
#include "Material.h"
#include "Mesh.h"

ProjectileObject::ProjectileObject(Object3D* obj, const Vector3 &direction, float damage, float range) {
	m_type = ObjectType::PlayerProjectileO;
	m_speed = 20.0f;
	m_hitSomething = false;
	m_move_dir = Vector3::ZERO;
	m_damage = damage;
	m_range = range;
	m_disTraveled = 0.0f;

	m_object3D = obj;
	m_move_dir = direction;
	m_move_dir.normalize();
}

ProjectileObject::~ProjectileObject() {

}

bool ProjectileObject::Update(float dt) {
	if (HasItHit())
		return false;



	return Movement(dt);
}

void ProjectileObject::SetHit(bool h) {
	m_hitSomething = h;
}

bool ProjectileObject::HasItHit() const {
	return m_hitSomething;
}

bool ProjectileObject::Movement(const float &dt) {
	if (m_range > m_disTraveled) {
		Move(m_move_dir * m_speed * dt);
		m_disTraveled += m_speed * dt;
		return true;
	}
	return false;
}

float ProjectileObject::GetDamage() const {
	return m_damage;
}

void ProjectileObject::CleanUp() {

}
