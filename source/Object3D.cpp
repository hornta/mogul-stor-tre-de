#include "stdafx.h"
#include "Object3D.h"
#include "Mesh.h"

uint32_t Object3D::object_count = 0;

Object3D::Object3D()
	:m_derivedOrientation(Quaternion::IDENTITY),
	m_derivedPosition(Vector3::ZERO),
	m_derivedScale(Vector3::UNIT_SCALE),
	m_id(object_count++),
	m_matrix(Matrix4::IDENTITY),
	m_needMatrixUpdate(false),
	m_orientation(Quaternion::IDENTITY),
	m_parent(nullptr),
	m_parentNeedUpdate(false),
	m_position(Vector3::ZERO),
	m_scale(Vector3::UNIT_SCALE),
	m_type(EObjectType::OBJECT3D)
{
	needUpdate();

	shader = -1;
	raster = -1;
	sampler = -1;
	vertex_buffer = -1;
	vertex_format = -1;
	depth = -1;
	index_buffer = -1;
	draw_count = 0;
	draw_mode = EDrawMode::Invalid;
}

Object3D::~Object3D()
{

}

void Object3D::update()
{
	if (m_parentNeedUpdate)
	{
		updateFromParent();
	}

	for (auto &child : m_children)
	{
		child->update();
	}

	m_matrix.compose(getDerivedPosition(), getDerivedRotation(), getDerivedScale());
}

void Object3D::updateFromParent() const
{
	if (m_parent != nullptr)
	{
		const Quaternion& parentOrientation = m_parent->getDerivedRotation();
		m_derivedOrientation = parentOrientation * m_orientation;

		const Vector3& parentScale = m_parent->getDerivedScale();
		m_derivedScale = parentScale * m_scale;

		m_derivedPosition = parentOrientation * (parentScale * m_position);
		m_derivedPosition += m_parent->getDerivedPosition();
	}
	else
	{
		m_derivedOrientation = m_orientation;
		m_derivedScale = m_scale;
		m_derivedPosition = m_position;
	}

	m_parentNeedUpdate = false;
}

void Object3D::needUpdate()
{
	m_parentNeedUpdate = true;
	m_needMatrixUpdate = true;

	// alert all children that their parent has changed
	for (auto& child : m_children)
	{
		child->needUpdate();
	}
}


const Matrix4& Object3D::getMatrix() const
{
	return m_matrix;
}

const Quaternion& Object3D::getDerivedRotation() const
{
	if (m_parentNeedUpdate)
	{
		updateFromParent();
	}
	return m_derivedOrientation;
}

const Vector3& Object3D::getDerivedScale() const
{
	if (m_parentNeedUpdate)
	{
		updateFromParent();
	}
	return m_derivedScale;
}

const Vector3& Object3D::getDerivedPosition() const
{
	if (m_parentNeedUpdate)
	{
		updateFromParent();
	}
	return m_derivedPosition;
}

const Vector3& Object3D::getPosition() const
{
	return m_position;
}

const Quaternion& Object3D::getOrientation() const
{
	return m_orientation;
}

const Vector3& Object3D::getScale() const
{
	return m_scale;
}

void Object3D::setPosition(const Vector3& v, bool update)
{
	m_position = v;
	if (update)
	{
		needUpdate();
	}
}

void Object3D::translate(const Vector3& v, ETransformType relativeTo)
{
	switch (relativeTo)
	{
	case ETransformType::TRANSFORM_SELF:
		m_position += m_orientation * v;
		break;
	case ETransformType::TRANSFORM_WORLD:
		m_position += v;
		break;
	}

	needUpdate();
}


void Object3D::translateX(float v, ETransformType relativeTo)
{
	translate(Vector3(v, 0, 0), relativeTo);
}

void Object3D::translateY(float v, ETransformType relativeTo)
{
	translate(Vector3(0, v, 0), relativeTo);
}

void Object3D::translateZ(float v, ETransformType relativeTo)
{
	translate(Vector3(0, 0, v), relativeTo);
}

void Object3D::setRotation(const Quaternion& quaternion, bool update)
{
	m_orientation = quaternion;
	if (update)
	{
		needUpdate();
	}
}

void Object3D::rotateOnAxis(const Vector3& axis, float degrees, ETransformType relativeTo)
{
	Quaternion q;
	q.setFromAxisAngle(axis, Math::to_rad(degrees));

	switch (relativeTo)
	{
	case TRANSFORM_SELF:
		m_orientation = m_orientation * q;
		break;
	case TRANSFORM_WORLD:
		m_orientation = q * m_orientation;
		break;
	}

	needUpdate();
}

void Object3D::pitch(float degrees, ETransformType relativeTo)
{
	rotateOnAxis(Vector3(1, 0, 0), degrees, relativeTo);
}

void Object3D::yaw(float degrees, ETransformType relativeTo)
{
	rotateOnAxis(Vector3(0, 1, 0), degrees, relativeTo);
}

void Object3D::roll(float degrees, ETransformType relativeTo)
{
	rotateOnAxis(Vector3(0, 0, 1), degrees, relativeTo);
}

void Object3D::setScale(const Vector3& v)
{
	m_scale = v;
	needUpdate();
}

void Object3D::setScale(float x, float y, float z)
{
	setScale(Vector3(x, y, z));
}

void Object3D::setScale(float f)
{
	setScale(Vector3(f, f, f));
}

void Object3D::scale(const Vector3& v)
{
	m_scale *= v;
	needUpdate();
}

void Object3D::scale(float x, float y, float z)
{
	scale(Vector3(x, y, z));
}

void Object3D::scale(float f)
{
	scale(Vector3(f, f, f));
}

void Object3D::lookAt(const Vector3& target)
{
	Matrix4 mat = Matrix4::IDENTITY;
	mat.lookAt(target, m_position, Vector3::Up);
	m_orientation.setFromRotationMatrix(mat);
	needUpdate();
}

void Object3D::setDirection(const Vector3& v, ETransformType relativeTo)
{
	v;
	relativeTo;
}

void Object3D::add(Object3D* node)
{
	node->setParent(this);
	m_children.push_back(node);
}

void Object3D::setParent(Object3D* node)
{
	m_parent = node;

	// request update from parent
	needUpdate();
}

bool Object3D::hasChildren()
{
	return m_children.size() > 0;
}

std::vector<Object3D*>& Object3D::getChildren()
{
	return m_children;
}