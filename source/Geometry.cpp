#include "stdafx.h"
#include "Geometry.hpp"
#include "ResourceLoader.h"
#include <helium\service_locator.hpp>

using namespace helium;

Geometry::Geometry()
{
	m_numVertices = 0;
	m_numIndicies = 0;
	m_vertices = nullptr;
	m_indicies = nullptr;
}

Geometry::~Geometry()
{
	if (m_numVertices > 0)
	{
		delete[] m_vertices;
		m_vertices = nullptr;
	}

	if (m_numIndicies > 0)
	{
		delete[] m_indicies;
		m_indicies = nullptr;
	}
}


void Geometry::computeBoundingSphere()
{

}

// box geometry
BoxGeometry::BoxGeometry()
{
	m_numVertices = 36;
	m_vertices = new Vertex[m_numVertices];

	uint32_t index = 0;
	
	// front
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, -0.5f), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) };
	m_vertices[index++] = { Vector3(0.5f, 0.5f, -0.5f), Vector2(1.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, -0.5f), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, -0.5f), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) };
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, -0.5f), Vector2(0.0f, 1.0f), Vector3(0.0f, 0.0f, -1.0f) };
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, -0.5f), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, -1.0f) };

	// right
	m_vertices[index++] = { Vector3(0.5f, 0.5f, -0.5f), Vector2(0.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, 0.5f, 0.5f), Vector2(1.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, 0.5f), Vector2(1.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, 0.5f), Vector2(1.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, -0.5f), Vector2(0.0f, 1.0f), Vector3(1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, 0.5f, -0.5f), Vector2(0.0f, 0.0f), Vector3(1.0f, 0.0f, 0.0f) };

	// back
	m_vertices[index++] = { Vector3(0.5f, 0.5f, 0.5f), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) };
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, 0.5f), Vector2(1.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) };
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, 0.5f), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) };
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, 0.5f), Vector2(1.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, 0.5f), Vector2(0.0f, 1.0f), Vector3(0.0f, 0.0f, 1.0f) };
	m_vertices[index++] = { Vector3(0.5f, 0.5f, 0.5f), Vector2(0.0f, 0.0f), Vector3(0.0f, 0.0f, 1.0f) };

	// left
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, 0.5f), Vector2(0.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, -0.5f), Vector2(1.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, -0.5f), Vector2(1.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, -0.5f), Vector2(1.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, 0.5f), Vector2(0.0f, 1.0f), Vector3(-1.0f, 0.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, 0.5f), Vector2(0.0f, 0.0f), Vector3(-1.0f, 0.0f, 0.0f) };

	// top
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, 0.5f), Vector2(0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, 0.5f, 0.5f), Vector2(1.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, 0.5f, -0.5f), Vector2(1.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, 0.5f, -0.5f), Vector2(1.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, -0.5f), Vector2(0.0f, 1.0f), Vector3(0.0f, 1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, 0.5f, 0.5f), Vector2(0.0f, 0.0f), Vector3(0.0f, 1.0f, 0.0f) };

	// bottom
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, -0.5f), Vector2(0.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, -0.5f), Vector2(1.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, 0.5f), Vector2(1.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(0.5f, -0.5f, 0.5f), Vector2(1.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, 0.5f), Vector2(0.0f, 1.0f), Vector3(0.0f, -1.0f, 0.0f) };
	m_vertices[index++] = { Vector3(-0.5f, -0.5f, -0.5f), Vector2(0.0f, 0.0f), Vector3(0.0f, -1.0f, 0.0f) };

	ResourceLoader* resourceLoader = ServiceLocator<ResourceLoader>().get_service();
	resourceLoader->loadVertexBuffer("cube", m_vertices, m_numVertices);
}

BoxGeometry::~BoxGeometry()
{

}

PlaneBufferGeometry::PlaneBufferGeometry(float width, float height, uint32_t widthSegments, uint32_t heightSegments)
{
	m_width = width;
	m_height = height;
	m_normal = Vector3(0, 1, 0);

	float halfWidth = width / 2.f;
	float halfHeight = height / 2.f;

	uint32_t gridX = widthSegments;
	uint32_t gridY = heightSegments;

	uint32_t gridX1 = gridX + 1;
	uint32_t gridY1 = gridY + 1;

	float segmentWidth = width / widthSegments;
	float segmentHeight = height / heightSegments;

	m_numIndicies = gridX * gridY * 6;
	m_indicies = new uint16_t[m_numIndicies];
	m_numVertices = gridX1 * gridY1;
	m_vertices = new Vertex[m_numVertices];

	for (uint32_t y = 0; y < gridY1; y++)
	{
		for (uint32_t x = 0; x < gridX1; x++)
		{
			uint32_t index = y * gridX1 + x;
			float tx = float(x) / float(gridX);
			float ty = float(y) / float(gridY);
			m_vertices[index].position = Vector3(x * segmentWidth - halfWidth, 0.f, y * segmentHeight - halfHeight);
			m_vertices[index].texcoord = Vector2(tx, ty);
			m_vertices[index].normal = Vector3(0.f, 0.f, 0.f);
		}
	}

	uint32_t iindex = 0;
	for (uint32_t y = 0; y < gridX; ++y)
	{
		for (uint32_t x = 0; x < gridY; ++x)
		{
			float a = x     + y     * gridX1;
			float b = (x+1) + y     * gridX1;
			float c = (x+1) + (y+1) * gridX1;
			float d = x     + (y+1) * gridX1;

			m_indicies[iindex++] = a;
			m_indicies[iindex++] = c;
			m_indicies[iindex++] = b;
			m_indicies[iindex++] = d;
			m_indicies[iindex++] = c;
			m_indicies[iindex++] = a;
		}
	}

	for (uint32_t i = 0; i < m_numIndicies; i += 3)
	{
		Vector3& p0 = m_vertices[m_indicies[i + 0]].position;
		Vector3& p1 = m_vertices[m_indicies[i + 1]].position;
		Vector3& p2 = m_vertices[m_indicies[i + 2]].position;

		Vector3 q0 = p1 - p0;
		Vector3 q1 = p2 - p0;
		Vector3 n = q0.cross(q1);
		n.normalize();

		m_vertices[m_indicies[i + 0]].normal += n;
		m_vertices[m_indicies[i + 1]].normal += n;
		m_vertices[m_indicies[i + 2]].normal += n;
	}

	for (uint32_t i = 0; i < m_numVertices; i++)
	{
		m_vertices[i].normal.normalize();
	}
}

PlaneBufferGeometry::~PlaneBufferGeometry()
{

}

const Vector3& PlaneBufferGeometry::getNormal()
{
	return m_normal;
}

const Vector2 PlaneBufferGeometry::getSize()
{
	return Vector2(m_width, m_height);
}