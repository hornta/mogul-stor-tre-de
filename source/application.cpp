// application.cpp

#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/os_event.hpp>
#include "Game.h"
#include <application.hpp>

#include <time.h>

using namespace helium;

Application::Application()
{
	srand(time(NULL));
	m_running = false;
}

Application::~Application()
{
}

bool Application::initialize(HWND window_handle, int width, int height)
{
	// create os event dispatcher and set service locator
	// this is used by all other systems to receive imporant events
	m_event_dispatcher = OSEventDispatcher::create();
	ServiceLocator<OSEventDispatcher>::set_service(m_event_dispatcher.get());

	// create core systems
	m_render_system = system::RenderSystem::create(window_handle, width, height);
	m_audio_system = system::AudioSystem::create();
	m_collision_system = system::CollisionSystem::create();
	m_resource_cache = resource::ResourceCache::create();

	// default settings
	m_resource_cache->set_directory("../data/");

	// service locators
	ServiceLocator<system::RenderSystem>::set_service(m_render_system.get());
	ServiceLocator<system::AudioSystem>::set_service(m_audio_system.get());
	ServiceLocator<system::CollisionSystem>::set_service(m_collision_system.get());
	ServiceLocator<resource::ResourceCache>::set_service(m_resource_cache.get());

	// game
	m_game = Game::create();
	m_game->initialize();

	// attach this to messages
	m_event_dispatcher->attach(this);

	Debug::write(EDebugLevel::Info, "application: initialization complete.");

	return m_running = true;
}

void Application::shutdown()
{
	// cleanup
	m_game->shutdown();
	m_event_dispatcher->detach(this);

	Debug::write(EDebugLevel::Info, "application: shutdown complete.");
}

bool Application::process()
{
	m_event_dispatcher->process();
	if (!m_game->process())
		m_running = false;
	return m_running;
}

// private
void Application::notify(OSEvent* event)
{
	if (event->m_type == EOSEventType::Close)
		m_running = false;
}
