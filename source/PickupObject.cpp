// PickupObject.cpp

#include "stdafx.h"
#include "PickupObject.h"
#include "Object3D.h"
#include "Weapon.h"

	PickupObject::PickupObject() {

	}

	PickupObject::PickupObject(Object3D* obj, const Object3D* pp, PickupType pt, Weapon* weapon) {
		m_object3D = obj;
		m_playerPosition = pp;
		m_playerWithinReach = false;
		m_pickedup = false;
		m_pickupType = pt;
		m_type = PickupO;

		switch (pt) {
		case PickupType::WeaponPickup:
			m_weapon = weapon;
			m_amount = 1.0f;
			break;
		case PickupType::HealthPickup:
			m_amount = 10.0f;
			break;
		case PickupType::AmmunitionPickup:
			m_amount = 20.0f;
			break;
		}
	}

	PickupObject::~PickupObject() {

	}

	bool PickupObject::Update(float dt) {
		dt;
		if ((m_playerPosition->getDerivedPosition() - GetPosition()).length() < 1.0f)
			m_playerWithinReach = true;
		else
			m_playerWithinReach = false;

		return !m_pickedup;
	}

	PickupType PickupObject::GetPickupType() {
		return m_pickupType;
	}

	float PickupObject::GetAmount() {
		return m_amount;
	}

	bool PickupObject::WithinReach() {
		return m_playerWithinReach;
	}

	void PickupObject::Pickup() {
		m_pickedup = true;
	}

	Weapon* PickupObject::GetWeapon() {
		return m_weapon;
	}

	void PickupObject::WeaponTaken() {
		m_weapon = nullptr;
	}

	void PickupObject::CleanUp() {
		if (m_weapon != nullptr) {
			delete m_weapon;
			m_weapon = nullptr;
		}

		m_playerPosition = nullptr;
	}
