#include "stdafx.h"
#include <helium/service_locator.hpp>
#include <helium/system/render_system.hpp>
#include <helium/os_event.hpp>
#include <helium/os_event_dispatcher.hpp>
#include "GUI.h"
#include <helium\scene\camera.hpp>

using namespace helium;


GUIElement::GUIElement()
{
	m_shader = -1;
	m_texture = -1;
	m_sampler = -1;
	m_vertexformat = -1;
	m_vertexbuffer = -1;
	m_indexbuffer = -1;
	m_raster = -1;
	m_depth = -1;
	m_blend = -1;
	m_drawCount = 0;
	m_type = GUIElementType::GUIELEMENT_UNDEFINED;
}

int GUIElement::getId()
{
	return m_id;
}

void GUIElement::setId(int id)
{
	m_id = id;
}

void GUIElement::setPosition(float x, float y)
{
	m_position.m_x = x;
	m_position.m_y = y;
	m_world = m_world.translation(x, y, 0);
}

void GUIElement::setOffset(float x, float y)
{
	m_offset.m_x = x;
	m_offset.m_y = y;
}

void GUIElement::setSize(float x, float y)
{
	m_size.m_x = x;
	m_size.m_y = y;
}

void GUIElement::setTextureRect(float top, float left, float right, float bottom)
{
	m_textureRect.m_x = top;
	m_textureRect.m_y = left;
	m_textureRect.m_z = right;
	m_textureRect.m_w = bottom;
}

void GUIElement::setTexture(int texture)
{
	m_texture = texture;
}

void GUIElement::setShader(int shader)
{
	m_shader = shader;
}

void GUIElement::setSampler(int sampler)
{
	m_sampler = sampler;
}

void GUIElement::setVertexFormat(int vertexformat)
{
	m_vertexformat = vertexformat;
}

void GUIElement::setBlendState(int blendstate)
{
	m_blend = blendstate;
}

Vector2& GUIElement::getOffset()
{
	return m_offset;
}

Vector2& GUIElement::getSize()
{
	return m_size;
}

void GUIElement::update()
{

}

void GUIElement::receiveEvent(OSEvent* event)
{
	event;
}


GUIButton::GUIButton()
{
	m_drawCount = 6;
	m_vb = new GUIVertex[m_drawCount];
	m_isDown = false;
	m_hasChanged = true;
	m_currentState = nullptr;
	m_bIdleState = false;
	m_bDownState = false;
}

void GUIButton::update()
{
	if (m_hasChanged)
	{
		if (m_isDown)
		{
			m_currentState = &m_downState;
			m_vertexbuffer = m_currentState->vertexBuffer;
		}
		else
		{
			m_currentState = &m_idleState;
			m_vertexbuffer = m_currentState->vertexBuffer;
		}
	}
}

void GUIButton::construct()
{
	float startX = m_size.m_x * -m_offset.m_x;
	float startY = m_size.m_y * -m_offset.m_y;

	float endX = startX + m_size.m_x;
	float endY = startY + m_size.m_y;


	m_vb[0].position = Vector3(startX, startY, .5f);
	m_vb[1].position = Vector3(endX, startY, .5f);
	m_vb[2].position = Vector3(endX, endY, .5f);
	m_vb[3].position = Vector3(endX, endY, .5f);
	m_vb[4].position = Vector3(startX, endY, .5f);
	m_vb[5].position = Vector3(startX, startY, .5f);

	system::RenderSystem* renderSystem = ServiceLocator<system::RenderSystem>::get_service();

	if (m_bIdleState)
	{
		float left = m_idleState.textureRect.m_x;
		float right = m_idleState.textureRect.m_z;
		float top = m_idleState.textureRect.m_y;
		float bottom = m_idleState.textureRect.m_w;
		m_vb[0].texcoord = Vector2(left, top);
		m_vb[1].texcoord = Vector2(right, top);
		m_vb[2].texcoord = Vector2(right, bottom);
		m_vb[3].texcoord = Vector2(right, bottom);
		m_vb[4].texcoord = Vector2(left, bottom);
		m_vb[5].texcoord = Vector2(left, top);
		m_idleState.vertexBuffer = renderSystem->create_vertex_buffer(EBufferMode::Static, m_vb, sizeof(GUIVertex), m_drawCount);
	}


	if (m_bDownState)
	{
		float left = m_downState.textureRect.m_x;
		float right = m_downState.textureRect.m_z;
		float top = m_downState.textureRect.m_y;
		float bottom = m_downState.textureRect.m_w;
		m_vb[0].texcoord = Vector2(left, top);
		m_vb[1].texcoord = Vector2(right, top);
		m_vb[2].texcoord = Vector2(right, bottom);
		m_vb[3].texcoord = Vector2(right, bottom);
		m_vb[4].texcoord = Vector2(left, bottom);
		m_vb[5].texcoord = Vector2(left, top);
		m_downState.vertexBuffer = renderSystem->create_vertex_buffer(EBufferMode::Static, m_vb, sizeof(GUIVertex), m_drawCount);
	}
}

void GUIButton::receiveEvent(OSEvent* event)
{
	if (event->m_type == EOSEventType::MouseButton)
	{
		if (event->m_data.mouse.button.button == EMouseButton::Left)
		{
			if (event->m_data.mouse.button.state == true)
			{
				Vector2 mousePosition = m_gui->getMousePosition();

				float startX = m_position.m_x + m_size.m_x * -m_offset.m_x;
				float startY = m_position.m_y + m_size.m_y * -m_offset.m_y;

				float endX = startX + m_size.m_x;
				float endY = startY + m_size.m_y;

				if (mousePosition.m_x >= startX && mousePosition.m_x <= endX && mousePosition.m_y >= startY && mousePosition.m_y <= endY)
				{
					m_isDown = true;

					GUIEvent e;
					e.type = GUIEvent::ButtonPressed;
					e.button.button_id = m_id;
					m_gui->pushEvent(e);
				}
			}
			else
			{
				m_isDown = false;

				GUIEvent e;
				e.type = GUIEvent::ButtonReleased;
				e.button.button_id = m_id;
				m_gui->pushEvent(e);
			}
			m_hasChanged = true;
		}
	}
}

void GUIButton::setIdleState(const ButtonState& btnState)
{
	m_idleState = btnState;
	m_bIdleState = true;
}

void GUIButton::setDownState(const ButtonState& btnState)
{
	m_downState = btnState;
	m_bDownState = true;
}

GUI::GUI()
{
	ServiceLocator<OSEventDispatcher>::get_service()->attach(this);
}

GUI::~GUI()
{
	ServiceLocator<OSEventDispatcher>::get_service()->detach(this);

	for (std::size_t i = 0; i < m_elements.size(); ++i)
	{
		delete m_elements[i];
		m_elements[i] = nullptr;
		m_elements.erase(m_elements.begin(), m_elements.begin() + i);
	}
}

void GUI::init()
{
	m_render_dispatcher = renderer::RenderDispatcher::create(ServiceLocator<system::RenderSystem>::get_service());
	m_renderSystem = ServiceLocator<system::RenderSystem>::get_service();
	m_camera = Matrix4::orthogonal(m_renderSystem->get_width(), m_renderSystem->get_height(), 0.f, 1.f);
}

void GUI::draw()
{
	renderer::RenderCommand cmd;
	for (GUIElement* e : m_elements)
	{
		// shader
		cmd.m_type = renderer::ERenderCommandType::Shader;
		cmd.m_cmd.m_id = e->m_shader;
		m_render_queue.push(cmd);

		// texture
		cmd.m_type = renderer::ERenderCommandType::Texture;
		cmd.m_cmd.m_texture.m_id = e->m_texture;
		cmd.m_cmd.m_texture.m_slot = 0;
		m_render_queue.push(cmd);

		// sampler
		cmd.m_type = renderer::ERenderCommandType::Sampler;
		cmd.m_cmd.m_sampler.m_id = e->m_sampler;
		m_render_queue.push(cmd);

		// vertex format
		cmd.m_type = renderer::ERenderCommandType::VertexFormat;
		cmd.m_cmd.m_id = e->m_vertexformat;
		m_render_queue.push(cmd);

		// vertex buffer
		cmd.m_type = renderer::ERenderCommandType::VertexBuffer;
		cmd.m_cmd.m_id = e->m_vertexbuffer;
		m_render_queue.push(cmd);

		// index buffer
		cmd.m_type = renderer::ERenderCommandType::IndexBuffer;
		cmd.m_cmd.m_id = e->m_indexbuffer;
		m_render_queue.push(cmd);

		// blend
		cmd.m_type = renderer::ERenderCommandType::Blend;
		cmd.m_cmd.m_id = e->m_blend;
		m_render_queue.push(cmd);

		cmd.m_type = renderer::ERenderCommandType::ShaderConstant;
		cmd.m_cmd.m_constant.m_size = sizeof(Matrix4);
		cmd.m_cmd.m_constant.m_data = static_cast<void*>(&e->m_world);
		cmd.m_cmd.m_constant.m_hash = String::hash32("world", 5);
		m_render_queue.push(cmd);

		cmd.m_type = renderer::ERenderCommandType::ShaderConstant;
		cmd.m_cmd.m_constant.m_size = sizeof(Matrix4);
		cmd.m_cmd.m_constant.m_data = static_cast<void*>(const_cast<Matrix4*>(&m_camera));
		cmd.m_cmd.m_constant.m_hash = String::hash32("projection", 10);
		m_render_queue.push(cmd);


		// draw
		cmd.m_type = renderer::ERenderCommandType::Draw;
		cmd.m_cmd.m_draw.m_mode = static_cast<uint32_t>(system::EDrawMode::TriangleList);
		cmd.m_cmd.m_draw.m_count = e->m_drawCount;
		cmd.m_cmd.m_draw.m_start = 0;
		m_render_queue.push(cmd);
	}

	m_render_dispatcher->add_queue(&m_render_queue);
	m_render_dispatcher->process();
}

void GUI::update()
{
	for (auto& e : m_elements)
	{
		e->update();
	}
}

void GUI::addButton(GUIElement* element)
{
	element->m_gui = this;
	m_elements.push_back(element);
}

Vector2& GUI::getMousePosition()
{
	return m_mousePosition;
}

bool GUI::pollEvent(GUIEvent& event)
{
	if (m_events.size() == 0)
		return false;

	event = m_events.front();
	m_events.pop();

	return true;
}

void GUI::pushEvent(const GUIEvent& event)
{
	m_events.push(event);
}

Matrix4& GUI::getCamera()
{
	return m_camera;
}

void GUI::notify(OSEvent* event)
{
	if (event->m_type == EOSEventType::MouseMove)
	{
		m_mousePosition.m_x = event->m_data.mouse.move.x;
		m_mousePosition.m_y = event->m_data.mouse.move.y;
	}

	for (auto& e : m_elements)
	{
		e->receiveEvent(event);
	}
}