// HudManager.cpp

#include "stdafx.h"
#include "HudManager.h"
#include <helium/scene/scene.hpp>
#include "UI.h"
#include "PlayerObject.h"
#include "Weapon.h"
#include "GameStats.h"

enum weapon_type
{
	PISTOL = 137,
	MP5 = 888,
	SHOTGUN = 80085
};

HudManager::HudManager() {
	m_player = nullptr;
	m_gameStats = nullptr;
	m_ui = nullptr;

	m_health = nullptr;
	m_ammo = nullptr;
	m_weapon = nullptr;
	m_wave = nullptr;
	m_score = nullptr;
}

HudManager::~HudManager() {

}

void HudManager::Init(scene::Scene* scene, const GameStats* stats, const PlayerObject* player) {

	RenderSystem* renderSystem = scene->getRenderSystem();

	m_gameStats = stats;
	m_player = player;

	m_ui = new UI();
	m_ui->init(scene->getResourceLoader());
	m_ui->postfixFolder("gui/");

	m_health = new UIText();
	m_ui->addElement(m_health);
	m_health->setPosition(10, 350);
	m_health->setOffset(0, 0);
	m_health->setString("");
	m_health->setFont("font1.txt");
	m_health->setShader("gui.shader");
	m_health->setSampler("gui.sampler");
	m_health->setVertexFormat("gui.vertexformat", "gui.shader");
	m_health->setBlendState(renderSystem->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));
	m_health->setRasterState(renderSystem->create_rasterizer_state(EFillMode::Solid, ECullMode::Back));
	m_health->setDepthState(renderSystem->create_depth_state(false, false, EDepthMode::LessEqual));
	m_health->construct();

	
	{
		UIImage* img = new UIImage;
		m_ui->addElement(img);
		img->setTexture("pistolen.png");
		img->setPosition(600, 600);
		img->setTextureRect(0.f, 0.f, 1.f, 1.f);
		img->setSize(64.f, 64.f);
		img->setId(weapon_type::PISTOL);

		img->setShader("gui.shader");
		img->setSampler("gui.sampler");
		img->setVertexFormat("gui.vertexformat", "gui.shader");

		img->construct();
	}
	{
		UIImage* img = new UIImage;
		m_ui->addElement(img);
		img->setTexture("MP5.png");
		img->setPosition(600, 600);
		img->setTextureRect(0.f, 0.f, 1.f, 1.f);
		img->setSize(64.f, 64.f);
		img->setId(weapon_type::MP5);

		img->setShader("gui.shader");
		img->setSampler("gui.sampler");
		img->setVertexFormat("gui.vertexformat", "gui.shader");

		img->construct();
	}
	{
		UIImage* img = new UIImage;
		m_ui->addElement(img);
		img->setTexture("shotgun.png");
		img->setPosition(600, 600);
		img->setTextureRect(0.f, 0.f, 1.f, 1.f);
		img->setSize(64.f, 64.f);
		img->setId(weapon_type::SHOTGUN);

		img->setShader("gui.shader");
		img->setSampler("gui.sampler");
		img->setVertexFormat("gui.vertexformat", "gui.shader");

		img->construct();
	}

	m_ammo = new UIText();
	m_ui->addElement(m_ammo);
	m_ammo->setPosition(10, 300);
	m_ammo->setOffset(0, 0);
	m_ammo->setString("");
	m_ammo->setFont("font1.txt");
	m_ammo->setShader("gui.shader");
	m_ammo->setSampler("gui.sampler");
	m_ammo->setVertexFormat("gui.vertexformat", "gui.shader");
	m_ammo->setBlendState(renderSystem->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));
	m_ammo->setRasterState(renderSystem->create_rasterizer_state(EFillMode::Solid, ECullMode::Back));
	m_ammo->setDepthState(renderSystem->create_depth_state(false, false, EDepthMode::LessEqual));
	m_ammo->construct();


	m_weapon = new UIText();
	m_ui->addElement(m_weapon);
	m_weapon->setPosition(10, 250);
	m_weapon->setOffset(0, 0);
	m_weapon->setString("");
	m_weapon->setFont("font1.txt");
	m_weapon->setShader("gui.shader");
	m_weapon->setSampler("gui.sampler");
	m_weapon->setVertexFormat("gui.vertexformat", "gui.shader");
	m_weapon->setBlendState(renderSystem->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));
	m_weapon->setRasterState(renderSystem->create_rasterizer_state(EFillMode::Solid, ECullMode::Back));
	m_weapon->setDepthState(renderSystem->create_depth_state(false, false, EDepthMode::LessEqual));
	m_weapon->construct();


	m_wave = new UIText();
	m_ui->addElement(m_wave);
	m_wave->setPosition(10, 100);
	m_wave->setOffset(0, 0);
	m_wave->setString("");
	m_wave->setFont("font1.txt");
	m_wave->setShader("gui.shader");
	m_wave->setSampler("gui.sampler");
	m_wave->setVertexFormat("gui.vertexformat", "gui.shader");
	m_wave->setBlendState(renderSystem->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));
	m_wave->setRasterState(renderSystem->create_rasterizer_state(EFillMode::Solid, ECullMode::Back));
	m_wave->setDepthState(renderSystem->create_depth_state(false, false, EDepthMode::LessEqual));
	m_wave->construct();


	m_score = new UIText();
	m_ui->addElement(m_score);
	m_score->setPosition(10, 150);
	m_score->setOffset(0, 0);
	m_score->setString("");
	m_score->setFont("font1.txt");
	m_score->setShader("gui.shader");
	m_score->setSampler("gui.sampler");
	m_score->setVertexFormat("gui.vertexformat", "gui.shader");
	m_score->setBlendState(renderSystem->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));
	m_score->setRasterState(renderSystem->create_rasterizer_state(EFillMode::Solid, ECullMode::Back));
	m_score->setDepthState(renderSystem->create_depth_state(false, false, EDepthMode::LessEqual));
	m_score->construct();

	m_time = new UIText();
	m_ui->addElement(m_time);
	m_time->setPosition(10, 50);
	m_time->setOffset(0, 0);
	m_time->setString("");
	m_time->setFont("font1.txt");
	m_time->setShader("gui.shader");
	m_time->setSampler("gui.sampler");
	m_time->setVertexFormat("gui.vertexformat", "gui.shader");
	m_time->setBlendState(renderSystem->create_blend_state(system::EBlendMode::SrcAlpha, system::EBlendMode::OneMinusSrcAlpha, system::EBlendMode::One, system::EBlendMode::One, system::EBlendOp::Add, system::EBlendOp::Add));
	m_time->setRasterState(renderSystem->create_rasterizer_state(EFillMode::Solid, ECullMode::Back));
	m_time->setDepthState(renderSystem->create_depth_state(false, false, EDepthMode::LessEqual));
	m_time->construct();
}

void HudManager::Update() {
	UpdateHealth();
	UpdateAmmo();
	UpdateWeapon();
	UpdateWave();
	UpdateScore();
	UpdateTime();

	m_ui->update();
}

void HudManager::UpdateHealth() {
	m_health->setString("Health:" + std::to_string(m_player->GetHealth()));
}

void HudManager::UpdateAmmo() {
	const Weapon* weapon = m_player->LookAtWeapon();
	if (weapon != nullptr) {
		if (weapon->GetWeaponType() == WeaponType::Pistol)
			m_ammo->setString("0");
		else
			m_ammo->setString(std::to_string(weapon->GetCurrentAmmunition()));
	}
}

void HudManager::UpdateWeapon() {
	const Weapon* weapon = m_player->LookAtWeapon();
	if (weapon != nullptr) {
		switch (weapon->GetWeaponType()) {
		case WeaponType::Pistol:
			m_weapon->setString("Pistol");
			m_ui->getObjectByID(weapon_type::PISTOL)->setPosition(160, 220);
			m_ui->getObjectByID(weapon_type::MP5)->setPosition(4000, 4000);
			m_ui->getObjectByID(weapon_type::SHOTGUN)->setPosition(2000, 2000);
			break;
		case WeaponType::Auto:
			m_weapon->setString("Auto");
			m_ui->getObjectByID(weapon_type::MP5)->setPosition(160, 220);
			m_ui->getObjectByID(weapon_type::PISTOL)->setPosition(4000, 4000);
			m_ui->getObjectByID(weapon_type::SHOTGUN)->setPosition(2000, 2000);
			break;
		case WeaponType::Shotgun:
			m_ui->getObjectByID(weapon_type::SHOTGUN)->setPosition(160, 220);
			m_ui->getObjectByID(weapon_type::MP5)->setPosition(4000, 4000);
			m_ui->getObjectByID(weapon_type::PISTOL)->setPosition(2000, 2000);
			m_weapon->setString("Shotgun");
		}
	}
}

void HudManager::UpdateWave() {
	m_wave->setString("WaveNumber:" + std::to_string(m_gameStats->GetActiveWaveNumber()));
}

void HudManager::UpdateScore() {
	m_score->setString("Score:" + std::to_string(m_gameStats->GetScore()));
}

void HudManager::UpdateTime() {
	m_time->setString("Time:" + std::to_string(m_gameStats->GetTime()));
}

void HudManager::Draw() {
	m_ui->draw();
}

void HudManager::CleanUp() {
	if (m_ui != nullptr) {
		delete m_ui;
		m_ui = nullptr;
	}

	m_health = nullptr;
	m_ammo = nullptr;
	m_weapon = nullptr;
	m_score = nullptr;
	m_wave = nullptr;
	m_gameStats = nullptr;
	m_player = nullptr;
}