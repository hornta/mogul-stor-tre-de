// Weapon.cpp

#include "stdafx.h"
#include "Weapon.h"


Weapon::Weapon(WeaponType type) {
	m_type = type;
	switch (type) {
	case None:
		m_rateOfFire = 0.0f;
		m_maxAmmunition = 0;
		m_currentAmmunition = 0;
		m_range = 0.0f;
		m_damage = 0.0f;
		break;
	case Pistol:
		m_rateOfFire = 0.5f;
		m_maxAmmunition = 25;
		m_currentAmmunition = 15;
		m_range = 8.0f;
		m_damage = 1.8f;
		break;
		/*case Burst:
		m_rateOfFire = 1.0f;
		m_maxAmmunition = 15;
		m_currentAmmunition = 10;
		m_range = 5.5f;
		m_damage = 2.0f;
		break;*/
	case Auto:
		m_rateOfFire = 0.1f;
		m_maxAmmunition = 75;
		m_currentAmmunition = 25;
		m_range = 11.5f;
		m_damage = 1.6f;
		break;
	case Shotgun:
		m_rateOfFire = 0.5f;
		m_maxAmmunition = 20;
		m_currentAmmunition = 10;
		m_range = 6.0f;
		m_damage = 1.4f;
		break;
	}
	m_cooldown = 10.0f;
}

Weapon::~Weapon() {

}

WeaponType Weapon::GetWeaponType() const {
	return m_type;
}

float Weapon::GetRateOfFire() const {
	return m_rateOfFire;
}

int Weapon::GetMaxAmmunition() const {
	return m_maxAmmunition;
}

int Weapon::GetCurrentAmmunition() const {
	return m_currentAmmunition;
}

float Weapon::GetRange() const {
	return m_range;
}

float Weapon::GetDamage() const {
	return m_damage;
}


bool Weapon::Fire() {
	switch (m_type) {
	case WeaponType::Pistol:
		if (m_cooldown >= m_rateOfFire) {
			m_cooldown = 0.0f;
			return true;
		}

		break;
	case WeaponType::Auto:
	case WeaponType::Shotgun:
		if (m_cooldown >= m_rateOfFire && m_currentAmmunition > 0) {
			m_currentAmmunition--;
			m_cooldown = 0.0f;
			return true;
		}
		break;
	}

	return false;
}

void Weapon::NewAmmo(int amount) {
	switch (m_type) {
	case WeaponType::Pistol:

		break;
	case WeaponType::Auto:
	case WeaponType::Shotgun:
		m_currentAmmunition += amount;
		if (m_currentAmmunition > m_maxAmmunition)
			m_currentAmmunition = m_maxAmmunition;
		break;
	}
}

void Weapon::Update(float dt) {
	m_cooldown += dt;
}
