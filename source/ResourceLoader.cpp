#include "stdafx.h"
#include "ResourceLoader.h"
#include <helium/system/render_system.hpp>
#include <helium/resource/texture.hpp>
#include <helium/resource/shader.hpp>
#include <helium/resource/vertex_format.hpp>
#include <helium/resource/sampler.hpp>
#include <helium\resource\font.hpp>

#include "Model.h"

using namespace helium;
using namespace resource;

ResourceLoader::ResourceLoader()
{
}

ResourceLoader::~ResourceLoader()
{
	{
		auto it = m_images.begin();
		while (it != m_images.end())
		{
			delete it->second;
			it->second = nullptr;
			++it;
		}
	}

	{
		auto it = m_fonts.begin();
		while (it != m_fonts.end())
		{
			delete it->second;
			it->second = nullptr;
			++it;
		}
	}
	m_images.clear();
	m_shaders.clear();
	m_textures.clear();
	m_samplers.clear();
	m_vertexFormats.clear();
	m_fonts.clear();
}

void ResourceLoader::init(helium::system::RenderSystem* renderSystem)
{
	m_renderSystem = renderSystem;
}

int ResourceLoader::getShader(const std::string& filename)
{
	auto it = m_shaders.find(filename);
	if (it != m_shaders.end())
	{
		return it->second;
	}

	resource::Shader::Ptr res = resource::Shader::create(m_shaderFolder + filename);
	if (res.get() == nullptr)
	{
		Debug::write(EDebugLevel::Warning, "ResourceLoader::getShader(" + filename + ") failed to create a resource");
		return -1;
	}

	int id = m_renderSystem->create_shader(res->get_vertex_source().c_str(), res->get_pixel_source().c_str());
	m_shaders.insert(std::make_pair(filename, id));

	if (id == -1)
	{
		res.release();
	}
	return id;
}

int ResourceLoader::getTexture(const std::string& filename)
{
	auto it = m_textures.find(filename);
	if (it != m_textures.end())
	{
		return it->second;
	}

	resource::Texture* res = getImage(filename);

	if (res == nullptr)
	{
		return -1;
	}

	int id = m_renderSystem->create_texture(res->get_format(), res->get_type(), res->get_width(), res->get_height(), res->get_depth(), res->get_pixel_data());
	m_textures.insert(std::make_pair(filename, id));
	return id;
}

int ResourceLoader::getVertexFormat(const std::string& filename, const std::string filenameShader)
{
	auto it = m_vertexFormats.find(filename);
	if (it != m_vertexFormats.end())
	{
		return it->second;
	}

	resource::VertexFormat::Ptr res = resource::VertexFormat::create(m_vertexFormatFolder + filename);
	if (res.get() == nullptr)
	{
		Debug::write(EDebugLevel::Warning, "ResourceLoader::getVertexFormat(" + filename + ", " + filenameShader + ") failed to create a resource");
		return -1;
	}

	it = m_shaders.find(filenameShader);
	if (it == m_shaders.end())
	{
		Debug::write(EDebugLevel::Warning, "ResourceLoader::getVertexFormat(" + filename + ", " + filenameShader + ") failed to find shader");
		res.release();
		return -1;
	}

	int id = m_renderSystem->create_vertex_format(const_cast<system::VertexFormatDesc*>(res->get_desc()), res->get_count(), it->second);
	m_vertexFormats.insert(std::make_pair(filename, id));

	if (id == -1)
	{
		res.release();
	}
	return id;
}

int ResourceLoader::getVertexBuffer(const std::string& name)
{
	auto it = m_vertexBuffers.find(name);
	if (it != m_vertexBuffers.end())
	{
		return it->second;
	}

	return -1;
}

int ResourceLoader::getSampler(const std::string& filename)
{
	auto it = m_samplers.find(filename);
	if (it != m_samplers.end())
	{
		return it->second;
	}

	resource::Sampler::Ptr res = resource::Sampler::create(m_samplerFolder + filename);
	if (res.get() == nullptr)
	{
		Debug::write(EDebugLevel::Warning, "ResourceLoader::getSampler(" + filename + ") failed to create a resource");
		return -1;
	}

	int id = m_renderSystem->create_sampler_state(res->get_filter_mode(), res->get_addr_u(), res->get_addr_v(), res->get_addr_w());
	m_samplers.insert(std::make_pair(filename, id));

	if (id == -1)
	{
		res.release();
	}
	return id;
}

Font* ResourceLoader::getFont(const std::string& filename)
{
	auto it = m_fonts.find(filename);
	if (it != m_fonts.end())
	{
		return it->second;
	}

	resource::Font* res = new resource::Font();
	if (!res->loadFromFile(m_samplerFolder + filename))
	{
		delete res;
		res = nullptr;
		Debug::write(EDebugLevel::Warning, "ResourceLoader::getFont(" + filename + ") failed to create a resource");
		return nullptr;
	}

	m_fonts[filename] = res;
	return res;
}

Texture* ResourceLoader::getImage(const std::string& filename)
{
	auto it = m_images.find(filename);
	if (it != m_images.end())
	{
		return it->second;
	}

	resource::Texture* res = new resource::Texture();
	
	if (!res->loadTexture(m_textureFolder + filename))
	{
		delete res;
		res = nullptr;
		Debug::write(EDebugLevel::Warning, "ResourceLoader::getImage(" + m_textureFolder + filename + ") failed to create a resource");
		return nullptr;
	}

	m_images[filename] = res;
	return res;
}

void ResourceLoader::loadVertexBuffer(const std::string& name, const void* data, uint32_t count)
{
	auto it = m_vertexBuffers.find(name);
	if (it == m_vertexBuffers.end())
	{
		int id = m_renderSystem->create_vertex_buffer(system::EBufferMode::Static, data, sizeof(Vertex), count);
		m_vertexBuffers[name] = id;
	}
}

void ResourceLoader::setShaderFolder(const std::string& folder)
{
	m_shaderFolder = folder;
}

void ResourceLoader::setTextureFolder(const std::string& folder)
{
	m_textureFolder = folder;
}

void ResourceLoader::setVertexFormatFolder(const std::string& folder)
{
	m_vertexFormatFolder = folder;
}

void ResourceLoader::setSamplerFolder(const std::string& folder)
{
	m_samplerFolder = folder;
}

void ResourceLoader::setFontFolder(const std::string& folder)
{
	m_fontFolder = folder;
}