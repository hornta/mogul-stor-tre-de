#pragma once

#include <inttypes.h>

class Material
{
public:
	struct Constant
	{
		helium::Vector4 m_emissive;
		helium::Vector4 m_ambient;
		helium::Vector4 m_diffuse;
		helium::Vector4 m_specular;
		float m_specularPower;
		int m_useTexture;
		float m_padding[2];
	};

	Material();
	Material(helium::Vector4 emissive, helium::Vector4 ambient, helium::Vector4 diffuse, helium::Vector4 specular, float specularPower);
	Material(helium::Vector4 ambient, helium::Vector4 diffuse, helium::Vector4 specular, float specularPower);

	void initValues();
	void setTexture(int textureIndex);

	int getTexture();
	Constant& getConstant();

	static const Material RedPlastic;
	static const Material GreenPlastic;
	static const Material YellowPlastic;
	static const Material Grey;

private:
	Constant m_constant;
	int m_textureIndex;
};