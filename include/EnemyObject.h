// EnemyObject.h

#pragma once

#include "GameObject.h"

class EnemyObject : public GameObject {
private:
	enum State {
		Idle,
		Chasing,
		Dead,
		Frozen,
		Invalid,
	};

public:
	EnemyObject(Object3D* obj, const Object3D* pp);
	~EnemyObject();

	bool Update(float dt);
	float GetHealth() const;
	void RemoveHealth(float damage);
	void SetDead(bool d);
	bool IsItDead() const;
	int GetWorth() const;
	void ChaseMovement(const float &dt);
	float GetDamage() const;
	void Freeze();
	void Rotation();

	void CleanUp();

private:
	float m_health;
	bool m_dead;
	int m_worth;
	float m_moveSpeed;
	float m_damage;
	float m_currentDamage;
	float m_deathTimer;
	float m_frozenTimer;
	Vector3 m_move_dir;

	State m_currentState;

	const Object3D* m_playerPosition;
};
