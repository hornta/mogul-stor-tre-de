#pragma once

#include <string>

class Model
{
public:
	Model();

	bool loadFromFile(const std::string& filepath);
private:
	std::string m_materialFile;
};