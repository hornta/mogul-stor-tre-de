// PickupObject

#pragma once

#include "GameObject.h"

class Weapon;

enum PickupType {
	HealthPickup,
	WeaponPickup,
	AmmunitionPickup,
	InvalidPickup
};

class PickupObject : public GameObject {
public:
	PickupObject();
	PickupObject(Object3D* obj, const Object3D* pp, PickupType pt, Weapon* weapon = nullptr);
	~PickupObject();

	bool Update(float dt);
	PickupType GetPickupType();
	float GetAmount();
	bool WithinReach();
	void Pickup();
	Weapon* GetWeapon();
	void WeaponTaken();

	void CleanUp();

private:
	PickupType m_pickupType;
	float m_amount;
	const Object3D* m_playerPosition;
	bool m_playerWithinReach;
	bool m_pickedup;
	Weapon* m_weapon;
};