// geometry.hpp
#pragma once

#include <inttypes.h>

using namespace helium;

struct PhysicsShape;
class Geometry
{
public:
	Geometry();
	virtual ~Geometry();
	void computeBoundingSphere();

	uint32_t m_numVertices;
	uint32_t m_numIndicies;
	Vertex* m_vertices;
	uint16_t* m_indicies;
};

class BoxGeometry : public Geometry
{
public:
	BoxGeometry();
	~BoxGeometry();
};

class PlaneBufferGeometry : public Geometry
{
public:
	PlaneBufferGeometry(float width, float height, uint32_t widthSegments = 1, uint32_t heightSegments = 1);
	~PlaneBufferGeometry();

	const Vector3& getNormal();
	const Vector2 getSize();
private:
	Vector3 m_normal;
	float m_width;
	float m_height;
};