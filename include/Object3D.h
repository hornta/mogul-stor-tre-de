#pragma once

#include <helium/math.hpp>
#include <helium/system/render_system.hpp>

using namespace helium;
using namespace helium::system;

enum EObjectType
{
	OBJECT3D,
	MESH,
	LIGHT
};

enum ETransformType
{
	TRANSFORM_SELF,
	TRANSFORM_WORLD
};

class Object3D
{
public:
	static uint32_t object_count;
	Object3D();
	virtual ~Object3D();

	void update();
	void updateFromParent() const;
	void needUpdate();

	const Matrix4& getMatrix() const;
	const Quaternion& getDerivedRotation() const;
	const Vector3& getDerivedScale() const;
	const Vector3& getDerivedPosition() const;

	const Vector3& getPosition() const;
	const Quaternion& getOrientation() const;
	const Vector3& getScale() const;

	void setPosition(const Vector3& v, bool update = true);
	void translate(const Vector3& v, ETransformType relativeTo = ETransformType::TRANSFORM_WORLD);
	void translateX(float v, ETransformType relativeTo = ETransformType::TRANSFORM_WORLD);
	void translateY(float v, ETransformType relativeTo = ETransformType::TRANSFORM_WORLD);
	void translateZ(float v, ETransformType relativeTo = ETransformType::TRANSFORM_WORLD);

	void setRotation(const Quaternion& quaternion, bool update = true);
	void rotateOnAxis(const Vector3& axis, float degrees, ETransformType relativeTo = ETransformType::TRANSFORM_SELF);
	void pitch(float degrees, ETransformType relativeTo = ETransformType::TRANSFORM_SELF);
	void yaw(float degrees, ETransformType relativeTo = ETransformType::TRANSFORM_SELF);
	void roll(float degrees, ETransformType relativeTo = ETransformType::TRANSFORM_SELF);

	void setScale(const Vector3& v);
	void setScale(float x, float y, float z);
	void setScale(float f);
	void scale(const Vector3& v);
	void scale(float x, float y, float z);
	void scale(float f);

	void lookAt(const Vector3& target);
	void setDirection(const Vector3& v, ETransformType relativeTo = ETransformType::TRANSFORM_WORLD);

	void add(Object3D* node);
	void setParent(Object3D* node);
	bool hasChildren();
	std::vector<Object3D*>& getChildren();

public:
	helium::Vector3 m_localPosition;

	int shader;
	int sampler;
	int vertex_format;
	int vertex_buffer;
	int index_buffer;
	int raster;
	int depth;
	int draw_count;
	EDrawMode draw_mode;
	uint32_t m_id;
	EObjectType m_type;

protected:
	Quaternion m_orientation;
	Vector3 m_scale;
	Vector3 m_position;
	mutable Quaternion m_derivedOrientation;
	mutable Vector3 m_derivedScale;
	mutable Vector3 m_derivedPosition;

	Matrix4 m_matrix;
	bool m_needMatrixUpdate;
	mutable bool m_parentNeedUpdate;
	bool m_physicsBodyNeedUglyTransform;

	Object3D* m_parent;
	std::vector<Object3D*> m_children;
	bool m_hasPhysicsBody;
};
