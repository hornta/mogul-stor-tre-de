// GameObject.h

#pragma once

#include "stdafx.h"
#include <helium\math.hpp>

enum ObjectType {
	Static,
	Player,
	Enemy,
	Pickup,
	Invalid,
};

using namespace helium;
class Object {
public:

	Object() {}
	virtual ~Object() {}

	virtual void SetPosition(Vector3 pos);
	virtual void SetRotation(Vector3 rot);
	
	virtual void Move(Vector3 pos);
	virtual void RotateX(float degrees);
	virtual void RotateY(float degrees);
	virtual void RotateZ(float degrees);

	virtual Vector3 GetPosition();
	virtual Vector3 GetRotation();

protected:
	ObjectType m_type;
	Vector3 m_position;
	Vector3 m_rotation;
};