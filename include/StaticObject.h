// StaticObject.h

#pragma once

#include "GameObject.h"

class StaticObject : public GameObject {
public:
	StaticObject(Object3D* obj);
	~StaticObject();

	bool Update(float dt);

	void CleanUp();

private:

};