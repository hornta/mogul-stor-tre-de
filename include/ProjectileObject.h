// ProjectileObject.h

#pragma once

#include "stdafx.h"
#include "GameObject.h"

class ProjectileObject : public GameObject {
public:
	ProjectileObject(Object3D* obj, const Vector3 &direction, float damage, float range);
	~ProjectileObject();

	bool Update(float dt);
	void SetHit(bool h);
	bool HasItHit() const;
	bool Movement(const float &dt);
	float GetDamage() const;

	void CleanUp();

private:
	float m_speed;
	Vector3 m_move_dir;
	float m_damage;
	bool m_hitSomething;
	float m_range;
	float m_disTraveled;
};
