// PlayerObject.h

#pragma once

#include "GameObject.h"
#include "stdafx.h"
#include "PickupObject.h"
#include "SoundSystem.h"

class Object3D;

class PlayerObject : public GameObject {
public:
	PlayerObject(Object3D* obj_3D, Weapon* pistol);
	~PlayerObject();

	float GetYRotation();

	bool Update(float dt);
	bool Fire();
	const Weapon* LookAtWeapon() const;
	void UpdateWeapon(float dt);
	void Pickup(PickupObject* object);
	void GiveWeapon(Weapon* weapon);
	void RemoveHealth(int amount);
	int GetHealth() const;

	void CleanUp();

private:
	void Movement(float dt);
	void Rotate();

private:
	Vector3 m_move_dir;
	float m_move_speed;
	Weapon* m_weapon;
	Weapon* m_pistol;
	int m_maxHealth;
	int m_currentHealth;

	Keyboard::Ptr m_keyboard;
	Mouse::Ptr m_mouse;
};
