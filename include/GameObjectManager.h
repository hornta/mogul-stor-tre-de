// GameObjectManager.h

#pragma once

#include "stdafx.h"
#include "PickupObject.h"
#include <helium/scene/scene.hpp>

class PlayerObject;
class GameObject;

using namespace helium;

class GameObjectManager
{
public:
	GameObjectManager(scene::Scene* scene, float spawnSizeXMin, float spawnSizeXMax, float spawnSizeZMin, float spawnSizeZMax);
	~GameObjectManager();

	void AddObject(GameObject* obj);
	GameObject* GetObjectByID(int id);
	void RemoveObjectByID(int id);

	size_t GetSize();
	scene::Scene* GetScene();
	void RemoveAllObjects();

	void UpdateObjects(float dt);
	int GetScoreThisTurn();
	int GetNumberOfEnemies();

	void SpawnWave(int amount);
	void SpawnPlayer();
	void SpawnMap();
	void SpawnRandomPickup();

	bool PlayerIsAlive() const;
	const PlayerObject* GetPlayer() const;

	void CleanUp();
	scene::Scene* getScene()
	{
		return m_scene;
	}
private:
	void AddBullet(Vector3 pos, Vector3 dir, float damage, float range);
	void AddRandomEnemy();
	void CollisionManagement();
	void SpawnWall(const Vector3& pos, int length, bool vertical);
	void SpawnFloor(const Vector3& pos, int width, int length);
	void AddPickup(Vector3 pos, PickupType pt, Weapon* weapon = nullptr);

private:
	int m_scoreThisTurn;
	std::vector<GameObject*> m_gameObjects;

	float m_mapSizeXMin;
	float m_mapSizeXMax;
	float m_mapSizeZMin;
	float m_mapSizeZMax;
	const Object3D* m_playerPos;
	int m_playerIndex;

	scene::Scene* m_scene;
};