#pragma once

#include <helium/os_event_listener.hpp>
#include <helium/math.hpp>
#include <helium/renderer/render_queue.hpp>
#include <helium/renderer/render_dispatcher.hpp>

namespace helium
{
	namespace system
	{
		class RenderSystem;
	}
	namespace resource
	{
		class Font;
	}
}

using namespace helium;
using namespace helium::resource;

class ResourceLoader;

enum UIElementType
{
	UIELEMENT_UNDEFINED,
	UIELEMENT_BUTTON
};

struct UIVertex
{
	Vector3 position;
	Vector2 texcoord;
};

struct ButtonState
{
	Vector4 textureRect;
	int vertexBuffer;
};

class UIEvent
{
public:
	struct ButtonEvent
	{
		int button_id;
	};

	enum EventType
	{
		ButtonPressed,
		ButtonReleased
	};

	EventType type;

	union
	{
		ButtonEvent button;
	};
};

class UI;

class UIElement
{
public:
	friend class UI;
	UIElement();
	~UIElement();

	int getId();
	void setId(int id);
	void setPosition(float x, float y);
	void setOffset(float x, float y);
	void setSize(float x, float y);
	void setTexture(const std::string& filename);
	void setShader(const std::string& filename);
	void setSampler(const std::string& filename);
	void setVertexFormat(const std::string& filename, const std::string& filenameShader);
	void setBlendState(int blendstate);
	void setRasterState(int raster);
	void setDepthState(int depth);
	void setTextureRect(float top, float left, float right, float bottom);
	Vector2& getOffset();
	Vector2& getSize();
	virtual void update();
	virtual void receiveEvent(OSEvent* event);

	virtual void construct() = 0;
public:
	int m_shader;
	int m_texture;
	int m_sampler;
	int m_vertexformat;
	int m_vertexbuffer;
	int m_indexbuffer;
	int m_raster;
	int m_blend;
	int m_depth;
	int m_drawCount;

	Matrix4 m_world;
	std::string m_string;

protected:
	UIElementType m_type;
	UIVertex* m_vb;
	uint32_t m_numVertices;
	Vector2 m_offset;
	Vector2 m_size;
	Vector2 m_position;
	Vector4 m_textureRect;
	UI* m_ui;
	int m_id;
	bool m_dirty;
};

class UIButton : public UIElement
{
public:
	UIButton();

	void update();
	void construct();
	void receiveEvent(OSEvent* event);

	void setIdleState(const ButtonState& btnState);
	void setDownState(const ButtonState& btnState);

private:
	ButtonState m_idleState;
	ButtonState m_downState;
	ButtonState* m_currentState;
	bool m_bIdleState;
	bool m_bDownState;

	bool m_isDown;
	bool m_hasChanged;
};

class UIText : public UIElement
{
public:
	UIText();

	void update();
	void construct();
	void receiveEvent(OSEvent* event);

	void setString(const std::string& string);
	void setFont(const std::string& filename);

private:
	Font* m_font;
};

class UIImage : public UIElement
{
public:
	UIImage();

	void update();
	void construct();
	void receiveEvent(OSEvent* event);
};

class UI : OSEventListener
{
public:
	friend class UIElement;
	UI();
	~UI();

	void init(ResourceLoader* resourceLoader);
	void postfixFolder(const std::string& folder);
	void draw();
	void update();
	void addElement(UIElement* element);
	UIElement* getObjectByID(int ID);
	Vector2& getMousePosition();
	bool pollEvent(UIEvent& event);
	void pushEvent(const UIEvent& event);
	Matrix4& getCamera();
	ResourceLoader* getResourceLoader();
	const std::string& getPostfixFolder() const;
private:
	void notify(helium::OSEvent* event);

private:
	renderer::RenderQueue m_render_queue;
	renderer::RenderDispatcher::Ptr m_render_dispatcher;
	std::vector<UIElement*> m_elements;
	std::queue<UIEvent> m_events;
	system::RenderSystem* m_renderSystem;
	ResourceLoader* m_resourceLoader;
	Matrix4 m_camera;
	Vector2 m_mousePosition;
	std::string m_postfixFolder;
};