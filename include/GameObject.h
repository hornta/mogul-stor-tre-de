// GameObject.h

#pragma once

#include "stdafx.h"
#include <helium\math.hpp>

enum ObjectType {
	StaticO,
	PlayerO,
	EnemyO,
	PickupO,
	PlayerProjectileO,
	Invalid,
};
class GameObjectManager;
using namespace helium;
class GameObject {
public:
	GameObject() {}
	virtual ~GameObject() {}

	virtual void SetPosition(Vector3 pos);
	virtual void SetRotation(Vector3 rot);

	virtual void Move(Vector3 pos);
	virtual void RotateX(float degrees);
	virtual void RotateY(float degrees);
	virtual void RotateZ(float degrees);

	virtual Vector3 GetPosition();
	virtual Vector3 GetRotation();
	virtual Object3D* GetObject3D();

	virtual bool Update(float dt) = 0;
	virtual ObjectType GetType();

	virtual void CleanUp() = 0;
public:
	ObjectType m_type;
	Object3D* m_object3D;
	GameObjectManager *m_gameObjectManager;
};
