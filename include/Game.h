#pragma once

#include <helium/gameplay/abstract_game.hpp>

namespace helium
{
	class Game : public gameplay::AbstractGame
	{
	public:
		typedef std::unique_ptr<Game> Ptr;
		static gameplay::AbstractGame::Ptr create();

		Game();
		~Game();

		void initialize();
		void shutdown();

	};
}