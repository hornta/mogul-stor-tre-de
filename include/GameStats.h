// GameStats.h

#pragma once

#include "stdafx.h"

class GameStats {
public:
	GameStats();
	~GameStats();

	int GetScore() const;
	int GetActiveWaveNumber() const;
	float GetTime() const;

	void IncrementWaveNumber();
	void IncreaseScore(int amount);
	void UpdateTime(float dt);
	void ResetWaveNumber();
	void ResetScore();
	void ResetTime();

private:
	int m_waveNumber;
	int m_score;
	float m_time;
};