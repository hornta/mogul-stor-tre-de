#pragma once
#include <d3d11.h>
#include <inttypes.h>
#include "Color.h"

class ShadowMap
{
public:
	ShadowMap();
	~ShadowMap();

	bool initalize(ID3D11Device* device, uint32_t width, uint32_t height);

private:
	ID3D11Texture2D* m_shadowMap;
	ID3D11DepthStencilView* m_depthStencilView;
	ID3D11ShaderResourceView* m_shaderResourceView;
};