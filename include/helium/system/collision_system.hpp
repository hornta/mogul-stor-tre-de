// collision_system.hpp

#ifndef COLLISIONSYSTEM_HPP_INCLUDED
#define COLLISIONSYSTEM_HPP_INCLUDED

namespace helium
{
	namespace system
	{
		class CollisionShape;
		class CollisionTransform;
		class CollisionPair;
		class CollisionDetector;

		class CollisionSystem
		{
		private: // non-copyable
			CollisionSystem(const CollisionSystem&);
			CollisionSystem& operator=(const CollisionSystem&);

		public:
			typedef std::unique_ptr<CollisionSystem> Ptr;

			static Ptr create();

		private:
			CollisionSystem();

		public:
			~CollisionSystem();

			void process();
		};
	}
}

#endif // COLLISIONSYSTEM_HPP_INCLUDED
