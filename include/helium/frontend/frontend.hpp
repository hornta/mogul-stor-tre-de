// frontend.hpp

#ifndef FRONTEND_HPP_INCLUDED
#define FRONTEND_HPP_INCLUDED

#include <helium/scene/camera.hpp>
#include <helium/renderer/render_queue.hpp>
#include <helium/renderer/render_dispatcher.hpp>

namespace helium
{
	namespace frontend
	{
		/*
		note(tommi): this class is used to render all 2d 
		*/
		class FrontEnd
		{
		private:// non-copyable
			FrontEnd(const FrontEnd&);
			FrontEnd& operator=(const FrontEnd&);

		public:
			typedef std::unique_ptr<FrontEnd> Ptr;

			static Ptr create();

		private:
			FrontEnd();

		public:
			~FrontEnd();

			void pre_render();
			void post_render();
			void render();

			void set_camera(scene::Camera* camera);
			scene::Camera* get_camera();

		private:
			scene::Camera* m_camera;
			renderer::RenderQueue m_render_queue;
			renderer::RenderDispatcher::Ptr m_render_dispatcher;
		};
	}
}

#endif // FRONTEND_HPP_INCLUDED
