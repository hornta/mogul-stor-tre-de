// math.hpp

#ifndef MATH_HPP_INCLUDED
#define MATH_HPP_INCLUDED

class Object3D;
namespace helium
{
	class Quaternion;
	class Vector3;
	class Matrix4;

	class Math
	{
	public:
		static const float PI;
		static const float PI2;

		static float sqrt(float value);
		static float sin(float radian);
		static float cos(float radian);
		static float tan(float radian);
		static float to_rad(float degrees);
		static float to_deg(float radians);

		template <typename T>
		static T max(T a, T b)
		{
			return a > b ? a : b;
		}

		template <typename T>
		static T min(T a, T b)
		{
			return a < b ? a : b;
		}

		template <typename T>
		static T abs(T a)
		{
			return a < 0 ? -a : a;
		}

		template <typename T>
		static T clamp(T v, T a, T b)
		{
			return v < a ? a : v > b ? b : v;
		}

		template <typename T>
		static bool are_equal(const T& a, const T& b) { return a == b; }
		template <>
		static bool are_equal<float>(const float& a, const float& b);
		template <>
		static bool are_equal<double>(const double& a, const double& b);
		
		static Matrix4 makeViewMatrix(const Vector3& position, const Quaternion& orientation);
	};

	class Euler
	{
	public:
		Euler();

		float x;
		float y;
		float z;

		bool operator !=(const Euler& right);
		bool operator ==(const Euler& right);
	};

	class Vector2
	{
	public:
		static const Vector2 UP;
		static const Vector2 ZERO;
		Vector2();
		Vector2(float x, float y);
		Vector2(const Vector2& rhs);

		Vector2& operator=(const Vector2& rhs);
		Vector2& operator+=(const Vector2& rhs);
		Vector2& operator-=(const Vector2& rhs);
		Vector2& operator*=(const float real);
		Vector2& operator/=(const float real);
		Vector2 operator+(const Vector2&rhs);
		Vector2 operator-(const Vector2&rhs);
		Vector2 operator*(const float real);
		Vector2 operator/(const float real);

		float length() const;
		float length_squared() const;
		float dot(const Vector2& rhs) const;
		void normalize();

		float m_x;
		float m_y;
	};

	class Vector3
	{
	public:
		Vector3();
		Vector3(float x, float y, float z);
		Vector3(const Vector3& rhs);

		Vector3& operator=(const Vector3& rhs);
		Vector3& operator+=(const Vector3& rhs);
		Vector3& operator-=(const Vector3& rhs);
		Vector3& operator*=(const float real);
		Vector3& operator*=(const Vector3& rhs);
		Vector3& operator/=(const float real);
		Vector3& operator/=(const Vector3& rhs);
		Vector3 operator+(const Vector3& rhs);
		const Vector3 operator+(const Vector3& rhs) const;
		Vector3 operator-(const Vector3& rhs) const;
		Vector3 operator*(const float real);
		Vector3 operator*(const Vector3& rhs) const;
		Vector3 operator/(const float real) const;
		Vector3 operator/(const Vector3& rhs);

		const Vector3 operator*(const float real) const;

		static Vector3 min(const Vector3& left, const Vector3& right);
		static Vector3 max(const Vector3& left, const Vector3& right);
		static Vector3 normalize(const Vector3& v);

		static const Vector3 Up;
		static const Vector3 UP;
		static const Vector3 RIGHT;
		static const Vector3 FORWARD;
		static const Vector3 ZERO;
		static const Vector3 UNIT_SCALE;

		Vector3 cross(const Vector3& rhs);
		void crossVectors(const Vector3& l, const Vector3& r);
		void subVectors(const Vector3&l, const Vector3& r);
		void divideScalar(float scalar);
		void normalize();

		float length() const;
		float length_squared() const;
		float dot(const Vector3& rhs) const;

		float m_x;
		float m_y;
		float m_z;
	};

	class Matrix4;
	class Quaternion
	{
	public:
		static const Quaternion IDENTITY;
		Quaternion();
		Quaternion(float x, float y, float z, float w);
		~Quaternion();
		void set(float x, float y, float z, float w);

		void setFromAxisAngle(Vector3 axis, float angle);
		void setFromEuler(const Euler& euler);
		void setFromRotationMatrix(const Matrix4& mat);

		Euler toEuler() const;

		void normalize();
		void multiply(const Quaternion& right);
		Vector3 operator*(const Vector3& rhs) const;
		Quaternion operator*(const Quaternion& rhs) const;
		Quaternion operator*(float rhs) const;

		float x;
		float y;
		float z;
		float w;
	};

	class Vector4
	{
	public:
		Vector4();
		Vector4(float x, float y, float z, float w);
		Vector4(const Vector4& rhs);
		Vector4(const Vector3& rhs, float w);
		Vector4& operator=(const Vector4& rhs);

		float m_x;
		float m_y;
		float m_z;
		float m_w;
	};

	class Matrix4
	{
	public:
		Matrix4();
		Matrix4(float _11, float _12, float _13, float _14,
			float _21, float _22, float _23, float _24,
			float _31, float _32, float _33, float _34,
			float _41, float _42, float _43, float _44);
		Matrix4(const Matrix4& rhs);

		Matrix4& operator=(const Matrix4& rhs);
		Matrix4 operator*(const Matrix4 &rhs);
		const Matrix4 operator*(const Matrix4 &rhs) const;
		Matrix4 Matrix4::operator-() const;

		Vector3 operator*(const Vector3& rhs);

		void identity();
		Matrix4 inverse();
		void compose(const Vector3& position, const Quaternion& quaternion, const Vector3& scale);
		void makeRotationFromQuaternion(const Quaternion& q);
		void setPosition(const Vector3& p);
		void setScale(const Vector3& s);
		void lookAt(const Vector3& position, const Vector3& target, const Vector3& up);

		static const Matrix4 IDENTITY;
		static Matrix4 scale(float x, float y, float z);
		static Matrix4 scale(const Vector3 &v);
		static Matrix4 rotation(const Quaternion& quat);
		static Matrix4 rotation(const Vector3 &axis, float radians);
		static Matrix4 rotation(float x, float y, float z, float radians);
		static Matrix4 translation(float x, float y, float z);
		static Matrix4 translation(const Vector3 &v);
		static Matrix4 perspective(float fov, float aspect, float znear, float zfar);
		static Matrix4 orthogonal(float width, float height, float znear, float zfar);
		static Matrix4 transpose(const Matrix4& m);

		union
		{
			float _m[16];
			struct
			{
				float _11, _12, _13, _14;
				float _21, _22, _23, _24;
				float _31, _32, _33, _34;
				float _41, _42, _43, _44;
			} m;
		};
	};

	class Ray
	{
	public:
		Ray();
		Ray(const Ray& rhs);
		Ray(const Vector3& origin, const Vector3& direction);

		Ray& operator=(const Ray& rhs);

		Vector3 m_origin;
		Vector3 m_direction;
	};

	class Plane
	{
	public:
		Plane();
		Plane(const Plane& rhs);
		Plane(const Vector3& normal, const float D);
		Plane& operator=(const Plane& rhs);

		Vector3 m_normal;
		float m_D;
	};

	struct Vertex
	{
		Vector3 position;
		Vector2 texcoord;
		Vector3 normal;
	};

	class BoundingSphere
	{
	public:
		BoundingSphere();
		BoundingSphere(const Vector3& center, float radius);
		BoundingSphere(const BoundingSphere& rhs);
		BoundingSphere& operator=(const BoundingSphere& rhs);

		Vector3 m_center;
		float m_radius;
	};

	class AxisAlignedBoundingBox
	{
	public:
		AxisAlignedBoundingBox();
		AxisAlignedBoundingBox(const AxisAlignedBoundingBox& rhs);
		AxisAlignedBoundingBox& operator=(const AxisAlignedBoundingBox& rhs);

		void setFromPoints(const Vertex* p, int numPoints);

		Vector3 m_min;
		Vector3 m_max;
	};

	class Frustum
	{
	public:
		Frustum();
		Frustum(const Frustum& rhs);
		Frustum& operator=(const Frustum& rhs);

		void construct(Matrix4& viewproj);
		const bool is_inside(const Vector3& point) const;
		const bool is_inside(const BoundingSphere& sphere) const;
		const bool is_inside(const AxisAlignedBoundingBox& aabb) const;
		bool intersectsObject(Object3D* object) const;
		Plane m_planes[6];
	};

	class Transform
	{
	public:
		Transform();
		Transform(const Matrix4& matrix);

		const Matrix4& getMatrix() const;
		Transform getInverse() const;

		void translate(float x, float y, float z);
		void translate(const Vector3& offset);

		void rotateX(float angle);
		void rotateY(float angle);
		void rotateZ(float angle);

		void scale(float x, float y, float z);
		void scale(const Vector3& scale);

		Transform operator *(const Transform& right);
		Transform operator *=(const Transform& right);
	private:
		Matrix4 m_matrix;
	};

	float RandomRange(float min, float max);
}

#endif // MATH_HPP_INCLUDED
