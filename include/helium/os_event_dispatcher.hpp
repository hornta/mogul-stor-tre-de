// os_event_dispatcher.hpp

#ifndef OSEVENTDISPATCHER_HPP_INCLUDED
#define OSEVENTDISPATCHER_HPP_INCLUDED

#include <helium/os_event.hpp>

namespace helium
{
	class OSEventListener;

	class OSEventDispatcher
	{
	public:
		typedef std::unique_ptr<OSEventDispatcher> Ptr;

		static Ptr create();

	private:
		OSEventDispatcher();

	public:
		~OSEventDispatcher();

		void queue(OSEvent event);
		void process();

		void attach(OSEventListener* listener);
		void detach(OSEventListener* listener);

	private:
		std::queue<OSEvent> m_event_queue;
		std::list<OSEventListener*> m_listener_list;
	};
}

#endif // OSEVENTDISPATCHER_HPP_INCLUDED
