#pragma once

#include "Object3D.h"

namespace helium
{
	namespace scene
	{
		class Camera : public Object3D
		{
		public:
			Camera();
			~Camera();

			void update();

			void move_x(float amount);
			void move_y(float amount);
			void move_z(float amount);
			void move_forward(float amount);
			void move_sidestep(float amount);
			void move_elevate(float amount);
			void rotatex(float amount);
			void rotatey(float amount);
			void rotatez(float amount);
			void lookAt(const Vector3& v);
			void updatePosition(const Vector3& playerPos);

			void set_perspective(float fov, float aspect, float znear, float zfar);
			void set_orthogonal(float width, float height, float znear, float zfar);

			const Matrix4& get_view() const;
			const Matrix4& get_projection() const;
			Vector3 get_position() const;

			const Frustum& get_frustum() const;

		private:
			bool m_dirty;
			float m_pitch;
			float m_yaw;
			float m_roll;

			Vector3 m_right;
			Vector3 m_up;
			Vector3 m_forward;

			Matrix4 m_view;
			Matrix4 m_projection;

			Frustum m_frustum;
		};
	}
}
