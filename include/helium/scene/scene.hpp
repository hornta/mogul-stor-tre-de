// scene.hpp

#ifndef SCENE_HPP_INCLUDED
#define SCENE_HPP_INCLUDED

#include <helium/scene/camera.hpp>
#include <helium/scene/scene_node.hpp>
#include <helium/renderer/render_queue.hpp>
#include <helium/renderer/render_dispatcher.hpp>
#include "ResourceLoader.h"
#include "Color.h"
#include "ShadowMap.h"

class Object3D;
class Mesh;
class Light;
class SoundSystem;

namespace helium
{
	namespace scene
	{
		const int MAX_LIGHTS = 8;
		class Scene
		{
		public:
			Scene(helium::system::RenderSystem* renderSystem, SoundSystem* soundSystem);
			~Scene();

			void pre_render();
			void post_render();
			void render();
			void update(float dt);

			void add(Object3D* object3d);

			void setAmbientColor(Vector4 color);
			void set_camera(Camera* camera);
			void setResourceLoader(ResourceLoader* resourceLoader);
			Camera* get_camera();
			void setShadowTexture(uint32_t id);
			void setShadowDepthStencilView(uint32_t);

			Object3D* getObjectById(uint32_t id);
			Object3D* findObjectByIdRecursive(Object3D* parent, uint32_t id);
			Object3D* getLightById(uint32_t id);
			void removeObjectById(uint32_t id);
			void removeObject(Object3D* object);

			RenderSystem* getRenderSystem();
			SoundSystem* getSoundSystem();
			ResourceLoader* getResourceLoader();
		private:
			void renderMesh(Mesh* mesh);
			void renderRecursive(Object3D* node);
			void removeObject(std::size_t index);
		private:
			Camera* m_camera;
			std::vector<Object3D*> m_nodes;
			std::vector<Object3D*> m_visibles;
			std::vector<Object3D*> m_lightNodes;
			
			/* LIGHTNING */
			Vector4 m_eye;
			Color m_globalAmbient;
			
			struct LightConstant
			{
				Vector3 position; float pad0;
				Vector3 direction; float pad1;
				Color color;
				float spotAngle;
				float constantAttenuation;
				float linearAttenuation;
				float quadraticAttenuation;
				int type;
				int enabled;
				int padding[2];
			};
			LightConstant m_lights[MAX_LIGHTS];
			int m_lightIndex;

			SoundSystem* m_soundSystem;
			ResourceLoader* m_resourceLoader;
			system::RenderSystem* m_renderSystem;
			renderer::RenderQueue m_render_queue;
			renderer::RenderDispatcher::Ptr m_render_dispatcher;
		};
	}
}

#endif // SCENE_HPP_INCLUDED
