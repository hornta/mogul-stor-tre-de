// os_event.hpp

#ifndef OSEVENT_HPP_INCLUDED
#define OSEVENT_HPP_INCLUDED

#include <helium/mouse.hpp>
#include <helium/keyboard.hpp>

namespace helium
{
	enum class EOSEventType : uint32_t
	{
		Close,
		MouseMove,
		MouseButton,
		MouseWheel,
		Keyboard,
		Text,
		Resize,
		Focus,
		Invalid,
	};

	class OSEvent
	{
	public:
		OSEvent();
		explicit OSEvent(EOSEventType type);

		struct MouseMove {
			short x, y;
		};
		struct MouseButton {
			EMouseButton button;
			bool state;
		};
		struct MouseWheel {
			float delta;
		};
		struct Keyboard {
			unsigned keycode;
			bool state;
		};
		struct Text {
			unsigned unicode;
		};
		struct Resize {
			int width;
			int height;
		};
		struct Focus {
			bool state;
		};

		EOSEventType m_type;
		union
		{
			union
			{
				MouseMove move;
				MouseButton button;
				MouseWheel wheel;
			} mouse;
			Keyboard keyboard;
			Text text;
			Resize resize;
			Focus focus;
		} m_data;
	};
}

#endif // EVENT_HPP_INCLUDED
