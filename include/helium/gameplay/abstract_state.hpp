// abstract_state.hpp

#ifndef ABSTRACTSTATE_HPP_INCLUDED
#define ABSTRACTSTATE_HPP_INCLUDED


namespace helium
{
	namespace gameplay
	{
		class AbstractGame;
		class AbstractState
		{
		public:
			virtual ~AbstractState();

			virtual void initialize() = 0;
			virtual void shutdown() = 0;
			virtual bool update(float dt) = 0;
			virtual void draw() = 0;
			void setGame(AbstractGame* game);
			uint32_t get_next();
			void setNext(uint32_t hash);
		protected:
			AbstractGame* m_game;
			Time m_start_time;
			Time m_total_time;
			uint32_t m_id_hash;
			uint32_t m_id_hash_next;
		};
	}
}

#endif // ABSTRACTSTATE_HPP_INCLUDED
