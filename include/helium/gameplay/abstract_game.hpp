// abstract_game.hpp

#ifndef ABSTRACTGAME_HPP_INCLUDED
#define ABSTRACTGAME_HPP_INCLUDED

#include <helium/os_event_listener.hpp>
#include <helium/gameplay/state_manager.hpp>

class SoundSystem;
class ResourceLoader;
namespace helium
{
	namespace gameplay
	{
		class AbstractGame
		{
		public:
			typedef std::unique_ptr<AbstractGame> Ptr;
			
			static Ptr create();

		public:
			virtual ~AbstractGame();

			virtual void initialize();
			virtual void shutdown();
			virtual bool process();

			system::RenderSystem* renderer();
			system::CollisionSystem* collision();
			system::AudioSystem* audio();
			SoundSystem* getSoundSystem();
			ResourceLoader* getResourceLoader();
		
		protected:
			StateManager::Ptr m_state_manager;
			system::CollisionSystem* m_collision_system;
			system::RenderSystem* m_render_system;
			system::AudioSystem* m_audio_system;
			SoundSystem* m_soundSystem;
			ResourceLoader* m_resourceLoader;
			Time m_time;
		};
	}
}

#endif // ABSTRACTGAME_HPP_INCLUDED
