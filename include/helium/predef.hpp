// predef.hpp

#ifndef PREDEF_HPP_INCLUDED
#define PREDEF_HPP_INCLUDED

namespace helium
{
	namespace system
	{
		class RenderSystem;
		class AudioSystem;
		class CollisionSystem;
	}
}

#endif // PREDEF_HPP_INCLUDED
