// render_queue.hpp

#ifndef RENDERQUEUE_HPP_INCLUDED
#define RENDERQUEUE_HPP_INCLUDED

#include <helium/renderer/render_command.hpp>

namespace helium
{
	namespace renderer
	{
		class RenderQueue
		{
		public:
			RenderQueue();

			void reset();
			void push(RenderCommand& cmd);
			bool pop(RenderCommand& cmd);

		private:
			std::queue<RenderCommand> m_queue;
		};
	}
}

#endif // RENDERQUEUE_HPP_INCLUDED
