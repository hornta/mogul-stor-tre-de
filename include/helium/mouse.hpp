// mouse.hpp

#ifndef MOUSE_HPP_INCLUDED
#define MOUSE_HPP_INCLUDED

#include <helium/os_event_listener.hpp>

namespace helium
{
	enum class EMouseButton : uint32_t
	{
		Left,
		Right,
		Middle,
		Extra1,
		Extra2,
		Invalid,
	};

	class Mouse : OSEventListener
	{
	private: // non-copyable
		Mouse(const Mouse&);
		Mouse& operator=(const Mouse&);

	public:
		typedef std::unique_ptr<Mouse> Ptr;

		static Ptr create();

	private:
		Mouse();

	public:
		~Mouse();

		bool is_button_down(EMouseButton btn) const;
		bool is_button_down_once(EMouseButton btn) const;
		int32_t get_x() const;
		int32_t get_y() const;

		void post_frame();

	private:
		void notify(OSEvent* event);

	private:
		int32_t m_x, m_y;
		float m_delta;
		bool m_curr[(uint32_t)EMouseButton::Invalid];
		bool m_prev[(uint32_t)EMouseButton::Invalid];
	};
}

#endif // MOUSE_HPP_INCLUDED
