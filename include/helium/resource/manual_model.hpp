// manual_model.hpp

#ifndef MANUALMODEL_HPP_INCLUDED
#define MANUALMODEL_HPP_INCLUDED

#include <helium/resource/model.hpp>

namespace helium
{
	namespace resource
	{
		class ManualModel
		{
		private: // non-copyable
			ManualModel(const ManualModel&);
			ManualModel& operator=(const ManualModel&);

		public:
			typedef std::unique_ptr<ManualModel> Ptr;

			static Ptr create();

		private:
			ManualModel();

		public:
			~ManualModel();

		private:
			VertexBuffer::Ptr m_vertex_buffer;
			IndexBuffer::Ptr m_index_buffer;

			uint32_t m_vertex_size;
			uint32_t m_vertex_count;
			void* m_vertices;

			uint32_t m_index_size;
			uint32_t m_index_count;
			void* m_indices;
		};
	}
}

#endif // MANUALMODEL_HPP_INCLUDED
