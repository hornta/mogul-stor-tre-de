// font.hpp

#ifndef FONT_HPP_INCLUDED
#define FONT_HPP_INCLUDED

#include <helium/resource/resource.hpp>

namespace helium
{
	namespace resource
	{
		struct Glyph
		{
			uint32_t id;
			uint32_t x;
			uint32_t y;
			uint32_t width;
			uint32_t height;
			uint32_t xOffset;
			uint32_t yOffset;
			uint32_t xAdvance;
			uint32_t page;
			uint32_t chnl;
			Vector4 texcoord;
		};

		struct Page
		{
			std::string filename;
		};

		struct Kerning
		{
			uint32_t first;
			uint32_t second;
			float amount;
		};

		class Texture;

		class Font : public Resource
		{
		public:
			Font();
			~Font();

			bool loadFromFile(const std::string& filepath);
			bool hasGlyph(uint8_t ascii) const;
			void retrieveGlyphs(const std::string& string, std::vector<Glyph*>& glyphs);
			float getKerning(uint8_t first, uint8_t second) const;
			Glyph* getGlyph(uint8_t ascii);
			std::vector<Page*> getPages();

			void dispose() {}

		private:
			int m_lineHeight;
			int m_base;
			int m_scaleW;
			int m_scaleH;
			int m_numPages;
			int m_packed;
			std::unordered_map<uint32_t, Glyph> m_glyphs;
			std::unordered_map<uint32_t, Page> m_pages;
			std::vector<Kerning> m_kernings;
		};
	}
}

#endif // FONT_HPP_INCLUDED
