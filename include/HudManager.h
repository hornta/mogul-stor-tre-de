// HudManager.h

#pragma once

#include "stdafx.h"
#include <helium/scene/scene.hpp>

class GameStats;
class UI;
class UIText;
class PlayerObject;

using namespace helium;
class HudManager {
public:
	HudManager();
	~HudManager();

	void Init(scene::Scene* scene, const GameStats* stats, const PlayerObject* player);
	void Update();
	void Draw();
	void CleanUp();

private:
	void UpdateHealth();
	void UpdateAmmo();
	void UpdateWeapon();
	void UpdateWave();
	void UpdateScore();
	void UpdateTime();

private:
	const GameStats* m_gameStats;
	UI* m_ui;
	const PlayerObject* m_player;
	UIText* m_health;
	UIText* m_ammo;
	UIText* m_weapon;
	UIText* m_wave;
	UIText* m_score;
	UIText* m_time;
};