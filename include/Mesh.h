#pragma once

#include "Geometry.hpp"
#include "Material.h"
#include "Object3D.h"

struct PhysicsBody;
class Model;

using namespace helium::system;
class Mesh : public Object3D
{
public:
	Mesh(Geometry* geometry, Material material);
	~Mesh();

	const AxisAlignedBoundingBox& getAABB() const;
	const BoundingSphere& getBoundingSphere() const;
	
	void constructVertexIndexBuffer(RenderSystem* renderSystem);
	void updateBound();
	void updateAABB();

	Geometry* m_geometry;
	Material m_material;

private:
	AxisAlignedBoundingBox m_aabb;
	BoundingSphere m_bs;
};

class PhysicsMesh : public Mesh
{
public:
	PhysicsMesh(Geometry* geometry, Material material);

	void setBody(PhysicsBody* body);
	PhysicsBody* getBody();
private:
	PhysicsBody* m_body;
};