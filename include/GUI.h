#pragma once

#include <helium/os_event_listener.hpp>
#include <helium/math.hpp>
#include <helium/renderer/render_queue.hpp>
#include <helium/renderer/render_dispatcher.hpp>

namespace helium
{
	namespace system
	{
		class RenderSystem;
	}
}

using namespace helium;

enum GUIElementType
{
	GUIELEMENT_UNDEFINED,
	GUIELEMENT_BUTTON
};

struct GUIVertex
{
	Vector3 position;
	Vector2 texcoord;
};

struct ButtonState
{
	Vector4 textureRect;
	int vertexBuffer;
};

class GUIEvent
{
public:
	struct ButtonEvent
	{
		int button_id;
	};

	enum EventType
	{
		ButtonPressed,
		ButtonReleased
	};

	EventType type;

	union
	{
		ButtonEvent button;
	};
};

class GUI;

class GUIElement
{
public:
	friend class GUI;
	GUIElement();

	int getId();
	void setId(int id);
	void setPosition(float x, float y);
	void setOffset(float x, float y);
	void setSize(float x, float y);
	void setTexture(int texture);
	void setShader(int shader);
	void setSampler(int sampler);
	void setVertexFormat(int vertexformat);
	void setBlendState(int blendstate);
	void setTextureRect(float top, float left, float right, float bottom);
	Vector2& getOffset();
	Vector2& getSize();
	virtual void update();
	virtual void receiveEvent(OSEvent* event);

	virtual void construct() = 0;
public:
	int m_shader;
	int m_texture;
	int m_sampler;
	int m_vertexformat;
	int m_vertexbuffer;
	int m_indexbuffer;
	int m_raster;
	int m_depth;
	int m_blend;
	int m_drawCount;

	Matrix4 m_world;

protected:
	GUIElementType m_type;
	GUIVertex* m_vb;
	uint32_t m_numVertices;
	Vector2 m_offset;
	Vector2 m_size;
	Vector2 m_position;
	Vector4 m_textureRect;
	GUI* m_gui;
	int m_id;
};

class GUIButton : public GUIElement
{
public:
	GUIButton();

	void update();
	void construct();
	void receiveEvent(OSEvent* event);

	void setIdleState(const ButtonState& btnState);
	void setDownState(const ButtonState& btnState);

private:
	ButtonState m_idleState;
	ButtonState m_downState;
	ButtonState* m_currentState;
	bool m_bIdleState;
	bool m_bDownState;

	bool m_isDown;
	bool m_hasChanged;
};

class GUI : OSEventListener
{
public:
	friend class GUIElement;
	GUI();
	~GUI();

	void init();
	void draw();
	void update();
	void addButton(GUIElement* element);
	Vector2& getMousePosition();
	bool pollEvent(GUIEvent& event);
	void pushEvent(const GUIEvent& event);
	Matrix4& getCamera();
private:
	void notify(helium::OSEvent* event);

private:
	renderer::RenderQueue m_render_queue;
	renderer::RenderDispatcher::Ptr m_render_dispatcher;
	std::vector<GUIElement*> m_elements;
	std::queue<GUIEvent> m_events;
	system::RenderSystem* m_renderSystem;
	Matrix4 m_camera;
	Vector2 m_mousePosition;
};