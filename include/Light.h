#pragma once

#include "Object3D.h"
#include "Color.h"
#include <inttypes.h>
#include <helium/math.hpp>

enum ELightType
{
	DIRECTIONAL_LIGHT,
	POINT_LIGHT,
	SPOT_LIGHT,
	UNDEFINED
};

class Light : public Object3D
{
public:
	Light();

	helium::Vector3 m_direction;
	Color m_color;
	float m_spotAngle;
	helium::Vector3 m_attenuation;
	ELightType m_light_type;
	bool m_enabled;
	bool m_castShadow;
	helium::Matrix4 m_projection;
};

class DirectionalLight : public Light
{
public:
	DirectionalLight(helium::Vector3 direction, Color color);
};

class PointLight : public Light
{
public:
	PointLight(helium::Vector3 attenuation, Color color);
};