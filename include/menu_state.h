#pragma once

#include <helium/gameplay/abstract_state.hpp>
#include <helium/scene/scene.hpp>
#include <inttypes.h>

namespace helium
{
	class PlayerObject;
}

using namespace helium;
class UI;

class menu_state : public gameplay::AbstractState
{
public:
	menu_state();
	~menu_state();

	void initialize();
	void shutdown();
	bool update(float dt);
	void draw();
private:
	Keyboard::Ptr m_keyboard;
	Mouse::Ptr m_mouse;
	float m_mouseDeltaX;
	float m_mouseDeltaY;
	UI* m_ui;
	bool m_leaveState;
};