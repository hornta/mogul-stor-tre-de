#pragma once
#include "fmod.hpp"
#include "fmod.h"
#include "fmod_errors.h"
#include <vector>
#include <map>

class Sound
{
public:
	Sound();

	FMOD::Channel* m_channel;
	FMOD::Sound* m_sound;
};

//FMOD AUDIO MANAGER
class SoundSystem
{
public:
	SoundSystem(void);
	~SoundSystem(void);

	void Update(); 
	void loadSound(const std::string& filename, const std::string& key, FMOD_MODE mode); 
	void playSound(const std::string& key, float volume);
	void changeVolume(const std::string& key, float amount);
	float getVolume(const std::string& key);

private:
	FMOD::System* m_system;
	std::map<std::string, Sound*> m_sounds;		
	float m_ChannelVolume;	
};
