#pragma once

#include <inttypes.h>

class Color
{
public:
	Color();
	Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);

	static const Color Black;
	static const Color Silver;
	static const Color Gray;
	static const Color White;
	static const Color Maroon;
	static const Color Red;
	static const Color Purple;
	static const Color Fuchsia;
	static const Color Green;
	static const Color Lime;
	static const Color Olive;
	static const Color Yellow;
	static const Color Navy;
	static const Color Blue;
	static const Color Teal;
	static const Color Aqua;
	static const Color Transparent;

	float m_r, m_g, m_b, m_a;
};