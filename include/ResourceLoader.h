#pragma once

#include <map>
#include <fmod.h>

namespace helium
{
	namespace system
	{
		class RenderSystem;
	}
	namespace resource
	{
		class Font;
		class Texture;
	}
}
using namespace helium::resource;

class ResourceLoader
{
public:
	ResourceLoader();
	~ResourceLoader();

	void init(helium::system::RenderSystem* renderSystem);
	int getShader(const std::string& filename);
	int getTexture(const std::string& filename);
	int getVertexFormat(const std::string& filename, const std::string filenameShader = "");
	int getVertexBuffer(const std::string& name);
	int getSampler(const std::string& filename);
	Font* getFont(const std::string& filename);
	Texture* getImage(const std::string& filename);
	
	void loadVertexBuffer(const std::string& name, const void* data, uint32_t count);

	void setShaderFolder(const std::string& folder);
	void setTextureFolder(const std::string& folder);
	void setVertexFormatFolder(const std::string& folder);
	void setSamplerFolder(const std::string& folder);
	void setFontFolder(const std::string& folder);
private:
	std::string m_shaderFolder;
	std::string m_textureFolder;
	std::string m_vertexFormatFolder;
	std::string m_samplerFolder;
	std::string m_fontFolder;

	std::map<std::string, int> m_shaders;
	std::map<std::string, int> m_textures;
	std::map<std::string, int> m_vertexFormats;
	std::map<std::string, int> m_vertexBuffers;
	std::map<std::string, int> m_samplers;
	std::map<std::string, Font*> m_fonts;
	std::map<std::string, Texture*> m_images;
	helium::system::RenderSystem* m_renderSystem;
};