// Weapon.h

#pragma once

#include "stdafx.h"

	enum WeaponType {
		None,
		Pistol,
		//Burst,
		Auto,
		Shotgun
	};

	class Weapon {
	public:
		Weapon(WeaponType type);
		~Weapon();

		WeaponType GetWeaponType() const;
		float GetRateOfFire() const;
		int GetMaxAmmunition() const;
		int GetCurrentAmmunition() const;
		float GetRange() const;
		float GetDamage() const;

		bool Fire();
		void NewAmmo(int amount);
		void Update(float dt);

	private:
		WeaponType m_type;
		float m_rateOfFire;
		int m_maxAmmunition;
		int m_currentAmmunition;
		float m_range;
		float m_damage;

		float m_cooldown;
	};
