#pragma once

#include <helium/gameplay/abstract_state.hpp>
#include <helium/scene/scene.hpp>
#include <inttypes.h>
#include <SoundSystem.h>

class PlayerObject;
class EnemyObject;
class GameObjectManager;
class HudManager;
class GameStats;

using namespace helium;

class loading_state : public gameplay::AbstractState
{
public:
	loading_state();
	~loading_state();

	void initialize();
	void shutdown();
	bool update(float dt);
	void draw();
private:
	scene::Scene* m_scene;
	Keyboard::Ptr m_keyboard;
	Mouse::Ptr m_mouse;
	float m_mouseDeltaX;
	float m_mouseDeltaY;

	GameObjectManager* m_gom;
	HudManager* m_hm;
	GameStats* m_gs;
	SoundSystem* m_soundSystem;;

	int m_score;
	float m_gameOverTimer;
	float m_enemyTimer;
	float m_pickupTimer;
	bool m_playerAlive;

	bool m_running;

};